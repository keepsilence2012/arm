#include <QTcpSocket>
#include <QDebug>
#include <QCloseEvent>
#include <QThread>
#include <QObject>
#include <QBitArray>
#include <QtCore/qmath.h>
#include <QTimer>
#include <QList>

#define OPEN_TIMEOUT 3000
#define ModbusTCP_Update_Interval 1000
#define MaxQuery 64
//#define DEBUG
#define ModbusAddr 85
#define ModbusStatus 124

namespace Ui {
    class TModbusTCPDevice;
    class TModbusTCPConnection;
    class TModbusTCPProcess;
    class TDIalog;
}

class TDialog : public QObject
{
    Q_OBJECT

public:
    TDialog(QObject * par = 0);
    int id;
    bool buzy;
    QByteArray Request;
    QByteArray Response;
};

class TModbusTCPProcess:public QThread
{
    Q_OBJECT

public:
    TModbusTCPProcess(QString inip, int inport, QObject * par = 0);
    ~TModbusTCPProcess();
    QTcpSocket * TCPSocket;
    bool needToRun;
    void run();
    bool socket_open(QTcpSocket *Socket);
    int socket_send(QTcpSocket *Socket, QByteArray req, QByteArray *resp);
    int next(int n);

    QList <TDialog *> dialog;

    bool connected;
    QString ip;
    int port;
    int processing;
    int last;
    int tmout;

signals:
    void error(QString);
    void answer(int id, QByteArray req, QByteArray resp);
public slots:
    void disconnected();
};

class TModbusTCPConnection : public QObject
{
    Q_OBJECT

public:
    TModbusTCPConnection(QString inip, int inport, QObject * par = 0);
    ~TModbusTCPConnection();
    QString ip;
    int port;
    int id;
    TModbusTCPProcess * process;
    int addQ(QByteArray req);
signals:
    void error(QString message);
};

class TModbusTCPDevice: public QObject
{
    Q_OBJECT

public:
    TModbusTCPDevice(QString inip, quint16 mda, int inport, QObject * par = 0);
    ~TModbusTCPDevice();
    TModbusTCPConnection * connection;
    void ch_on();
    void ch_off();

    QTimer status_timer;
    QByteArray Status;
    QString ip;
    int port;
    quint16 mdbAddr;

signals:
    void data(QByteArray);
    void error(QString);
public slots:
    void answer(int id, QByteArray req, QByteArray resp);
    void read();
};
