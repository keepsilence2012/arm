#include "ui.h"

void UI::openBase() {

    QDir d;
    qDebug() << "making dev dir" << d.mkdir(MAIN_PATH + "/devices") << "in" << d.absolutePath();
    QSqlDatabase::addDatabase("QSQLITE","con1");
    QSqlDatabase::database("con1").setDatabaseName(MAIN_PATH + "devices/devices");
    if (!QSqlDatabase::database("con1").open()) {
        trayMessage("АРМ SK-712","Не удалось подключиться к БД!",2);
        QMessageBox::warning(this,"APM SK-712","Ошибка: " + QSqlDatabase::database("con1").lastError().text());
    } else {
        trayMessage("АРМ SK-712","Подключение у БД успешно!",3);
        QSqlQuery q("",QSqlDatabase::database("con1"));
        qDebug() << "Creating database tables";
        if (q.exec(QString("BEGIN TRANSACTION"))
            && q.exec(QString("CREATE TABLE IF NOT EXISTS `work` ( 	`id`	INTEGER, 	`did`	INTEGER, 	`year`	INTEGER, 	`month`	INTEGER, 	`day`	INTEGER, 	`hour`	INTEGER, 	`work`	INTEGER, 	`uid`	TEXT UNIQUE, 	PRIMARY KEY(id) )"))
            && q.exec(QString("INSERT OR IGNORE INTO `work` VALUES(1,0,0,0,0,0,0,'0000/00/00/00&d0');"))
            && q.exec(QString("CREATE TABLE  IF NOT EXISTS `status` ( 	`id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, 	`device`	INTEGER NOT NULL, 	`timestamp`	INTEGER NOT NULL, 	`alarm`	INTEGER DEFAULT '1', 	`fulltext`	TEXT DEFAULT '*ALARM* *TIMEOUT* *END*', 	`working`	TEXT );"))
            && q.exec(QString("CREATE TABLE  IF NOT EXISTS `devices` ( 	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 	`did`	INTEGER NOT NULL UNIQUE, 	`address`	TEXT, 	`name`	TEXT, 	`type`	TEXT, 	`map`	INTEGER, 	`cordx`	REAL, 	`cordy`	REAL, 	`t_per`	REAL, 	`alx`	TEXT, 	`aly`	TEXT, 	`alw`	TEXT, 	`pumps`	INTEGER, 	`start`	TEXT, 	`mdbaddr`	INTEGER, 	`mdbpage`	INTEGER, 	`comment`	TEXT, 	`view`	INTEGER, 	`flow`	REAL )"))
            && q.exec(QString("COMMIT"))) {
            trayMessage("АРМ SK-712","База данных готова к использованию!",3);
            qDebug() << "creating non-existing tables: success";
        } else {
            trayMessage("АРМ SK-712","Ошибка базы данных!",3);
            qDebug() << "creating non-existing tables: failed" << q.lastError();
        }
    };
}


void UI::_loadDailyStat(quint16 id, QDate min, QDate max, QDate current) {
    if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ДНЕВНОЙ НАРАБОТКИ" << id;
    if (deviceArray.contains(id)) {
        QString q_s;
        double working = 0;
        double counter = 0,whole = (min.daysTo(max) + 1) * 24;
        q_s = QString("SELECT hour,work,year,month,day FROM WORK WHERE did = '%1' AND NOT(year > %3 OR (year = %3 AND (month > %5 OR (month = %5 AND day > %7)))) AND NOT (year < %2 OR (year = %2 AND (month < %4 OR(month = %4 AND day < %6))))").arg(id).arg(min.year()).arg(max.year()).arg(min.month()).arg(max.month()).arg(min.day()).arg(max.day());

        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            QList<double> av_val,max_val,la_val,min_val; QList<int> count; QList<QDate> l_d; QList<QDate> m_d; QList<QDate> min_d;
            for (int i=0;i<24;i++) {
                av_val.append(0); max_val.append(0); count.append(0); la_val.append(-1); l_d.append(QDate(2000,1,1)); m_d.append(QDate(2000,1,1)); min_val.append(61); min_d.append(QDate(2000,1,1));
            }

            if (query.size()) {
                while (query.next()) {
                    int h = query.value(0).toInt(),w = query.value(1).toInt(), y = query.value(2).toInt(), m = query.value(3).toInt(), d = query.value(4).toInt();
                    if (query.value(1).toInt() >= 0) counter++; else qDebug() << "MISSING";
                    count[h]++;
                    working += w;
                    av_val[h] = (av_val[h] * (count[h] - 1) + w) / count[h];
                    if (max_val[h] < w) {
                        max_val[h] = w;
                        m_d[h] = QDate(y,m,d);
                    }

                    if (min_val[h] > w && w > -1) {
                        min_val[h] = w;
                        min_d[h] = QDate(y,m,d);
                    }
                    if (QDate(y,m,d) == current) {
                        la_val[h] = w; l_d[h] = QDate(y,m,d);
                    }
                }
            }
            for (int i=0;i<24;i++) if (count[i] == 0) {av_val[i] = -1; max_val[i] = -1;};
            for (int i=0;i<24;i++) qDebug() << i << av_val[i] << max_val[i] << count[i]  << la_val[i];
            qDebug() << "ACCURACY: " << counter << "of" << whole;
            if (deviceWindows.contains(id)) {
                deviceWindows[id]->dailyWindow->setGraph(max_val,m_d,min_val, min_d, av_val,la_val,working / 60,(double)((double)counter/whole * 100));
                deviceWindows[id]->dailyWindow->setWindowTitle(deviceArray[id].name + " | Суточный график");
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить суточный график из БД:" + error,2);
        };

    }
}


void UI::saveDaily(quint16 id, QDate min, QDate max) {
    if (deviceArray.contains(id)) {
        QString q_s;
        double working = 0;
        double counter = 0,whole = 0;
        q_s = QString("SELECT hour,work,day,month,year FROM WORK WHERE did = '%1' AND NOT(year > %3 OR (year = %3 AND (month > %5 OR (month = %5 AND day > %7)))) AND NOT (year < %2 OR (year = %2 AND (month < %4 OR(month = %4 AND day < %6))))").arg(id).arg(min.year()).arg(max.year()).arg(min.month()).arg(max.month()).arg(min.day()).arg(max.day());

        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            QList<double> av_val,max_val; QList<int> count;
            QMap<QString,dailyWorking::csvData> values;
            for (int i=0;i<24;i++) {
                av_val.append(0); max_val.append(0); count.append(0);
            }

            if (query.size()) {
                QDate t_d;
                t_d = max;
                while (t_d >= min) {
                    qDebug() << "adding" << t_d.toString("yyyy/MM/dd");
                    dailyWorking::csvData i_d;
                    for (int i=0;i<24;i++) i_d.working[i] = -1;
                    values.insert(t_d.toString("yyyy/MM/dd"),i_d);
                    values[t_d.toString("yyyy/MM/dd")].day = t_d.day();
                    values[t_d.toString("yyyy/MM/dd")].month = t_d.month();
                    values[t_d.toString("yyyy/MM/dd")].year = t_d.year();
                    values[t_d.toString("yyyy/MM/dd")].date = t_d.toString("yyyy/MM/dd");
                    t_d = t_d.addDays(-1);

                };
                while (query.next()) {
                    whole++;
                    int h = query.value(0).toInt(),d=query.value(2).toInt(),m=query.value(3).toInt(),y=query.value(4).toInt();
                    int w = query.value(1).toInt();
                    if (query.value(1).toInt() >= 0) counter++; else qDebug() << "MISSING";
                    count[h]++;
                    working += w;
                    av_val[h] = (av_val[h] * (count[h] - 1) + w) / count[h];
                    if (max_val[h] < w) max_val[h] = w;

                    t_d.setDate(y,m,d);
                    if (!values.contains(t_d.toString("yyyy/MM/dd"))){
                        dailyWorking::csvData i_d;
                        for (int i=0;i<24;i++) i_d.working[i] = -1;
                        values.insert(t_d.toString("yyyy/MM/dd"),i_d);
                        values[t_d.toString("yyyy/MM/dd")].day = d;
                        values[t_d.toString("yyyy/MM/dd")].month = m;
                        values[t_d.toString("yyyy/MM/dd")].year = y;
                        values[t_d.toString("yyyy/MM/dd")].date = t_d.toString("yyyy/MM/dd");
                    }

                    values[t_d.toString("yyyy/MM/dd")].working[h] = w;
                }
            }
            for (int i=0;i<24;i++) if (count[i] == 0) {av_val[i] = -1; max_val[i] = -1;};
            for (int i=0;i<24;i++) qDebug() << i << av_val[i] << max_val[i] << count[i];
            qDebug() << "ACCURACY: " << counter << "of" << whole;
            if (deviceWindows.contains(id)) {
                //deviceWindows[id]->dailyWindow->setGraph(max_val,av_val,working / 60,(double)((double)counter/whole * 100));
                deviceWindows[id]->dailyWindow->setWindowTitle(deviceArray[id].name + " | Суточный график");
                if (deviceWindows[id]->dailyWindow->exportToCsv(values,av_val,max_val,deviceArray[id].name)) {
                    trayMessage("АРМ SK-712","Экспорт наработки завершен",7);
                } else {
                    trayMessage("АРМ SK-712","Экспорт не выполнен",7);
                };
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить суточный график из БД:" + error,2);
        };

    }
}

void UI::getMonth(quint16 id, int month, int year) {
    if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА МЕСЯЧНОЙ НАРАБОТКИ" << id ;
    if (deviceArray.contains(id)) {
        QString q_s; q_s = QString("SELECT day,sum(work),count(work) from work WHERE did=%3 AND month=%2 AND year=%1 AND work >-1 GROUP BY day").arg(year).arg(month).arg(id);
        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            if (query.size()) {
                int h,w,c=0;
                QList<double> out; for (int i=0;i<32;i++) out.append(-1); QMap<int,int> days;
                QList<double> per; for (int i=0;i<32;i++) per.append(0);
                while (query.next()) {
                    h=query.value(0).toInt();w=query.value(1).toInt();
                    c+=query.value(2).toInt();
                    if (!days.contains(h)) days.insert(h,w); else days[h] += w;
                    if (per.count() > h ) per[h] += (double)query.value(2).toInt() * 100/24;
                }

                foreach(int a, days.keys()) out[a] = (double)days[a] / 60 * deviceArray[id].flow;
                qDebug() << per;
                deviceWindows[id]->dailyWindow->setMGraph(out,per,deviceArray[id].flow,c);
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить месячный график из БД:" + error,2);
        }
    }
}

void UI::m_save(quint16 id, int month, int year) {
    if (deviceArray.contains(id)) {
        QString q_s; q_s = QString("SELECT day,sum(work),count(work) from work WHERE did=%3 AND month=%2 AND year=%1 AND work >-1 GROUP BY day").arg(year).arg(month).arg(id);
        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            if (query.size()) {
                int h,w,c;
                dailyWorking::m_csvData t_d; QMap<int,dailyWorking::m_csvData> out;
                while (query.next()) {
                    h=query.value(0).toInt();w=query.value(1).toInt();c=query.value(2).toInt();
                    if (!out.contains(h)) {
                        t_d.date = QDate(year,month,h).toString("yyyy/MM/dd");
                        t_d.day = h;
                        t_d.percent = (double)c / (24) * 100;
                        t_d.working = w;
                        out.insert(h,t_d);

                    } else {
                        out[h].working += w;
                    }
                }

                deviceWindows[id]->dailyWindow->m_export(out,deviceArray[id].name,deviceArray[id].flow);
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить месячный график из БД:" + error,2);
        }
    }
}

void UI::getYear(quint16 id, int year) {
        if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ГОДОВОЙ НАРАБОТКИ" << id;
    if (deviceArray.contains(id)) {
        QString q_s; q_s = QString("SELECT month,sum(work),count(work) from work WHERE did=%2 AND year=%1 AND work >-1 GROUP BY month").arg(year).arg(id);
        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            if (query.size()) {
                int h,w,c=0;
                QList<double> out; for (int i=0;i<13;i++) out.append(-1); QMap<int,int> months;
                QList<double> per; for (int i=0;i<13;i++) per.append(0);
                while (query.next()) {
                    h=query.value(0).toInt();w=query.value(1).toInt();
                    c+=query.value(2).toInt();
                    if (!months.contains(h)) months.insert(h,w); else months[h] += w;
                    if (per.count() > h) per[h] += (double)query.value(2).toInt() * 100 / (24 * QDate(year,h,1).daysInMonth());
                }

                foreach(int a, months.keys()) out[a] = (double)months[a] / 60 * deviceArray[id].flow;
                deviceWindows[id]->dailyWindow->setYGraph(out,per,deviceArray[id].flow,c);
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить месячный график из БД:" + error,2);
        }
    }
}

void UI::y_save(quint16 id, int year) {
    if (deviceArray.contains(id)) {
        QString q_s; q_s = QString("SELECT month,sum(work),count(work) from work WHERE did=%2 AND year=%1 AND work >-1 GROUP BY month").arg(year).arg(id);
        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
            if (query.size()) {
                int h,w,c; QDate d;
                dailyWorking::m_csvData t_d; QMap<int,dailyWorking::m_csvData> out;
                while (query.next()) {
                    h=query.value(0).toInt();w=query.value(1).toInt();c=query.value(2).toInt();
                    if (!out.contains(h)) {
                        t_d.date = QDate(year,h,1).toString("yyyy/MM/dd");
                        t_d.day = h;
                        t_d.percent = (double)c / (24 * QDate(year,h,1).daysInMonth()) * 100;
                        t_d.working = w;
                        out.insert(h,t_d);

                    } else {
                        out[h].working += w;
                    }
                }

                deviceWindows[id]->dailyWindow->y_export(out,deviceArray[id].name,deviceArray[id].flow);
            }
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить месячный график из БД:" + error,2);
        }
    }
}


void UI::exportToCsv() {
    exportDial = new exportDialog();
    foreach(deviceStruct d,deviceArray) {
            exportDial->addItem(d.index,d.name);
    }
    if (exportDial->exec() == QDialog::Accepted) {
        QDate min = exportDial->from(), max = exportDial->to();
        QString q_s = QString("SELECT hour,work,year,month,day,did FROM WORK WHERE work > -1 and NOT(year > %2 OR (year = %2 AND (month > %4 OR (month = %4 AND day > %6)))) AND NOT (year < %1 OR (year = %1 AND (month < %3 OR(month = %3 AND day < %5)))) AND (").arg(min.year()).arg(max.year()).arg(min.month()).arg(max.month()).arg(min.day()).arg(max.day());
        foreach(quint16 num,exportDial->checkedNums()) q_s.append(QString("did = %1 OR ").arg(num)); q_s.append("id = -1 ) ORDER BY uid");
        qDebug() << q_s;

        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(q_s)) {
           QString fileName = QFileDialog::getSaveFileName(this,"Сохранить как...",QString("Наработка с %1 по %2.csv").arg(min.toString("yyyy.MM.dd")).arg(max.toString("yyyy.MM.dd")),"*.csv");
           if (fileName.length()) {
               QFile file(fileName);
               if (file.open(QIODevice::WriteOnly)) {
                   file.write(QString("Наработка с %1 по %2\r\n").arg(min.toString("dd/MM/yyyy")).arg(max.toString("dd/MM/yyyy")).toLocal8Bit());
                   file.write(QString("Имя станции;Год;Месяц;День;Час;\"Наработка в час, мин\";\"Наработка в час, м3\"\r\n").toLocal8Bit());
                    if (query.size()) {
                        while (query.next()) {
                            int h = query.value(0).toInt(),y=query.value(2).toInt(),m=query.value(3).toInt(),d=query.value(4).toInt();
                            int w = query.value(1).toInt(); int id = query.value(5).toInt();
                            if (deviceArray.contains(id))
                            {
                                double f_w = (double) w * deviceArray[id].flow / 60;
                                file.write(QString("\"%1\";%2;%3;%4;%5;%6;%7\r\n").arg(deviceArray[id].name).arg(y).arg(m).arg(d).arg(h).arg(w).arg(QString::number(f_w,'f',3)).toLocal8Bit());
                            }
                        }
                    }
               } else {
                   trayMessage("APM SK-712","Не удалось открыть файл на запись!",3);
               };
               file.close();
               trayMessage("АРМ SK-712","Экспорт наработки завершен",7);

            } else {
            trayMessage("APM SK-712","Сохранение наработки отменено",1);
          };

        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось загрузить суточный график из БД:" + error,2);
        };
    }
    delete exportDial;
}
