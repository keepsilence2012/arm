#ifndef CALWID_H
#define CALWID_H

#include <QWidget>
#include <QCalendarWidget>
#include <QDate>
#include <QTimer>
#include <QDebug>

namespace Ui {
class calWid;
}

class calWid : public QWidget
{
    Q_OBJECT

public:
    bool dateFocused;
    bool calFocused;
    bool dropped;
    bool firstDrop;
    QCalendarWidget * cal;
    explicit calWid(QWidget *parent = 0);
    ~calWid();
    QDate date();
    bool eventFilter(QObject *obj, QEvent *ev);
    bool down;

public slots:
    void setDate(QDate d);
    void checkFocus();
    void setDateRange(QDate from, QDate to);
    void drop();
    void activateDate();
    void setPos(int x,int y);

signals:
    void dateChanged(QDate);

private:
    Ui::calWid *ui;
    int X,Y;

};

#endif // CALWID_H
