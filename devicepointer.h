#ifndef DEVICEPOINTER_H
#define DEVICEPOINTER_H

#include <QWidget>
#include <QLabel>
#include <QHoverEvent>
#include <QMouseEvent>
#include <QMoveEvent>
#include <QDebug>

namespace Ui {
class devicePointer;
}

class devicePointer : public QWidget
{
    Q_OBJECT
    
public:
    explicit devicePointer(QWidget *parent = 0);
    ~devicePointer();
    Ui::devicePointer *ui;
    void enterEvent(QEvent *ev);
    void leaveEvent(QEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

    //Указатели на элементы управления
    QLabel * pointer; //Картинка указателя
    QLabel * index; // Номер картинки
signals:
    void hovered(quint16); //Наведен курсор
    void left(quint16); // курсор убран
    void dblclick(quint16); //Двойной щелчок
    void moved(quint16); //Перемещение
};

#endif // DEVICEPOINTER_H
