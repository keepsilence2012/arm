#include "pressure.h"
#include "ui_pressure.h"

pressure::pressure(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::pressure)
{
    ui->setupUi(this);

    downs.append(ui->down_1);sliders.append(ui->slider_1);presses.append(ui->press_1);ups.append(ui->up_1);
    downs.append(ui->down_2);sliders.append(ui->slider_2);presses.append(ui->press_2);ups.append(ui->up_2);
    downs.append(ui->down_3);sliders.append(ui->slider_3);presses.append(ui->press_3);ups.append(ui->up_3);
    downs.append(ui->down_4);sliders.append(ui->slider_4);presses.append(ui->press_4);ups.append(ui->up_4);
    downs.append(ui->down_5);sliders.append(ui->slider_5);presses.append(ui->press_5);ups.append(ui->up_5);
    downs.append(ui->down_6);sliders.append(ui->slider_6);presses.append(ui->press_6);ups.append(ui->up_6);
    downs.append(ui->down_7);sliders.append(ui->slider_7);presses.append(ui->press_7);ups.append(ui->up_7);
    downs.append(ui->down_8);sliders.append(ui->slider_8);presses.append(ui->press_8);ups.append(ui->up_8);
    downs.append(ui->down_9);sliders.append(ui->slider_9);presses.append(ui->press_9);ups.append(ui->up_9);
    downs.append(ui->down_10);sliders.append(ui->slider_10);presses.append(ui->press_10);ups.append(ui->up_10);
    downs.append(ui->down_11);sliders.append(ui->slider_11);presses.append(ui->press_11);ups.append(ui->up_11);
    downs.append(ui->down_12);sliders.append(ui->slider_12);presses.append(ui->press_12);ups.append(ui->up_12);
    downs.append(ui->down_13);sliders.append(ui->slider_13);presses.append(ui->press_13);ups.append(ui->up_13);
    downs.append(ui->down_14);sliders.append(ui->slider_14);presses.append(ui->press_14);ups.append(ui->up_14);
    downs.append(ui->down_15);sliders.append(ui->slider_15);presses.append(ui->press_15);ups.append(ui->up_15);
    downs.append(ui->down_16);sliders.append(ui->slider_16);presses.append(ui->press_16);ups.append(ui->up_16);
    downs.append(ui->down_17);sliders.append(ui->slider_17);presses.append(ui->press_17);ups.append(ui->up_17);
    downs.append(ui->down_18);sliders.append(ui->slider_18);presses.append(ui->press_18);ups.append(ui->up_18);
    downs.append(ui->down_19);sliders.append(ui->slider_19);presses.append(ui->press_19);ups.append(ui->up_19);
    downs.append(ui->down_20);sliders.append(ui->slider_20);presses.append(ui->press_20);ups.append(ui->up_20);
    downs.append(ui->down_21);sliders.append(ui->slider_21);presses.append(ui->press_21);ups.append(ui->up_21);
    downs.append(ui->down_22);sliders.append(ui->slider_22);presses.append(ui->press_22);ups.append(ui->up_22);
    downs.append(ui->down_23);sliders.append(ui->slider_23);presses.append(ui->press_23);ups.append(ui->up_23);
    downs.append(ui->down_24);sliders.append(ui->slider_24);presses.append(ui->press_24);ups.append(ui->up_24);


    for (int i=0;i<24;i++) {
        presses[i]->setText("12.4");
        sliders[i]->setRange(0,255);
        sliders[i]->setValue(124);
        sliders[i]->setObjectName("s" + QString::number(i));
        downs[i]->setObjectName("d" + QString::number(i));
        ups[i]->setObjectName("u"+QString::number(i));
        connect(sliders[i],SIGNAL(valueChanged(int)),this,SLOT(sliderMoved()));
        connect(downs[i],SIGNAL(clicked()),this,SLOT(slideDown()));
        connect(ups[i],SIGNAL(clicked()),this,SLOT(slideUp()));
    }

}

pressure::~pressure()
{
    for (int i=0;i<24;i++) {
        downs.removeLast();sliders.removeLast();ups.removeLast();presses.removeLast();
    }
    delete ui;
}

void pressure::sliderMoved() {
    int i = sender()->objectName().remove(0,1).toInt();
    if (i > -1 && i < 24) presses[i]->setText(QString().setNum((float)((float)sliders[i]->value() / 10 ),'f',1));
}

void pressure::slideDown() {
    int i = sender()->objectName().remove(0,1).toInt();
    qDebug() << i;
    if (i > -1 && i < 24) sliders[i]->setValue(sliders[i]->value() - 1);
}

void pressure::slideUp() {
    int i = sender()->objectName().remove(0,1).toInt();
    qDebug() << i;
    if (i > -1 && i < 24) sliders[i]->setValue(sliders[i]->value() + 1);
}
