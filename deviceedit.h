#ifndef DEVICEEDIT_H
#define DEVICEEDIT_H

#include <QWidget>
#include <QTextEdit>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QRegExp>
#include <QDialog>
#include <QCloseEvent>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

namespace Ui {
class deviceEdit;
}

class deviceEdit : public QDialog
{
    Q_OBJECT
    
public:
    explicit deviceEdit(QWidget *parent = 0);
    ~deviceEdit();
    Ui::deviceEdit *ui;

    QLineEdit * alx;
    QLineEdit * aly;
    QLineEdit * alw;
    QLineEdit * num1;
    QLineEdit * num2;
    QLineEdit * num;
    QLineEdit * name;
    QComboBox * type;
    QSpinBox * index;
    QSpinBox * pumps;
    QPushButton * delButton;
    QComboBox * mdbPort;
    QSpinBox * mdbAddr;
    QWidget * gsmWidget;
    QWidget * tcpWidget;
    QWidget * mdbWidget;
signals:
    void Error(QString); //Для сигнала о некорретности ввода
    void indexChanged(quint16 index); //Смена индекса
    void deleteDevice();
public slots:
    void checkAccept();
    void num1Changed();
    void num2Changed();
    void typeChanged();
};

#endif // DEVICEEDIT_H
