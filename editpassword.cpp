#include "editpassword.h"
#include "ui_editpassword.h"

editPassword::editPassword(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editPassword)
{
    ui->setupUi(this);
    setWindowTitle("Изменение пароля");
    setModal(true);
    connect(ui->ok,SIGNAL(clicked()),this,SIGNAL(ok()));
    connect(ui->no,SIGNAL(clicked()),this,SIGNAL(no()));
    old = ui->old;
    new1 = ui->new1;
    new2 = ui->new2;
    reallyClose = false;
}

editPassword::~editPassword()
{
    delete ui;
}

void editPassword::closeEvent(QCloseEvent *ev) {
    if (reallyClose) {
        ev->accept();
    } else {
        emit no();
        ev->ignore();
    };
}
