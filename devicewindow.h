#ifndef DEVICEWINDOW_H
#define DEVICEWINDOW_H

#include <QWidget>
#include <QList>
#include <QLabel>
#include <QGroupBox>
#include <QLine>
#include <QMoveEvent>
#include <QTextBrowser>
#include <QPushButton>
#include <QCheckBox>
#include <QListWidget>
#include <QComboBox>
#include <QDateEdit>
#include <QMessageBox>
#include <dailyworking.h>

namespace Ui {
class deviceWindow;
}

class deviceWindow : public QWidget
{
    Q_OBJECT
    
public:
    dailyWorking * dailyWindow;
    explicit deviceWindow(QWidget *parent = 0);
    ~deviceWindow();
    quint16 index;
    double flow;
    bool waiting;
    bool graphics;
    Ui::deviceWindow *ui;
    QList<QLabel *> in;
    QList<QLabel *> rdy;
    QList<QLabel *> run;
    QList<QLabel *> err;
    QList<QLabel *> pwr;
    QList<QFrame *> pumpSelector;
    QGroupBox * pumps;
    QLabel * an;
    QLabel * sbm;
    QLabel * ssm;
    QLabel *alxLab, *alx,*alxLab_2,*alx_2;
    QLabel *alyLab, *aly,*alyLab_2,*aly_2;
    QLabel *alwLab, *alw,*alwLab_2,*alw_2;
    QTextBrowser * text;
    QPushButton * onButton;
    QPushButton * offButton;
    QPushButton * statButton;
    QPushButton * resetButton;
    QWidget * loading;
    QLabel * loadingStat;

    QListWidget * historyList;
    QCheckBox * onlyAlarm;
    QComboBox * historyPeriod;
    QDateEdit * minDate;
    QDateEdit * maxDate;
    QPushButton * showHistory;
    QPushButton * turnHistory;
    QPushButton * turnHistory2;
    QWidget * history;
    QWidget * data;
    void Show();

    QLabel * sbm_g;
    QLabel * ssm_g;
    QList<QLabel *> in_g;
    QLabel * an_g;
    QLabel * winCon;
    QLabel * con;
    bool conStat;
    void SetWindowTitle(QString title);
    void moveEvent(QMoveEvent *ev);

    QString fullName;

public slots:
    void on();
    void off();
    void reset();
    void stat();
    void histPr();
    void shH();
    void shHS();
    void dCh();
    void frWk();
    void frMnt();
    void lastF();
    void inPr();
    void setType(QString type);
    void showDailyWorking();
    void setGraphics(bool on);
signals:
    void Action(quint16,QString);
    void dailyQuery(quint16,QDate,QDate,QDate);
    void monthQuery(quint16,int,int);
    void m_saveQuery(quint16,int,int);
    void yearQuery(quint16,int);
    void y_saveQuery(quint16,int);
    void saveQuery(quint16,QDate,QDate);
    void History(quint16);
    void ShowHistory(quint16,qint16);
    void ShowHistoryStat(quint16);
    void Last(quint16);
    void info(quint16);
    void moved(quint16);
};

#endif // DEVICEWINDOW_H
