#ifndef DOWNLOADUPDATE_H
#define DOWNLOADUPDATE_H

#define MAIN_PATH QString("")
#define MAIN_PATH1 QString("C:\\Prog\\APM SK-712\\")

#include <QMainWindow>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QList>
#include <QFile>
#include <QRegExp>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QStringList>
#include <QTimer>
#include <QUrl>
#include "downloadwidget.h"
#include <private/qzipreader_p.h>
#include <QProcess>

class downloadUpdate : public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QNetworkReply * currentDownload;
    QString myVer;
    QString newVer;
    bool check;
    downloadWidget * wid;
    QString fileName;

public:
    bool newer(QString ver);
    downloadUpdate();
    void doDownload(const QUrl &url);
    void setCurrentVersion(QString ver);

public slots:
    void bytesChanged(qint64 bytes,qint64 total);
    void downloadFinished();
    void sslErrors(const QList<QSslError> &errors);
    void download();
    void save(QString filename);
    void checkUpdate();
signals:
    void message(QString,QString,int);
};

#endif // DOWNLOADUPDATE_H
