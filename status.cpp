#include "ui.h"

//Загрузка статистики прибора
void UI::_loadDeviceStat(quint16 index) {
    status st;
    if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ЛОГОВ ПРИБОРОВ: ИНДЕКС " << index;
    if (deviceArray.contains(index)) {
        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(QString("SELECT fulltext FROM status WHERE device = %1 ORDER BY timestamp DESC LIMIT 1 ").arg(index))) {
            if (query.next()) {
                st = StringToStat(query.value(0).toString());
                if (st.correct) {
                    addStatus(index,st); //Добавляем статус в НС
                    if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ЛОГОВ ПРИБОРОВ: ЗАГРУЖЕНО!" << index << st.stat << " at " << st.date;
                } else {
                    QString t(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss").append(" *ALARM* *TIMEOUT* *END*")); while(t.count() < 198) t.append(" ");
                    st = StringToStat(t);
                    addStatus(index,st);
                    _saveDeviceStat(index,t);
                };
            } else {
                qDebug() << "НЕТ СОСТОЯНИЙ ПРИБОРА!!!";
                QString t(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss").append(" *ALARM* *TIMEOUT* *END*")); while(t.count() < 198) t.append(" ");
                st = StringToStat(t);
                addStatus(index,st);
                _saveDeviceStat(index,t);
            };
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось сделать запись в БД:" + error,2);
        };
    };
}

//Сохранить состояние в лог прибора
void UI::_saveDeviceStat(quint16 number, QString status) {
    while (status.length() < 198) status.append(" ");
    QString dateStat(status); dateStat.truncate(19);
    QDateTime curDate = QDateTime().fromString(dateStat, "yyyy/MM/dd hh:mm:ss");
    int alarm = (status.contains("*ALARM*")) ? 1 : 0;
    QString working;
    for (int i=0;i<24;i++) working.append("z");
    QSqlQuery query("",QSqlDatabase::database("con1"));
    if (query.exec(QString("INSERT INTO status (device, timestamp, alarm, fulltext, working) VALUES(%1, %2, %3, '%4', '%5')").arg(number).arg(curDate.toMSecsSinceEpoch()).arg(alarm).arg(status).arg(working))) {

    } else {
        QString error = query.lastError().text();
        qDebug() << "WRITING ERROR!!!";
        qDebug() << QString("INSERT INTO status (device, timestamp, alarm, fulltext, working) VALUES(%1, %2, %3, '%4', '%5')").arg(number).arg(curDate.toMSecsSinceEpoch()).arg(alarm).arg(status).arg(working);
        qDebug() << error;
        trayMessage("APM SK-712","Не удалось сделать запись в БД:" + error,2);
    };
}



//Преобразование строки в статус
status UI::StringToStat(QString str) {
    QList<QString> w_n;
    w_n << "0" << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9" << "A" << "B" << "C" << "D" << "E" << "F" << "G" << "H" << "I" << "J" << "K" << "L" << "M" << "N" << "O" << "P" << "Q" << "R" << "S" << "T" << "U" << "V" << "W" << "X" << "Y" << "Z" << "a" << "b" << "c" << "d" << "e" << "f" << "g" << "h" << "i" << "j" << "k" << "l" << "m" << "n" << "o" << "p" << "q" << "r" << "s" << "t" << "u" << "v" << "w" << "x" << "y";
    status out;
    int i;

    //Нужные для рабты функции регулярки
    QRegExp inReg("IN=([0-1])([0-1])([0-1])([0-1])([0-1])([0-1])");
    QRegExp sbmReg("SBM=([0-1])");
    QRegExp ssmReg("SSM=([0-1])");
    QRegExp anReg("AN=\\s{0,1}([0-9\\.]{1,4})");
    QRegExp rdyReg("RDY=([1_])([2_])([3_])([4_])([5_])([6_])");
    QRegExp runReg("RUN=([1_])([2_])([3_])([4_])([5_])([6_])");
    QRegExp errReg("ERR=([1_])([2_])([3_])([4_])([5_])([6_])");
    QRegExp pwrReg("PWR=([1_])([2_])([3_])([4_])([5_])([6_])");
    QRegExp errRegExp("E([0-9]{2})");
    QRegExp dateReg("([0-9]{4}\/[0-9]{2}\/[0-9]{2}\\s[0-9]{2}:[0-9]{2}:[0-9]{2})");
    QRegExp dailyReg("Q=([0-9a-zA-Z]{1,24})");

    bool correct = true; //флаг корректности смс

    //Дневные очереди
    out.daily = false;
    for (i=0;i<24;i++) out.work[i] = -1;




    out.fullText = str;

    if (str.contains(dailyReg)) {
        QString w_s,s_d,tmp;
        out.daily = false;
//        for (i=0;i<24;i++) {
//            w_s = dailyReg.cap(i+1);
//            s_d.append(w_s).append(QString::number(w_n.indexOf(w_s)));
//            if (w_s != "z") {
//                out.work[i] = w_n.indexOf(w_s);
//            }
//        }

        w_s = dailyReg.cap(1);
        int i = 0;
        while (w_s.length()) {
            tmp = w_s.left(1);
            s_d.append(tmp).append(QString::number(w_n.indexOf(tmp)));
            if (tmp != "z") {
                out.work[i] = w_n.indexOf(tmp);
            }
            w_s = w_s.right(w_s.length() - 1);
            i++;
        }
        qDebug() << s_d;
    }


    //Вырезаем дату
    if (str.contains(dateReg)) out.date = QDateTime().fromString(dateReg.cap(1),"yyyy/MM/dd hh:mm:ss");
    else correct = false;

    //преобразовываем заголовок
    if (str.contains("*READY")) out.stat = "ready";
    else if (str.contains("*OFF*")) out.stat = "off";
    else if (str.contains("*HAND MODE*")) out.stat = "handMode";
    else if (str.contains("*TIMEOUT*")) out.stat = "timeout";
    else if (str.contains("*ALARM*")) out.stat = "alarm";
    else correct = false;

    if (str.contains(dailyReg)) {
            for (i=0;i<24;i++) {

            };
            out.daily = true;
    };

    //Парсим насосы
    if (str.contains(rdyReg)) for(i=0;i<6;i++) out.rdy[i] = ((rdyReg.cap(i+1).toInt() == i+1)); else if (out.stat != "timeout") correct = false;
    if (str.contains(runReg)) for(i=0;i<6;i++) out.run[i] = ((runReg.cap(i+1).toInt() == i+1)); else if (out.stat != "timeout") correct = false;
    if (str.contains(errReg)) for(i=0;i<6;i++) out.err[i] = ((errReg.cap(i+1).toInt() == i+1)); else if (out.stat != "timeout") correct = false;
    if (str.contains(pwrReg)) for(i=0;i<6;i++) out.pwr[i] = ((pwrReg.cap(i+1).toInt() == i+1)); else if (out.stat != "timeout") correct = false;

    //Парсим входы-выходы
    if (str.contains(inReg)) for (i=0;i<6;i++) out.in[i] = (inReg.cap(i+1) == "1"); else if (out.stat != "timeout") correct = false;

    out.sbm = 0;
    out.ssm = 0;
    //Парсим SBM SSM
    if (str.contains(sbmReg)) out.sbm = (sbmReg.cap(1) == "1"); else if (out.stat != "timeout") correct = false;
    if (str.contains(ssmReg)) out.ssm = (ssmReg.cap(1) == "1"); else if (out.stat != "timeout") correct = false;

    //Парсим alx aly alw
    out.alx = str.contains("AL.X"); out.aly = str.contains("AL.Y"); out.alw = str.contains("AL.W");

    //Парсим AN
    if (str.contains(anReg)) out.an = anReg.cap(1).toDouble(); else out.an = 0.00;

    //Парсим коды ошибок
    i=0;
    while (i>=0) {
        i = errRegExp.indexIn(str,i+1);
        if (i >=0) {
            out.errors.append(errRegExp.cap(1).toUInt());
        }
    };


    //Забить структуру, если таймаут
    if (out.stat == "timeout") {
        for (i=0;i<6;i++) {
            out.rdy[i] = false; out.run[i] = false; out.err[i] = false; out.pwr[i] = false; out.in[i] = false; out.an = 0;
        }
    }

    if (!correct && str.contains("*NO INFO*")) {
        correct = true;
        for (i=0;i<6;i++) {
            out.rdy[i] = false; out.run[i] = false; out.err[i] = false; out.pwr[i] = false; out.in[i] = false; out.an = 0;
        }
    }
    out.correct = correct;

    return out;
}




//Загрузка полнейшей истории
//void UI::_loadFullHistory(quint16 index,qint16 count) {
//    if (deviceArray.contains(index)) {
//        //OLOLOLO
//        QFile file(MAIN_PATH + "devices/" + QString::number(index) + "/log");
//        if (file.exists() && file.open(QIODevice::ReadOnly)) {
//            deviceWindows[index]->historyList->clear();
//            QByteArray fileData = file.readAll().replace(0,' ');
//            QStringList sl = QString(fileData).split("\r\n");
//            bool isCount = !(count < 0);
//            quint16 writes = 0;
//            sl.sort();
//            QString s_t;
//            for(quint64 i = 0; i< sl.count() / 2; i++) {
//                s_t = sl[i];
//                sl[i] = sl[sl.count() - i - 1];
//                sl[sl.count() - i - 1] = s_t;
//            };

//            if (count > 0) while (sl.count() > count) sl.removeLast();
//            foreach (QString s, sl) {
//                status devSt = StringToStat(s);
//                bool onlyAlarm = deviceWindows[index]->onlyAlarm->isChecked();
//                QDate max = deviceWindows[index]->maxDate->date();
//                QDate min = deviceWindows[index]->minDate->date();
//                if (DEBUG_MODE) qDebug() << " ЧТЕНИЕ ПОЛНОЙ ИСТОРИИ МЕЖДУ " << min << " и " << max;
//                if (devSt.correct && devSt.date.date() <= max && devSt.date.date() >= min) {
//                    if (!onlyAlarm || (onlyAlarm && (devSt.stat == "alarm" || devSt.stat == "timeout"))) {
//                        deviceWindows[index]->historyList->addItem(new QListWidgetItem(s));
//                        //if (isCount && ++writes >= count) break;
//                    };
//                };
//            }
//            deviceWindows[index]->historyList->sortItems(Qt::DescendingOrder);
//            if (deviceWindows[index]->historyList->count() > 0) {
//                deviceWindows[index]->historyList->setCurrentRow(0);
//                _setDeviceWindowStatus(index,StringToStat(deviceWindows[index]->historyList->item(0)->text()));
//            } else {
//                _setDeviceWindowStatus(index,StringToStat(QString(" *ALARM* *TIMEOUT* *END*").prepend(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss"))));
//            };
//        } else {
//            if (DEBUG_MODE) qDebug() << "ЧТЕНИЕ ПОЛНОЙ ИСТОРИИ: НЕВОЗМОЖНО ОТКРЫТЬ ФАЙЛ";
//        };
//    };
//}
void UI::_loadFullHistory(quint16 index,qint16 count) {
    if (deviceArray.contains(index)) {
        deviceWindows[index]->historyList->clear();
        qint64 max = QDateTime(deviceWindows[index]->maxDate->date().addDays(1)).toMSecsSinceEpoch();
        qint64 min = QDateTime(deviceWindows[index]->minDate->date()).toMSecsSinceEpoch();
        bool alarms = deviceWindows[index]->onlyAlarm->isChecked();
        QSqlQuery query("",QSqlDatabase::database("con1"));
        QString queryString = QString("SELECT fulltext FROM status WHERE device = %1 %4 AND timestamp >= %2 AND timestamp < %3 ORDER BY timestamp DESC ").arg(index).arg(min).arg(max).arg((alarms) ? " AND `alarm` = 1 " : "");
        if (count > -1) queryString.append(QString("LIMIT %1").arg(count));
        if (query.exec(queryString)) {
            if (query.size()) {
                while (query.next()) {
                    QString ans = query.value(0).toString();
                   if (StringToStat(ans).correct) deviceWindows[index]->historyList->addItem(new QListWidgetItem(ans));
                };
            } else {
                qDebug() << "ЗАГРУЗКА ИСТОРИИ: НЕТ ЗАПИСЕЙ";
            };
        } else {
            qDebug() << "ЗАГРУЗКА ИСТОРИИ: ОШИБКА "  << query.lastError().text();
        };

        //deviceWindows[index]->historyList->sortItems(Qt::DescendingOrder);
        if (deviceWindows[index]->historyList->count() > 0) {
            deviceWindows[index]->historyList->setCurrentRow(0);
            _setDeviceWindowStatus(index,StringToStat(deviceWindows[index]->historyList->item(0)->text()));
        } else {
            qDebug() << "ЗАГРУЗКА ИСТОРИИ: НЕТ ЗАПИСЕЙ!";
            _setDeviceWindowStatus(index,StringToStat(QString(" *ALARM* *TIMEOUT* *END*").prepend(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss"))));
        };
    } else {
        qDebug() << "ЗАГРУЗКА ИСТОРИИ: НЕТ ТАКОГО ПРИБОРА";
    };
}
void UI::_startWaiting(quint16 index,QString text) {
    if (deviceWindows.contains(index)) {
        QString statusLab;
        if (text == "1") statusLab = "Включение";
        else if (text == "0") statusLab = "Выключение";
        else if (text == "5") statusLab = "Запрос состояния";
        else if (text == "9") statusLab = "Сброс часов";
        deviceWindows[index]->loadingStat->setText(statusLab + ".....");
        deviceWindows[index]->loading->setVisible(true);
        deviceWindows[index]->waiting = true;
    };
}

void UI::_stopWaiting(quint16 index) {
    if (deviceWindows.contains(index)) {
        deviceWindows[index]->loading->setVisible(false);
        deviceWindows[index]->waiting = false;
    };
}

void UI::readConStat() {
    foreach(quint16 key,deviceArray.keys()) {
        bool correct = true;
        QFile f(MAIN_PATH + "devices/" + QString::number(key) + "/packs");
        if (DEBUG_MODE) qDebug() << f.fileName();
        if (f.open(QIODevice::ReadOnly)) {
            QStringList sl =QString(f.readAll()).split("\r\n");
            if (deviceArray[key].type == "gsm") {
                if (sl[0].contains(QRegExp("^[0-9]{1,10}$")) && sl[1].contains(QRegExp("^[0-9]{1,10}$"))) {
                    deviceArray[key].packsBad = sl[0].toUInt();
                    deviceArray[key].packsAll = sl[1].toUInt();
                } else correct = false;
            } else if (deviceArray[key].type == "tcp") {
                if (sl[0].contains(QRegExp("^[0-9]{1,10}$")) && sl[1].contains(QRegExp("^[0-9]{1,10}$"))) {
                    deviceArray[key].packsBad = sl[0].toUInt();
                    deviceArray[key].packsAll = sl[1].toUInt();
                } else correct = false;
            } else if (deviceArray[key].type == "modbus") {
                if (sl[0].contains(QRegExp("^[0-9]{1,10}$")) && sl[1].contains(QRegExp("^[0-9]{1,10}$")) && sl[2].contains(QRegExp("^[0-9]{1,10}$"))) {
                    deviceArray[key].packsBad = sl[0].toUInt();
                    deviceArray[key].packsNull = sl[1].toUInt();
                    deviceArray[key].packsAll = sl[2].toUInt();
                } else correct = false;
            };
        } else {
            correct = false;
        };

        f.close();
        if (!correct) {
            deviceArray[key].packsAll = 0; deviceArray[key].packsNull = 0; deviceArray[key].packsBad = 0;
        };
    };
}

void UI::writeConStat() {
           QString i_s("INSERT OR IGNORE INTO `work` (`did`,`year`,`month`,`day`,`hour`,`work`,`uid`) VALUES (0,0,0,0,0,0,\"0000/00/00/00&d0\")"), u_s("UPDATE OR IGNORE `work` set work=work+1 WHERE `uid` = \"0000/00/1111\"");
    foreach(quint16 key, deviceArray.keys()) {
        QFile f(MAIN_PATH + "devices/" + QString::number(key) + "/packs");
        if (f.open(QIODevice::WriteOnly)) {
            if (deviceArray[key].type == "gsm") {
                f.write(QString().setNum(deviceArray[key].packsBad).toUtf8());
                f.write("\r\n");
                f.write(QString().setNum(deviceArray[key].packsAll).toUtf8());
            } else if (deviceArray[key].type == "tcp") {
                f.write(QString().setNum(deviceArray[key].packsBad).toUtf8());
                f.write("\r\n");
                f.write(QString().setNum(deviceArray[key].packsAll).toUtf8());
            } else if (deviceArray[key].type == "modbus") {
                f.write(QString().setNum(deviceArray[key].packsBad).toUtf8());
                f.write("\r\n");
                f.write(QString().setNum(deviceArray[key].packsNull).toUtf8());
                f.write("\r\n");
                f.write(QString().setNum(deviceArray[key].packsAll).toUtf8());
            };
        }
        f.close();


       if (deviceArray[key].type =="tcp" || deviceArray[key].type == "modbus") {
            i_s.append(QString(", (%1,%2,%3,%4,%5,%6,\"%7\")").arg(key).arg(QDate::currentDate().year()).arg(QDate::currentDate().month()).arg(QDate::currentDate().day()).arg(QDateTime::currentDateTime().time().hour()).arg(0).arg(QString("%1&d%2").arg(QDateTime::currentDateTime().toString("yyyy/MM/dd/hh")).arg(key)));
            if (deviceArray[key].working > 59) {
                u_s.append(QString(" OR uid=\"%1\"").arg(QString("%1&d%2").arg(QDateTime::currentDateTime().toString("yyyy/MM/dd/hh")).arg(key)));
                deviceArray[key].working = deviceArray[key].working - 60;
            }
       }
    }

    QSqlQuery query1("",QSqlDatabase::database("con1")),query2("",QSqlDatabase::database("con1"));
    if (!query1.exec(i_s) || !query2.exec(u_s)) {
        trayMessage("Запись наработки TCP и MODBUS","Запись не может быть выполнена: " + query1.lastError().text() + ", " + query2.lastError().text(),7);
    }
    if (DEBUG_MODE) qDebug() << "TCP & MDB WORKING: insert string: " << i_s;
    if (DEBUG_MODE) qDebug() << "TCP & MDB WORKING: update string: " << u_s;
}
