#include "ui.h"

//Декдирование PDU
smsData UI::decodePDU(QString pdu) {
    QString tmp1;
    QString pduTmp = pdu;
    QString tmp2,lil,cache,cou;
    smsData out;
    char chr;
    tmp2 = "unknown";
    int i,l;
    l = 0;
    tmp1.append(pdu.at(0)).append(pdu.at(1));

    if (tmp1.toInt(0,16) != 0) {
        pduTmp.remove(0,2);
        tmp2.clear();
        for(i = 1; i <= tmp1.toInt(0,16); i++) {
            tmp2.append(pduTmp.at(1)).append(pduTmp.at(0));
            pduTmp.remove(0,2);
        };
        tmp2.remove(0,2);
        tmp2.remove(QRegExp("\\D"));
        tmp2.prepend("+");
    } else {
        pduTmp.remove(0,2);
    };


    tmp1.clear();
    tmp2.clear();

    tmp1.append(pduTmp.at(0)).append(pduTmp.at(1));
    if (tmp1.toInt(0,16) & 3 != 0) goto succ;

    pduTmp.remove(0,2);

    tmp1.clear();
    tmp1.append(pduTmp.at(0)).append(pduTmp.at(1));
    pduTmp.remove(0,2);
    if (tmp1.toInt(0,16) > 16) {
       goto succ;
    } else {
        pduTmp.remove(0,2);
        for (i=1;i<= tmp1.toInt(0,16); i+=2) {
            tmp2.append(pduTmp.at(1)).append(pduTmp.at(0));
            pduTmp.remove(0,2);
        };
        tmp2.remove(QRegExp("\\D")).prepend("+");
    }

    out.number = tmp2;
    pduTmp.remove(0,2);
    tmp1.clear();
    tmp1.append(pduTmp.at(0)).append(pduTmp.at(1));
    pduTmp.remove(0,2);
    tmp2.clear();
    out.date = QDateTime();

    tmp2.append(pduTmp.at(7)).append(pduTmp.at(6)).append(QString(":"));
    tmp2.append(pduTmp.at(9)).append(pduTmp.at(8)).append(QString(":"));
    tmp2.append(pduTmp.at(11)).append(pduTmp.at(10));
    out.date.setTime(QTime().fromString(tmp2,"hh:mm:ss"));
    tmp2.append(QString(" "));
    tmp2.append(pduTmp.at(5)).append(pduTmp.at(4)).append(QString("/"));
    tmp2.append(pduTmp.at(3)).append(pduTmp.at(2)).append(QString("/"));
    tmp2.append(pduTmp.at(1)).append(pduTmp.at(0));

    out.date.setDate(QDate().fromString(tmp2.remove(QRegExp("[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\\s")),"dd/MM/yy"));
    if (out.date.date().year() < 2001) out.date = out.date.addYears(100);

    pduTmp.remove(0,14);
    tmp2.clear();
    tmp2.append(pduTmp.at(0)).append(pduTmp.at(1));
    tmp2.setNum(tmp2.toInt(0,16));
    pduTmp.remove(0,2);

    if (tmp1.toInt(0,16) & 8) {
        tmp1.clear();
        if (tmp2.toInt() % 2 == 1) {
         goto succ;
        } else {
            tmp2.clear();
            while (pduTmp.length() % 4 != 0) pduTmp.append("0");
            while (!pduTmp.isEmpty()) {
                tmp1.clear();
                tmp1.append(pduTmp.at(0)).append(pduTmp.at(1));
                if (tmp1.toInt() == 0) {
                    tmp1.clear();
                    tmp1.append(pduTmp.at(2)).append(pduTmp.at(3));
                    if (tmp1.toInt(0,16) % 256 != 17) {
                        chr = (char)qAbs(tmp1.toInt(0,16)) % 256;
                    } else {
                        chr = '_';
                    };
                    tmp2.append(chr);
                } else {
                };
                pduTmp.remove(0,4);
            };
        };

    } else {
        l = tmp2.toInt(0,16);
        tmp2.clear();
        i = 0;
        lil = "";
        while (pduTmp.length() != 0) {
                tmp1.clear();
                tmp1.append(pduTmp.at(0)).append(pduTmp.at(1));
                pduTmp.remove(0,2);
                tmp1.setNum(tmp1.toInt(0,16),2);
                while (tmp1.length() < 8) tmp1.prepend("0");
                cache = tmp1;
                cache.remove(0,i+1);
                cache.append(lil);
                if (cache.toInt(0,2) != 0x11) {
                    chr = cache.toInt(0,2);
                } else {
                    chr = '_';
                };
                tmp2.append(chr);
                cou.setNum(i+1);

                lil = tmp1;
                lil.truncate(i+1);
                if (lil.length() == 7) {
                    if (lil.toInt(0,2) != 0x11) {
                        chr = lil.toInt(0,2);
                    } else {
                        chr = '_';
                    };
                    tmp2.append(chr);
                    lil = "";
                }

        i = (i+1) % 7;
        };
    };

    out.address = out.number;
    out.address.remove(QRegExp("\\D"));
    out.text = tmp2;
    out.text.replace("\r\n"," ");
    out.text.replace("\r"," ");
    out.text.replace("\n"," ");
    succ:
    return out;
}
