#include "ui.h"

/****************************
    ФУНКЦИИ
 *******************************/

//Добавить сообщение в обрабтку для прибора
void UI::addQueryFromDevice(quint16 index, QString deviceCommand) {
        //Если таой прибор есть:
    if (deviceArray.contains(index)) {
        if (deviceWindows.contains(index))
            if (deviceWindows[index]->history->isVisible()) {
                turnHistory(index);
            };
        if (deviceArray[index].type == "gsm" && deviceArray[index].outCurrentStat == 0) {
            if (!enableSending) { trayMessage("Отправка сообщения","Модем не готов к отправе смс. Команда будет отправлена при восстановлении соединения.",3);};
            //Если тип прибора - gsm
            if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ЦИКЛ: CФОРМИРОВАННОЕ СООБЩЕНИЕ: " << encodePDU(deviceArray[index].address,deviceCommand);;
            //Добавляем информацию в статус
            deviceArray[index].outCurrentPDU = encodePDU(deviceArray[index].address,deviceCommand);
            deviceArray[index].outCurrentDate = QDateTime().currentDateTime();
            deviceArray[index].outCurrentRepeat = 0;
            deviceArray[index].outCurrentStat = 1;
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - *Отправка сообщения*");
            if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ЦИКЛ: СООБЩЕНИЕ ДОБАВЛЕНО!!!";
            //Мигаем формой
            //deviceWindows[index]->onButton->setDisabled(true);
            //deviceWindows[index]->offButton->setDisabled(true);
            //deviceWindows[index]->resetButton->setDisabled(true);
            //deviceWindows[index]->waiting = true;
            _startWaiting(index,deviceCommand);
            trayMessage("Сообщение добавлено","",6);

            //Делаем мигание формой

        } else if (deviceArray[index].type == "tcp") {
            if (deviceCommand == "1") {
                //tcpPort.makeChan(deviceArray[index].address,0x3F);
                tcpDev[index]->ch_on();
            } else if(deviceCommand == "0") {
                //tcpPort.makeChan(deviceArray[index].address,0x00);
                tcpDev[index]->ch_off();
            };
            _startWaiting(index,deviceCommand);
        } else if (deviceArray[index].type == "modbus") {
            //trayMessage("(" + QString::number(index) + ") " + deviceArray[index].name,"Модуль MODBUS RTU отсутствует. Нет связи с прибором.",3);
            emit mdbSetMessage(index,deviceCommand);
            _startWaiting(index,deviceCommand);
        } else {
            if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ЦИКЛ: НЕЛЬЗЯ ДОБАВИТЬ СООБЩЕНИЕ В ЦИКЛ!!!";
        };
    } else {
        if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ЦИКЛ: ТАКОГО ПРИБОРА НЕТ В СПИСКЕ!!!";
    };
}

//Цикл работы с GSM-приборами

void UI::gsmTimerCycle() {
    if (deviceArray.count() == 0) return;
//Выставляем количество сообщений
    quint16 count = 0;
    foreach(deviceStruct dev,deviceArray) if (dev.outCurrentStat != 0 && dev.type == "gsm") count ++;
    gsmSmsNumber->setText(QString::number(count));

    if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: СООБЩЕНИЙ В ОЧЕРЕДИ: " << count;

//Проверяем таймауты по статусу
    qint32 i = 0;
    foreach(i,deviceArray.keys()) {
        //Если прибор ничего не отправляет и его тип - GSM
        if ((deviceArray[i].type == "gsm") && (deviceArray[i].outCurrentStat == 0)) {
            //Если текущая дата больше, чем предыдущий статус + период доверия в минутах + 1 час
            if (QDateTime::currentMSecsSinceEpoch() > (deviceArray[i].stat.date.toMSecsSinceEpoch() + deviceArray[i].period * 60000 + (quint64)3600000)) {
                if (DEBUG_MODE) qDebug() << QDateTime::currentMSecsSinceEpoch() << deviceArray[i].stat.date.toMSecsSinceEpoch() << deviceArray[i].period * 60000 << (deviceArray[i].stat.date.toMSecsSinceEpoch() + deviceArray[i].period * 60000 + (quint64)3600000);
                if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: ТАЙМАУТ ПРИБОРА ПО ТАЙМЕРУ";
                smsData timeoutSmsData;
                timeoutSmsData.address = deviceArray[i].address;
                timeoutSmsData.date = QDateTime::currentDateTime();
                timeoutSmsData.number = deviceArray[i].address;
                timeoutSmsData.text = QString("*ALARM* *TIMEOUT* *END*");
                renderIncomingSms(timeoutSmsData);
                deviceArray[i].packsBad++;
            }
        }
    }

//Проверяем таймауты по ответам

    foreach (i, deviceArray.keys()) {
        if ((deviceArray[i].type == "gsm") && (deviceArray[i].outCurrentStat > 2)) {
            //Если текущая дата больше, чем время отправки + 15 минут
            if (QDateTime::currentDateTime() > QDateTime(deviceArray[i].outCurrentDate).addSecs(900)) {
                if (DEBUG_MODE) qDebug() << QDateTime::currentMSecsSinceEpoch() << (deviceArray[i].outCurrentDate.toMSecsSinceEpoch() + (quint64)900000);
                if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: ТАЙМАУТ ПРИБОРА ПО ОТВЕТУ";
                smsData timeoutSmsData;
                timeoutSmsData.address = deviceArray[i].address;
                timeoutSmsData.date = QDateTime::currentDateTime();
                timeoutSmsData.number = deviceArray[i].address;
                timeoutSmsData.text = QString("*ALARM* *TIMEOUT* *END*");
                renderIncomingSms(timeoutSmsData);
                deviceArray[i].packsBad++;
            };
        }
    }
//Ошибка на дурака
    if (!(deviceArray.contains(gsmIterator))) {
        gsmIterator = minIndex;
        if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: НЕТ ТАКОГО ПРИБОРА В СПИСКЕ";
        if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: итератор изменен на: " << gsmIterator;
        return;
    }

    //if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: устройство номер " << gsmIterator << " имеет состояние " << deviceArray[gsmIterator].outCurrentStat;
//Проверяем, отправлено ли сообщение
    if (deviceArray[gsmIterator].outCurrentStat == 2) {
        //Если сообщение отправляется
        if (DEBUG_MODE) qDebug() << "message to device num " << gsmIterator << " is sending";
        if ((deviceArray[gsmIterator].outCurrentDate.toMSecsSinceEpoch() + (quint64)60000) < QDateTime::currentDateTime().toMSecsSinceEpoch()) {
            trayMessage("Ошибка","Сообщение не отправлено. Будет произведена еще одна попытка.",3);
            deviceArray[gsmIterator].outCurrentStat = 1;
        };
    } else if (deviceArray[gsmIterator].outCurrentStat == 1) {
        //Если сообщение в очереди на отправку
        if (deviceArray[gsmIterator].outCurrentRepeat >= 3) {
            //Если с третьей попытки не отправилось ничего, пишем ругательства
            if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: сообщение не отправлено!!!";
            //И обнулаем его
            //deviceArray[gsmIterator].outCurrentDate = QDateTime();
            //deviceArray[gsmIterator].outCurrentNum = 0;
            //deviceArray[gsmIterator].outCurrentPDU = "";
            _setDeviceWindowStatus(gsmIterator,deviceArray[gsmIterator].stat);
            deviceArray[gsmIterator].outCurrentRepeat = 0;
            deviceArray[gsmIterator].outCurrentStat = 1;

            //deviceWindows[gsmIterator]->onButton->setDisabled(false);
            //deviceWindows[gsmIterator]->offButton->setDisabled(false);
            //deviceWindows[gsmIterator]->resetButton->setDisabled(false);
            //deviceWindows[gsmIterator]->waiting = false;
            //trayMessage("Ошибка","Невозможно отправить сообщение.");
            _moveGsmIterator();
        } else {
            //Если пришла наша очередь, отправляем сообщение
            currentPdu = deviceArray[gsmIterator].outCurrentPDU;
            deviceArray[gsmIterator].outCurrentRepeat++;
            deviceArray[gsmIterator].outCurrentStat = 2;
            deviceArray[gsmIterator].outCurrentNum = 0;
            deviceArray[gsmIterator].outCurrentDate = QDateTime().currentDateTime();
            if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: сообщение назначено на отправку";
        };
    } else if (deviceArray[gsmIterator].outCurrentStat == 4) {
        //Обнуляем поле
        //deviceArray[gsmIterator].outCurrentDate = QDateTime();
        deviceArray[gsmIterator].outCurrentNum = 0;
        deviceArray[gsmIterator].outCurrentPDU = "";
        deviceArray[gsmIterator].outCurrentRepeat = 0;
        _moveGsmIterator();
    } else if (deviceArray[gsmIterator].outCurrentStat == 0) {
        _moveGsmIterator();
    } else if (deviceArray[gsmIterator].outCurrentStat == 3) {
        _moveGsmIterator();
    };

}

void UI::_moveGsmIterator() {
    //gsmIterator
    quint16 tI =  (gsmIterator+1) % (maxIndex + 1);
    while (!(deviceArray.contains(tI) && deviceArray[tI].type == "gsm" && deviceArray[tI].outCurrentStat == 1) && tI != gsmIterator) tI =  (tI+1) % (maxIndex+1);
    gsmIterator = tI;
    if (DEBUG_MODE) qDebug() << "ЦИКЛ GSM: итератор изменен на: " << gsmIterator;
}

//Добавление сообщения в очередь
void UI::addSmsToQuery(quint16 index, QString command) {
    if (deviceArray.contains(index)) {
        //Если прибор GSM, добавляем данные
        if (deviceArray[index].type == "gsm") {
            deviceArray[index].outCurrentPDU = encodePDU(deviceArray[index].address,command);
            deviceArray[index].outCurrentDate = QDateTime().currentDateTime();
            deviceArray[index].outCurrentRepeat = 0;
            deviceArray[index].outCurrentStat = 1;
            if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ОЧЕРЕДЬ: СООБЩЕНИЕ ДОБАВЛЕНО!!!";
            //deviceWindows[index]->onButton->setDisabled(true);
            //deviceWindows[index]->offButton->setDisabled(true);
            //deviceWindows[index]->resetButton->setDisabled(true);
            //deviceWindows[index]->waiting = true;
            _startWaiting(index,command);
        };
    } else {
        if (DEBUG_MODE) qDebug() << "ДОБАВЛЕНИЕ СООБЩЕНИЯ В ОЧЕРЕДЬ: ТАКОГО ПРИБОРА НЕТ В СПИСКЕ!!!";
    };
}

void UI::clearGsmQueue() {
    foreach(quint16 index,deviceArray.keys()) {
        deviceArray[index].outCurrentDate = QDateTime();
        deviceArray[index].outCurrentNum = 0;
        deviceArray[index].outCurrentPDU = "";
        _setDeviceWindowStatus(index,deviceArray[index].stat);
        deviceArray[index].outCurrentRepeat = 0;
        deviceArray[index].outCurrentStat = 0;

        //deviceWindows[index]->onButton->setDisabled(false);
        //deviceWindows[index]->offButton->setDisabled(false);
        //deviceWindows[index]->resetButton->setDisabled(false);
        //deviceWindows[index]->waiting = false;
        _stopWaiting(index);
    };
    trayMessage("Выполнено","Очередь сообщений очищена",7);
}


//***********************************TCP - цикл*********************************************

void UI::tcpTimerCycle() {
    foreach(quint16 index,deviceArray.keys()) {
        if (deviceArray[index].type == "tcp") {
            if (QDateTime(deviceArray[index].tcpLastReport).addSecs(tcpWaiting) < QDateTime().currentDateTime()) {
                if (DEBUG_MODE) qDebug() << deviceArray[index].stat.date << QDateTime().currentDateTime();
                renderIncomingTcpQuey(deviceArray[index].address,QString("").append(" *ALARM* *TIMEOUT* *END*"));
            }
        };
    };
    setTcpStat();
}


