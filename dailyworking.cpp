#include "dailyworking.h"
#include "ui_dailyworking.h"

specialScene::specialScene(QWidget *par) : QGraphicsScene(par) {

}

specialScene::~specialScene() {

}

void specialScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *ev) {
//    qDebug() << items();
//    if (itemAt(ev->buttonDownPos(Qt::LeftButton),QTransform()) != 0) {
//        qDebug() << "scene event: object exists!";
//        emit objClicked(itemAt(ev->buttonDownPos(Qt::LeftButton),QTransform()));
//    }
    qDebug() << ev->buttonDownScenePos(Qt::LeftButton);
    emit clicked(ev->buttonDownScenePos(Qt::LeftButton));
}

dailyWorking::dailyWorking(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dailyWorking)
{
    ui->setupUi(this);

    menu = new QMenu(ui->view);
    showMax = new QAction("Отобразить сутки с максимальным значением",ui->view);
    showMin = new QAction("Отобразить сутки с минимальным значением",ui->view);
    menu->addAction(showMax);
    menu->addAction(showMin);

    ui->view->setContextMenuPolicy(Qt::CustomContextMenu);

    statW = new statWidget(this);

    pos_x = 1110;
    pos_y = 430;
    off_x = 80;
    off_y = 80;
    off_y_x = 100;
    off_y_y = 0;

    for (int i=0;i<24;i++) {
        maxDate.append(QDate(2000,1,1)); minDate.append(QDate(2000,1,1));
        maxVal.append(60); minVal.append(0); lastVal.append(-1); averVal.append(30);
    }

    from = new calWid(ui->tab);
    from->setPos(ui->groupBox_3->x() + 40, ui->groupBox_3->y() + 51);
    from->setDate(QDate(2014,1,1));


    to = new calWid(ui->tab);
    to->setPos(ui->groupBox_3->x() + 190, ui->groupBox_3->y() + 51);
    to->setDate(QDate::currentDate());


    day = new calWid(ui->tab);
    day->setPos(ui->groupBox->x()+ 20, ui->groupBox->y() + 51);
    day->setDate(QDate::currentDate());

    qDebug() << "DATEPICKER SIZES:" << from->rect() << to->rect() << day->rect();

    ui->showButton->setVisible(false);

    monthNames << "" << "янв" << "фев" << "март" << "апр" << "май" << "июнь" << "июль" << "авг" << "сен" << "окт" << "ноя" << "дек";

    maxPen.setColor(QColor(255,222,0));
    lastPen.setColor(QColor(98,98,255));
    lastPen.setStyle(Qt::SolidLine);
    lastBrush.setColor(QColor(98,98,255));
    lastBrush.setStyle(Qt::SolidPattern);
    graphPen.setColor(QColor(200,200,200));
    graphPen.setStyle(Qt::DashLine);
    maxPen.setStyle(Qt::SolidLine);
    maxPen.setWidth(1);
    tagPen.setColor(QColor(100,100,100));
    tagPen.setStyle(Qt::SolidLine); activeRectPen.setWidth(1);passiveRectPen.setWidth(1);
    activeRectPen.setColor(QColor(255,153,98));
    activeRectPen.setStyle(Qt::SolidLine);
    passiveRectPen.setColor(QColor(200,200,200));
    passiveRectPen.setStyle(Qt::DashLine);
    activeRectBrush.setColor(QColor(255,153,98));
    activeRectBrush.setStyle(Qt::SolidPattern);
    passiveRectBrush.setColor(QColor(200,200,200));
    passiveRectBrush.setStyle(Qt::BDiagPattern);

    maxBrush.setColor(QColor(255,222,0));
    maxBrush.setStyle(Qt::SolidPattern);


    tagLine = new QGraphicsLineItem(off_x + 5,off_y - 5,off_x + 5,300 + off_y);
    tagLine->setPen(tagPen);
    scene.addItem(tagLine);

    for (int i=0;i<7;i++) {
        datas.append(new QGraphicsTextItem());
        scene.addItem(datas.last());
        datas.last()->setPos(off_x - 25,290 + off_y - (50 * i));
        datas.last()->setFont(QFont("Verdana",7));
        datas.last()->setTextWidth(30);
        datas.last()->setHtml(QString("<center><font align=right style='display: block; text-align: right; color: #333;'>%1</font></center>").arg(10 * i));

        x_lines.append(new QGraphicsLineItem(off_x,300 + off_y -(50*i),960 + off_x,300 + off_y-(50*i)));
        x_lines.last()->setPen(graphPen);
        scene.addItem(x_lines.last());
    }

    for (int i=0;i<25;i++) {
        y_lines.append(new QGraphicsLineItem(40*i+off_x + 5,off_y,40*i+off_x + 5,300 + off_y));
        y_lines.last()->setPen(graphPen);
        scene.addItem(y_lines.last());
    }

    for (int i=0;i<24;i++) {
        lines.append(new QGraphicsRectItem(40*i+off_x + 10,off_y + 20,40*i+off_x + 40,300 + off_y));
        lines.last()->setPen(maxPen);
        lines.last()->setBrush(maxBrush);
        lines.last()->setZValue(i);
        lines.last()->setData(100,QVariant(i));
        scene.addItem(lines.last());

        rects.append(new QGraphicsRectItem());
        rects.last()->setRect(40*i+off_x + 10,off_y + 150,30,150);
        rects.last()->setBrush(activeRectBrush);
        rects.last()->setPen(activeRectPen);
        rects.last()->setZValue(24+i);
        scene.addItem(rects.last());
        rects.last()->setData(100,QVariant(i));
        rects.last()->setAcceptTouchEvents(true);

        last.append(new QGraphicsRectItem(40*i + off_x + 15,off_y + 150,20,150));
        last.last()->setPen(lastPen);
        last.last()->setBrush(lastBrush);
        last.last()->setZValue(72+i);
        last.last()->setData(100,QVariant(i));
        scene.addItem(last.last());

        underText.append(new QGraphicsTextItem());
        scene.addItem(underText.last());
        underText.last()->setHtml("<center style='color: #333'>" + QString::number(i) + "</center>");
        underText.last()->setX(40*i +off_x - 10);
        underText.last()->setY(297 + off_y);
        underText.last()->setTextWidth(30);
        underText.last()->setFont(QFont("Verdana",7));

        texts.append(new QGraphicsTextItem());
        scene.addItem(texts.last());
        texts.last()->setHtml("<center><font color='#a63413'>50</font></center>");
        texts.last()->setX(40*i+off_x + 10);
        texts.last()->setY(off_y + 145);
        texts.last()->setTextWidth(30);
        texts.last()->setZValue(96+i);
        texts.last()->setData(100,QVariant(i));
        texts.last()->setFont(QFont("Verdana",8));

        max_texts.append(new QGraphicsTextItem());
        scene.addItem(max_texts.last());
        max_texts.last()->setHtml("<center><font color='#a63413'>50</font></center>");
        max_texts.last()->setX(40*i+off_x + 10);
        max_texts.last()->setY(off_y + 145);
        max_texts.last()->setTextWidth(30);
        max_texts.last()->setZValue(96+i);
        max_texts.last()->setData(100,QVariant(i));
        max_texts.last()->setFont(QFont("Verdana",8));

    }

    x_scale = new QGraphicsTextItem();
    x_scale->setHtml("<font color='#999'>Работа за 1 час, мин</font>");
    x_scale->setRotation(-90);
    x_scale->setX(off_x - 40);
    x_scale->setY(off_y + 220);
    x_scale->setFont(QFont("Verdana",9));
    scene.addItem(x_scale);

    y_scale = new QGraphicsTextItem();
    y_scale->setHtml("<font color='#999'>Часы</font>");
    y_scale->setX(460 + off_x);
    y_scale->setY(off_y + 310);
    y_scale->setFont(QFont("Verdana",9));
    scene.addItem(y_scale);
    scene.setBackgroundBrush(QBrush(QColor(255,255,255)));
    scene.setSceneRect(0,0,pos_x,pos_y);
    ui->view->setScene(&scene);
    ui->view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);




    m_v_lines.append(new QGraphicsLineItem(off_x -5, off_y, off_x -5, off_y + 300));
    m_v_lines.last()->setPen(graphPen);
    m_scene.addItem(m_v_lines.last());
    m_max_line = new QGraphicsLineItem(off_x-5,off_y,off_x + 960, off_y);
    m_max_line->setPen(maxPen);
    m_max_text = new QGraphicsTextItem();
    //m_max_text->setHtml("<center><font style='color: rgb(128,100,0)'>max</font></center><font style='color: rgb(128,100,0)'>0 м3</font><center></center>");
    //m_max_text->setPos(off_x + 930, off_y + 5);
    //m_scene.addItem(m_max_line);
    //m_scene.addItem(m_max_text);
    m_x_scale = new QGraphicsTextItem();
    m_x_scale->setHtml("<font color='#999'>Наработка в день, м3</font>");
    m_x_scale->setRotation(-90);
    m_x_scale->setPos(off_x - 60,off_y + 240);
    m_x_scale->setFont(QFont("Verdana",9));
    m_scene.addItem(m_x_scale);

    m_y_scale = new QGraphicsTextItem();
    m_y_scale->setHtml("<font color='#000'>Дни месяца</font>");
    m_y_scale->setX(460 + off_x);
    m_y_scale->setY(off_y + 320);
    m_y_scale->setFont(QFont("Verdana",9));
    m_scene.addItem(m_y_scale);

    for (int i=0;i<5;i++) {
        m_h_lines.append(new QGraphicsLineItem(off_x, off_y + (75 * i),off_x + 925,off_y + (75 * i)));
        m_h_lines.last()->setPen(graphPen);
        m_scene.addItem(m_h_lines.last());

        m_h_texts.append(new QGraphicsTextItem());
        m_h_texts.last()->setTextWidth(50);
        m_h_texts.last()->setPos(off_x - 40,off_y+(75*i) - 10);
        m_h_texts.last()->setHtml(QString("<div style='color: #666;text-align: right'>%1%</div>").arg(25 * (4-i)));
        m_scene.addItem(m_h_texts.last());
    }

    for (int i=0;i<31;i++) {
        m_rects.append(new QGraphicsRectItem(30 * i + off_x, off_y + 150, 20, 150));
        m_rects.last()->setPen(QPen(QColor(255,153,98)));
        m_rects.last()->setBrush(QBrush(QColor(255,153,98)));
        m_scene.addItem(m_rects.last());

        m_texts.append(new QGraphicsTextItem());
        m_texts.last()->setTextWidth(30);
        m_texts.last()->setHtml(QString("<center><font color='#666'>%1</font></center>").arg(i+1));
        m_texts.last()->setPos(30*i + off_x - 5,off_y + 301);
        m_scene.addItem(m_texts.last());

        m_values.append(new QGraphicsTextItem());
        m_values.last()->setTextWidth(30);
        m_values.last()->setHtml("<center><font color='#a63413'>50</font></center>");
        m_values.last()->setPos(30*i + off_x - 5,off_y + 130);
        m_scene.addItem(m_values.last());

        m_v_lines.append(new QGraphicsLineItem(30 * i + off_x + 25, off_y, 30 * i + off_x + 25, off_y + 300));
        m_v_lines.last()->setPen(graphPen);
        m_scene.addItem(m_v_lines.last());


    }

    y_v_lines.append(new QGraphicsLineItem(off_x + off_y_x  -5, off_y, off_x  + off_y_x -5, off_y + 300));
    y_v_lines.last()->setPen(graphPen);
    y_scene.addItem(y_v_lines.last());
    y_max_line = new QGraphicsLineItem(off_x + off_y_x -5,off_y,off_x + off_y_x  + 760, off_y);
    y_max_line->setPen(maxPen);
    y_max_text = new QGraphicsTextItem();
    //y_max_text->setHtml("<center><font style='color: rgb(128,100,0)'>max</font></center><font style='color: rgb(128,100,0)'>0 м3</font><center></center>");
    //y_max_text->setPos(off_x + off_y_x  + 730, off_y + 5);
    //y_scene.addItem(y_max_line);
    //y_scene.addItem(y_max_text);
    y_x_scale = new QGraphicsTextItem();
    y_x_scale->setHtml("<font color='#999'>Наработка в месяц, м3</font>");
    y_x_scale->setRotation(-90);
    y_x_scale->setPos(off_x  + off_y_x - 60,off_y + 240);
    y_x_scale->setFont(QFont("Verdana",9));
    y_scene.addItem(y_x_scale);

    y_y_scale = new QGraphicsTextItem();
    y_y_scale->setHtml("<font color='#000'>Месяцы</font>");
    y_y_scale->setX(460 + off_x);
    y_y_scale->setY(off_y + 320);
    y_y_scale->setFont(QFont("Verdana",9));
    y_scene.addItem(y_y_scale);

    for (int i=0;i<5;i++) {
        y_h_lines.append(new QGraphicsLineItem(off_x + off_y_x , off_y + (75 * i),off_x + off_y_x  + 715,off_y + (75 * i)));
        y_h_lines.last()->setPen(graphPen);
        y_scene.addItem(y_h_lines.last());

        y_h_texts.append(new QGraphicsTextItem());
        y_h_texts.last()->setTextWidth(60);
        y_h_texts.last()->setPos(off_x + off_y_x  - 40,off_y+(75*i) - 10);
        y_h_texts.last()->setHtml(QString("<div style='color: #666;text-align: right'>%1%</div>").arg(25 * (4-i)));
        y_scene.addItem(y_h_texts.last());
    }

    for (int i=0;i<12;i++) {
        y_rects.append(new QGraphicsRectItem(60 * i + off_x + off_y_x , off_y + 150, 50, 150));
        y_rects.last()->setPen(QPen(QColor(255,153,98)));
        y_rects.last()->setBrush(QBrush(QColor(255,153,98)));
        y_scene.addItem(y_rects.last());

        y_texts.append(new QGraphicsTextItem());
        y_texts.last()->setTextWidth(60);
        y_texts.last()->setHtml(QString("<center><font color='#666'>%1</font></center>").arg(monthNames[i+1]));
        y_texts.last()->setPos(60*i + off_x  + off_y_x - 5,off_y + 301);
        y_scene.addItem(y_texts.last());

        y_values.append(new QGraphicsTextItem());
        y_values.last()->setTextWidth(60);
        y_values.last()->setHtml("<center><font color='#a63413'>50</font></center>");
        y_values.last()->setPos(60*i + off_x  + off_y_x - 5,off_y + 130);
        y_scene.addItem(y_values.last());

        y_v_lines.append(new QGraphicsLineItem(60 * i + off_x + off_y_x  + 55, off_y, 60 * i + off_x + off_y_x  + 55, off_y + 300));
        y_v_lines.last()->setPen(graphPen);
        y_scene.addItem(y_v_lines.last());


    }

    ui->m_month->setCurrentIndex(QDate::currentDate().month() - 1);
    ui->m_year->setValue(QDate::currentDate().year());

    ui->y_year->setValue(QDate::currentDate().year());

    m_scene.setBackgroundBrush(QBrush(QColor(255,255,255)));
    m_scene.setSceneRect(0,0,pos_x,pos_y);
    ui->m_view->setScene(&m_scene);

    y_scene.setBackgroundBrush(QBrush(QColor(255,255,255)));
    y_scene.setSceneRect(0,0,pos_x,pos_y);
    ui->y_view->setScene(&y_scene);

    ui->m_show->setVisible(false);
    ui->y_show->setVisible(false);

    ui->m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->y_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->y_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    ui->m_help->hide();
    ui->y_help->hide();

    connect(ui->showButton,SIGNAL(clicked()),this,SLOT(sendDailyQuery()));
    connect(ui->saveButton,SIGNAL(clicked()),this,SLOT(sendSaveQuery()));
    connect(from,SIGNAL(dateChanged(QDate)),this,SLOT(changeMaxRange(QDate)));
    connect(to,SIGNAL(dateChanged(QDate)),this,SLOT(changeMinRange(QDate)));
    connect(ui->showAver,SIGNAL(clicked()),this,SLOT(_averChecked()));
    connect(ui->showMax,SIGNAL(clicked()),this,SLOT(_maxChecked()));
    connect(ui->showLast,SIGNAL(clicked()),this,SLOT(_lastChecked()));
    connect(from,SIGNAL(dateChanged(QDate)),this,SLOT(_setDate()));
    connect(to,SIGNAL(dateChanged(QDate)),this,SLOT(_setDate()));
    connect(ui->m_show,SIGNAL(clicked()),this,SLOT(_setMonthQuery()));
    connect(ui->m_save,SIGNAL(clicked()),this,SLOT(m_save()));
    connect(to,SIGNAL(dateChanged(QDate)),ui->showButton,SIGNAL(clicked()));
    connect(from,SIGNAL(dateChanged(QDate)),ui->showButton,SIGNAL(clicked()));
    connect(ui->m_month,SIGNAL(currentIndexChanged(int)),ui->m_show,SIGNAL(clicked()));
    connect(ui->m_year,SIGNAL(valueChanged(int)),ui->m_show,SIGNAL(clicked()));
    connect(ui->y_year,SIGNAL(valueChanged(int)),ui->y_show,SIGNAL(clicked()));
    connect(ui->y_show,SIGNAL(clicked()),this,SLOT(_setYearQuery()));
    connect(ui->y_save,SIGNAL(clicked()),this,SLOT(y_save()));
    connect(day,SIGNAL(dateChanged(QDate)),ui->showButton,SIGNAL(clicked()));
    connect(&scene,SIGNAL(objClicked(QGraphicsItem*)),this,SLOT(catchItem(QGraphicsItem*)));
    connect(&scene,SIGNAL(clicked(QPointF)),this,SLOT(catchClick(QPointF)));
    connect(ui->view,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customMenu(QPoint)));

    connect(statW,SIGNAL(showMinDate(int,QDate)),this,SLOT(sh_min_pr(int)));
    connect(statW,SIGNAL(showMaxDate(int,QDate)),this,SLOT(sh_max_pr(int)));

    setWindowFlags((windowFlags() & ~Qt::WindowMaximizeButtonHint & ~Qt::WindowMinimizeButtonHint)| Qt::WindowStaysOnTopHint);
}

dailyWorking::~dailyWorking()
{
    while (rects.count()) {scene.removeItem(rects.last()); delete rects.last(); rects.removeLast();};
    while (lines.count()) {scene.removeItem(lines.last()); delete lines.last(); lines.removeLast();};
    while (last.count()) {scene.removeItem(last.last()); delete last.last(); last.removeLast();};
    while (texts.count()) {scene.removeItem(texts.last()); delete texts.last(); texts.removeLast();};
    while (max_texts.count()) {scene.removeItem(max_texts.last()); delete max_texts.last(); max_texts.removeLast();};
    while (datas.count()) {scene.removeItem(datas.last()); delete datas.last(); datas.removeLast();};
    while (x_lines.count()) {scene.removeItem(x_lines.last()); delete x_lines.last(); x_lines.removeLast();};
    while (y_lines.count()) {scene.removeItem(y_lines.last()); delete y_lines.last(); y_lines.removeLast();};
    while (underText.count()) {scene.removeItem(underText.last()); delete underText.last(); underText.removeLast();};
    while (m_texts.count()) {m_scene.removeItem(m_texts.last()); delete m_texts.last(); m_texts.removeLast();};
    while (m_rects.count()) {m_scene.removeItem(m_rects.last()); delete m_rects.last(); m_rects.removeLast();};
    while (m_values.count()) {m_scene.removeItem(m_values.last()); delete m_values.last(); m_values.removeLast();};
    while (m_v_lines.count()) {m_scene.removeItem(m_v_lines.last()); delete m_v_lines.last(); m_v_lines.removeLast();};
    while (m_h_lines.count()) {m_scene.removeItem(m_h_lines.last()); delete m_h_lines.last(); m_h_lines.removeLast();};
    while (m_h_texts.count()) {m_scene.removeItem(m_h_texts.last()); delete m_h_texts.last(); m_h_texts.removeLast();};
    while (y_texts.count()) {y_scene.removeItem(y_texts.last()); delete y_texts.last(); y_texts.removeLast();};
    while (y_rects.count()) {y_scene.removeItem(y_rects.last()); delete y_rects.last(); y_rects.removeLast();};
    while (y_values.count()) {y_scene.removeItem(y_values.last()); delete y_values.last(); y_values.removeLast();};
    while (y_v_lines.count()) {y_scene.removeItem(y_v_lines.last()); delete y_v_lines.last(); y_v_lines.removeLast();};
    while (y_h_lines.count()) {y_scene.removeItem(y_h_lines.last()); delete y_h_lines.last(); y_h_lines.removeLast();};
    while (y_h_texts.count()) {y_scene.removeItem(y_h_texts.last()); delete y_h_texts.last(); y_h_texts.removeLast();};

    to->close(); delete to;
    from->close(); delete from;
    day->close(); delete day;
    scene.removeItem(tagLine);delete tagLine;
    scene.removeItem(x_scale);delete x_scale;
    scene.removeItem(y_scale);delete y_scale;

    delete ui;
}

void dailyWorking::setLines(QList<int> levels) {

}

void dailyWorking::setRects(QList<int> rectLevels) {

}


void dailyWorking::setGraph(QList<double> l_lev, QList<QDate> m_date, QList<double> min_lev, QList<QDate> min_date, QList<double> r_lev, QList<double> la_lev, double working, double percent) {
    if (l_lev.length() == 24 && r_lev.length() == 24) {
        QList<float> _l_lev, _r_lev, _la_lev, _min_lev;
        int max = -1;
        for (int i=0;i<24;i++) if (l_lev[i] > max) max = 60;
        for (int i=0;i<24;i++) {
            _l_lev.append((float)(l_lev[i] / max) * 100);
            _r_lev.append((float)(r_lev[i] / max) * 100);
            _la_lev.append((float)(la_lev[i] / max) * 100);
            _min_lev.append((float)(min_lev[i] / max) * 100);

            maxVal[i] = l_lev[i];
            minVal[i] = min_lev[i];
            averVal[i] = r_lev[i];
            lastVal[i] = la_lev[i];
        }

//        if (max == -1) {
//           for (int i=0;i<7;i++) datas.at(i)->setHtml(QString("<font style='text-align: right; color=000'>%1</font>").arg("-"));
//        } else {
//            for (int i=0;i<7;i++) datas.at(i)->setHtml(QString("<font style='text-align: right; color=000'>%1</font>").arg((int)(max * 0.25 * i)));
//        }

        for (int i=0;i<24;i++) {
            if (l_lev.at(i) >= 0) {
                QString te; te = QString("<center><font color='#a61a13'>%1</font></center>").arg(l_lev.at(i));
                max_texts.at(i)->setHtml(te);
                max_texts.at(i)->setY(300 + off_y - 20 - 3 *_l_lev.at(i));
                max_texts.at(i)->setVisible(true);
                lines.at(i)->setRect(40*i+off_x + 10,300 + off_y -(3*_l_lev.at(i)),30,3*_l_lev.at(i));
                lines.at(i)->setVisible(true);
                maxDate[i] = m_date[i];
            } else {
                max_texts.at(i)->setHtml("<center><font color='#a61a13'>н/д</font></center>");
                max_texts.at(i)->setVisible(false);
                max_texts.at(i)->setY(300 + off_y - 20);
                lines.at(i)->setToolTip("<center><font color='#a61a13'>н/д</font></center>");
                lines.at(i)->setRect(40*i+off_x + 10,300 + off_y,30,0);
                lines.at(i)->setVisible(false);
                maxDate[i] = QDate(2000,1,1);
            }

            if (min_lev[i] <= 61) {
                minDate[i] = min_date[i];
            } else {
                minDate[i] = QDate(2000,1,1);
            }

            if (r_lev.at(i) >= 0) {
                QString tooltip; //tooltip = QString("<center>%3:00 - %4:00</center><center><font color='#a61a13'>Максимальная наработка(за %6) - %1мин </font></center><center><font color='#009b80'>Минимальная наработка(за %8) - %9мин </font></center><center><font color='#a63413'><a href=''>Среднее значение -  %2мин</a></font></center><center><font style='color: #6262ff'>за %7 -  %5</font></center><center><font style='color:#999;font-style:italic; font-size: 0.8em'><br>Сделайте <b>двойной щелчок левой кнопкой</b> мыши<br>для отображения <br> подробной информации</font></center>").arg(l_lev.at(i)).arg((int)r_lev.at(i)).arg(i).arg((i+1) % 24).arg((la_lev[i] == -1) ? "н/д" : QString::number(la_lev[i],'f',0).append("мин")).arg(m_date[i].toString("dd-MM-yyyy")).arg(day->date().toString("dd/MM/yyyy")).arg(min_date[i].toString("dd/MM/yyyy")).arg(min_lev[i]);
                tooltip = QString("<center><font style='color:#999;font-style:italic; font-size: 0.8em'><b>двойной щелчок</b>для отображения sинформации</font></center>");
                QString te; te = QString("<center><font color='#a61a13'>%1</font></center>").arg(l_lev.at(i));
                QString tm; tm = QString("<center><font color='#a63413'>%1</font></center>").arg((int)r_lev.at(i));
                rects.at(i)->setPen(activeRectPen);
                rects.at(i)->setBrush(activeRectBrush);
                rects.at(i)->setRect(40*i+off_x + 10,300 + off_y -(_r_lev.at(i)*3),30,3*_r_lev.at(i));
                rects.at(i)->setToolTip(tooltip);
                lines.at(i)->setToolTip(tooltip);
                last.at(i)->setToolTip(tooltip);
                texts.at(i)->setHtml(tm);
                texts.at(i)->setY(300 + off_y - 3 *_r_lev.at(i));
            } else {
                rects.at(i)->setPen(passiveRectPen);
                rects.at(i)->setBrush(passiveRectBrush);
                rects.at(i)->setRect(40*i+off_x + 10,off_y,30,300);
                rects.at(i)->setToolTip("<center><font color='#a61a13'>н/д</font></center>");
                lines.at(i)->setToolTip("<center><font color='#a61a13'>н/д</font></center>");
                last.at(i)->setToolTip("<center><font color='#a61a13'>н/д</font></center>");
                texts.at(i)->setHtml("<center><font color='#a61a13'>н/д</font></center>");
                texts.at(i)->setY(300 + off_y - 20);
                max_texts.at(i)->setVisible(false);
            };

            if (la_lev.at(i) >= 0 && ui->showLast->isChecked()) {
                last.at(i)->setRect(40*i+off_x + 15, 300 + off_y - (_la_lev.at(i) * 3),20,_la_lev.at(i) * 3);
                last.at(i)->setVisible(true);
            } else {
                last.at(i)->setRect(40*i+off_x + 15, 300 + off_y ,20,0);
                last.at(i)->setVisible(false);
            }
        }

        if (working > -1) {
            ui->work->setText(QString::number(flow * working,'f',2));
        } else {
            ui->work->setText("...");
        }
        ui->percent->setText(QString::number(percent,'f',1));
        setHeader();
    }
}

void dailyWorking::testGraph(int lev) {
    QList<double> l_l,r_l,la_l; QList<QDate> d;
    for (int i=0;i<24;i++) {
        d.append(QDate(2000,1,1));
        if (i<2) {
            l_l.append(-1); r_l.append(-1); la_l.append(-1);
        } else {
            l_l.append((lev + qrand() % 100) % 100);
            la_l.append((lev + qrand() % 100) % 100);
            r_l.append((lev + qrand() % 100) % 100);
            if (l_l.last() < r_l.last()) l_l.last() = r_l.last();
            if (la_l.last() < r_l.last()) la_l.last() = r_l.last();
        };
    }
    //setGraph(l_l,d,r_l,la_l);
}

void dailyWorking::changeMaxRange(QDate min) {
}

void dailyWorking::changeMinRange(QDate max) {

}

void dailyWorking::sendDailyQuery() {
    if (from->date() > to->date()) {
        QMessageBox::warning(this,"Минимальная дата больше максимальной!", "Введите корректные значения минимальной и максимальной дат");
    } else emit dailyQuery((quint16)did,from->date(),to->date(),day->date());
}

void dailyWorking::showOrHide() {
    for (int i=0;i<24;i++) {
        rects.at(i)->setVisible(ui->showAver->isChecked());
        texts.at(i)->setVisible(ui->showAver->isChecked());
        lines.at(i)->setVisible(ui->showMax->isChecked());
        last.at(i)->setVisible(ui->showLast->isChecked());
        max_texts.at(i)->setVisible(ui->showMax->isChecked());
    }
    ui->dayDate->setVisible(ui->showLast->isChecked());
    setHeader();
}

void dailyWorking::_averChecked() {
    if (!ui->showAver->isChecked() && !ui->showMax->isChecked() && !ui->showLast->isChecked()) ui->showLast->setChecked(true);
    showOrHide();
}

void dailyWorking::_maxChecked() {
    if (!ui->showAver->isChecked() && !ui->showMax->isChecked() && !ui->showLast->isChecked()) ui->showAver->setChecked(true);
    showOrHide();
}

void dailyWorking::_lastChecked() {
    if (!ui->showAver->isChecked() && !ui->showMax->isChecked() && !ui->showLast->isChecked()) ui->showAver->setChecked(true);
    showOrHide();
}


void dailyWorking::setHeader() {
    QString out(""),out2("");
    out += QString("с %1 по %2").arg(from->date().toString("dd/MM/yyyy")).arg(to->date().toString("dd/MM/yyyy"));
    out2 += QString("%1").arg(day->date().toString("dd/MM/yyyy"));
    ui->header->setText(out);
    ui->dailyHeader->setText(out2);
}

bool dailyWorking::exportToCsv(QMap<QString, csvData> data, QList<double>av,QList<double> max, QString name) {
    bool out = true;
    QString fileName = QFileDialog::getSaveFileName(this,"Сохранить как...",QString("Наработка %3 с %1 по %2.csv").arg(from->date().toString("yyyy.MM.dd")).arg(to->date().toString("yyyy.MM.dd")).arg(name),"*.csv");
    if (fileName.length()) {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) out = false;
        file.write(QString("Суточная наработка %1\r\n").arg(name).toLocal8Bit());
        file.write(QString("Дата").toLocal8Bit());
        for (int i=0;i<24;i++) {
            file.write(QString(";%1.00-%2.00").arg(i).arg((i+1)%24).toLocal8Bit());
        }
        file.write(QString("\r\n\r\n").toLocal8Bit());
        foreach(csvData d,data) {
            QString str;
            str.append(d.date);
            for (int i=0;i<24;i++) str.append(";" + ((d.working[i] > -1) ? QString::number(d.working[i]) : "н/д"));
            str.append("\r\n");
            file.write(str.toLocal8Bit());
        }
        file.write(QString("\r\nСреднее значение").toLocal8Bit());
        for (int i=0;i<24;i++) file.write(QString::number(av[i],'f',3).prepend(";").toLocal8Bit());
        file.write(QString("\r\nМаксимальное значение").toLocal8Bit());
        for (int i=0;i<24;i++) file.write(QString::number((int)max[i]).prepend(";").toLocal8Bit());

        file.close();
    };
    return out;
}

void dailyWorking::sendSaveQuery() {
    if (from->date() > to->date()) {
        QMessageBox::warning(this,"Минимальная дата больше максимальной!", "Введите корректные значения минимальной и максимальной дат");
    } else emit saveQuery((quint16)did,from->date(),to->date());
}

void dailyWorking::_setDate() {
    day->setDateRange(from->date(),to->date());
}

void dailyWorking::_setMonthQuery() {
    emit monthQuery(did,ui->m_month->currentIndex() + 1,ui->m_year->value());
}

void dailyWorking::setMGraph(QList<double> lev, QList<double> per,double flow,int work) {
    QDate cur(ui->m_year->value(),ui->m_month->currentIndex() + 1, 1);
    double max_flow = flow * 24,res = 0,flow_p[5],pseudo_result = 0;
    int pseudo_counter = 0;
    for(int i=0;i<5;i++) flow_p[i] = (max_flow * (4-i)) / 4;
    for (int i=0;i<31;i++) {
        if (lev[i+1] > -1) {
            m_rects.at(i)->setRect(30 * i + off_x,off_y + 300 - (lev[i+1]/(flow*24) * 300), 20, (lev[i+1]/(flow*24) * 300));
            m_rects.at(i)->setVisible(i < cur.daysInMonth());
            m_rects.at(i)->setBrush(activeRectBrush);
            m_rects.at(i)->setPen(activeRectPen);
            m_rects.at(i)->setToolTip(QString("<center><font style='color:#a63413'>%1 %2</font></center><center><font style='color:#a63413'>%3 м3</font></center><center><font style='color:#a63413'>данные: %4\%</font></center>").arg(i+1).arg(ui->m_month->currentText()).arg(lev[i+1]).arg((int)per[i+1]));
            m_values.at(i)->setHtml(QString("<center><font color='#a63413'>%1</font></center>").arg((int)lev[i+1]));
            m_values.at(i)->setPos(30*i - 5 + off_x,off_y + 280 - (lev[i+1]/(flow*24) * 300));
            m_values.at(i)->setVisible(i < cur.daysInMonth());
            m_texts.at(i)->setVisible(i < cur.daysInMonth());

        } else {
            m_rects.at(i)->setRect(30 * i + off_x,off_y , 20, 300);
            m_rects.at(i)->setPen(passiveRectPen);
            m_rects.at(i)->setBrush(passiveRectBrush);
            m_rects.at(i)->setToolTip(QString("<center><font style='color:#a63413'>%1 %2</font></center><center><font style='color:#a63413'>н/д</font></center>").arg(i+1).arg(ui->m_month->currentText()));
            m_values.at(i)->setHtml(QString("<center><font color='#a63413'>н/д</font></center>"));
            m_values.at(i)->setPos(30*i - 5 + off_x,off_y + 150);
            m_rects.at(i)->setVisible(i < cur.daysInMonth());
            m_values.at(i)->setVisible(i < cur.daysInMonth());
            m_texts.at(i)->setVisible(i < cur.daysInMonth());
        }
    }

    for (int i=1;i<32;i++) {
        if (lev[i] > -1 && per[i] > 0) {
            pseudo_result += (lev[i] * 100 / per[i])/(flow * 24);
            pseudo_counter++;
        }
    }
    pseudo_result = (pseudo_counter) ? (pseudo_result * 100 / pseudo_counter) : 0;
    qDebug() << "PSEUDO_RESULT" << pseudo_result;
    for (int i=0;i<5;i++) m_h_texts[i]->setHtml(QString("<div style='color: #666;text-align: right'>%1</div>").arg(flow_p[i]));
    for (int i=0;i<5;i++) qDebug() << max_flow * (4-i) / 4;
    ui->m_header->setText(QString("Месячная наработка за %1 %2 года").arg(ui->m_month->currentText().toLower()).arg(ui->m_year->value()));
    for (int i=1;i<32;i++) res += (lev[i] == -1) ? 0 : lev[i];
    ui->m_work->setText(QString::number(res,'f',1));
    //m_max_text->setHtml(QString("<center><font style='color: rgb(128,100,0)'>max</font></center><center><font style='color: rgb(128,100,0)'>%1 м3</font></center>").arg(max_flow));
    //ui->m_max->setText(QString::number(max_flow * cur.daysInMonth(),'f',1));
    ui->m_max->setText(QString::number(max_flow * cur.daysInMonth(),'f',1));
    ui->m_max_2->setText(QString::number(max_flow * cur.daysInMonth(),'f',1));
    ui->m_max_val->setText(QString("%1 м3").arg(QString::number(max_flow * cur.daysInMonth(),'f',1)));
    ui->m_half_val->setText(QString("%1 м3").arg(QString::number(max_flow * cur.daysInMonth() / 2,'f',1)));
    ui->m_pseudo_work->setText(QString("%1 м3 (%2%)").arg(QString::number(pseudo_result * max_flow * cur.daysInMonth() / 100,'f',1)).arg(QString::number(pseudo_result,'f',1)));
    //double h = (115 - (res * 100 / (max_flow * cur.daysInMonth()))), p = (res * 100 / (max_flow * cur.daysInMonth()));
    double h = (117 - (pseudo_result)), p = (pseudo_result);
    ui->m_per->setGeometry(ui->m_per->x(),(int)h,ui->m_per->width(),(int)p);
    ui->m_per->setVisible(res > 0);
    //QString m_t = QString("Загрузка НС: %1%").arg(QString::number(res * 100 / (max_flow * cur.daysInMonth()),'f',1));
    QString m_t = QString("Загрузка НС: %1%").arg(QString::number(pseudo_result,'f',1));
    ui->m_max_per->setToolTip(m_t);
    ui->m_per->setToolTip(m_t);
    ui->m_percent->setText(QString::number((double)work/(24 * cur.daysInMonth()) * 100,'f',1));
    ui->m_percent->setStyleSheet(QString("color: rgb(%1,%2,0)").arg((int)((double)255/100*(100-((double)work/(24 * cur.daysInMonth()) * 100)))).arg((int)((double)255/100*((double)work/(24 * cur.daysInMonth()) * 100))));
    update();
}

void dailyWorking::m_save() {
    emit m_saveQuery(did,ui->m_month->currentIndex() + 1,ui->m_year->value());
}

bool dailyWorking::m_export(QMap<int, m_csvData> data, QString name, double flow) {
    bool out = true;
    QString fileName = QFileDialog::getSaveFileName(this,"Сохранить как...",QString("Наработка %1 за %2 %3г.csv").arg(name).arg(ui->m_month->currentText().toLower()).arg(ui->m_year->value()),"*.csv");
    if (fileName.length()) {
        QFile file(fileName);
        double sum = 0,s_flow = 0;
        if (!file.open(QIODevice::WriteOnly)) out = false;
        file.write(QString("Месячная наработка %1 за %2 %3г\r\n").arg(name).arg(ui->m_month->currentText().toLower()).arg(ui->m_year->value()).toLocal8Bit());

        foreach(int t,data.keys()) qDebug() << data[t].date << data[t].working << data[t].percent;
        QString line("День;\"Наработка, м3\";\"Наличие данных, %\"\r\n");
        file.write(line.toLocal8Bit());
        for (int i=1;i<QDate(ui->m_year->value(),ui->m_month->currentIndex()+1,1).daysInMonth()+1;i++) {
            if (data.contains(i)) {
                line = QString("%1;%2;%3\r\n").arg(data[i].day).arg(QString::number(((double)data[i].working * flow / 60),'f',2)).arg(QString::number((data[i].percent),'f',2));
                sum +=data[i].percent; s_flow += (double)data[i].working * flow / 60;
            } else {
                line = QString("%1;0.00;0.00\r\n").arg(i);
            };
            file.write(line.toLocal8Bit());
        }

        file.write(QString("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\"Производительность станции,м3/ч\";%1\r\n\"Максимально возможная наработка за месяц, м3\";%4\r\n\"Наработка за месяц,м3\";%2\r\n\"Наличие данных за месяц,%\";%3").arg(QString::number(flow,'f',2)).arg(QString::number(s_flow,'f',2)).arg(QString::number((sum / QDate(ui->m_year->value(),ui->m_month->currentIndex() +1,1).daysInMonth()),'f',2)).arg(QString::number((flow * 24 * QDate(ui->m_year->value(),ui->m_month->currentIndex() +1,1).daysInMonth()),'f',2)).toLocal8Bit());
        file.close();
    };
    return out;
}

void dailyWorking::setYGraph(QList<double> lev, QList<double> per,double flow,int work) {
    QDate cur(ui->y_year->value(),1, 1);
    double max_flow = flow * 24 * cur.daysInYear(),res = 0,pseudo_result = 0;
    int pseudo_counter = 0;
    for (int i=0;i<12;i++) {
        if (lev[i+1] > -1) {
            y_rects.at(i)->setRect(60 * i + off_x + off_y_x,off_y + 300 - (lev[i+1]/(flow*24 * 31) * 300), 50, (lev[i+1]/(flow*24*31) * 300));
            y_rects.at(i)->setVisible(i < cur.daysInMonth());
            y_rects.at(i)->setBrush(activeRectBrush);
            y_rects.at(i)->setPen(activeRectPen);
            y_rects.at(i)->setToolTip(QString("<center><font style='color:#a63413'>%1 %2</font></center><center><font style='color:#a63413'>%3 м3</font></center><center><font style='color:#a63413'>данные: %4\%</font></center>").arg(monthNames[i+1]).arg(ui->y_year->value()).arg(lev[i+1]).arg((int)per[i+1]));
            y_values.at(i)->setHtml(QString("<center><font color='#a63413'>%1</font></center>").arg((int)lev[i+1]));
            y_values.at(i)->setPos(60*i - 5 + off_x + off_y_x,off_y + 280 - (lev[i+1]/(flow*24*31) * 300));

        } else {
            y_rects.at(i)->setRect(60 * i + off_x + off_y_x,off_y , 50, 300);
            y_rects.at(i)->setPen(passiveRectPen);
            y_rects.at(i)->setBrush(passiveRectBrush);
            y_rects.at(i)->setToolTip(QString("<center><font style='color:#a63413'>%1 %2</font></center><center><font style='color:#a63413'>н/д</font></center>").arg(monthNames[i+1]).arg(ui->y_year->value()));
            y_values.at(i)->setHtml(QString("<center><font color='#a63413'>н/д</font></center>"));
            y_values.at(i)->setPos(60*i - 5 + off_x + off_y_x,off_y + 150);
        }
    }

    for (int i=1;i<=12;i++) {
        if (lev[i] > -1 && per[i] > 0) {
            pseudo_result += (lev[i] * 100 / per[i])/(flow * 24 * QDate(ui->y_year->value(),i,1).daysInMonth());
            pseudo_counter++;
            //qDebug() << pseudo_result;
        }
    }
    //pseudo_result = (pseudo_counter) ? (pseudo_result * 100 / (pseudo_counter)) : 0;
    pseudo_result = (pseudo_counter) ? (pseudo_result / (pseudo_counter)) * 100 : 0;
    //qDebug() << "pSEUDO RESULT " << pseudo_result;

    ui->y_header->setText(QString("Помесячная наработка за %1 год").arg(ui->y_year->value()));
    for (int i=1;i<=12;i++) res += (lev[i] == -1) ? 0 : lev[i];
    ui->y_work->setText(QString::number(res,'f',1));

    double h = (117 - (pseudo_result)), p = (pseudo_result);
    ui->y_per->setGeometry(ui->y_per->x(),(int)h,ui->y_per->width(),(int)p);
    ui->y_per->setVisible(res > 0);
    QString m_t = QString("Загрузка НС: %1%").arg(QString::number(pseudo_result,'f',1));
    ui->y_max_per->setToolTip(m_t);
    ui->y_per->setToolTip(m_t);



    ui->y_max_val->setText(QString("%1 м3").arg(QString::number(max_flow,'f',1)));
    ui->y_half_val->setText(QString("%1 м3").arg(QString::number(max_flow / 2,'f',1)));
    ui->y_pseudo_work->setText(QString("%1 м3 (%2%)").arg(QString::number(pseudo_result * max_flow / 100,'f',1)).arg(QString::number(pseudo_result,'f',1)));


    ui->y_per->setVisible(res > 0);
    ui->y_per->setToolTip(m_t);
    ui->y_max->setText(QString::number(max_flow,'f',1));
    ui->y_max_2->setText(QString::number(max_flow,'f',1));


    //y_max_text->setHtml(QString("<center><font style='color: rgb(128,100,0)'>max</font></center><center><font style='color: rgb(128,100,0)'>%1 м3</font></center>").arg(max_flow));
    ui->y_percent->setText(QString::number((double)work/(24 * cur.daysInYear()) * 100,'f',1));
    ui->y_percent->setStyleSheet(QString("color: rgb(%1,%2,0)").arg((int)((double)255/100*(100-((double)work/(24 * cur.daysInYear()) * 100)))).arg((int)((double)255/100*((double)work/(24 * cur.daysInYear()) * 100))));
    update();
}

void dailyWorking::_setYearQuery() {
    emit yearQuery(did,ui->y_year->value());
}

void dailyWorking::y_save() {
    emit y_saveQuery(did,ui->y_year->value());
}

bool dailyWorking::y_export(QMap<int, m_csvData> data, QString name, double flow) {
    bool out = true;
    QString fileName = QFileDialog::getSaveFileName(this,"Сохранить как...",QString("Наработка %1 за %2г.csv").arg(name).arg(ui->y_year->value()),"*.csv");
    if (fileName.length()) {
        QFile file(fileName);
        double sum = 0,s_flow = 0;
        if (!file.open(QIODevice::WriteOnly)) out = false;
        file.write(QString("Помесячная наработка %1 за %2г\r\n").arg(name).arg(ui->y_year->value()).toLocal8Bit());

        foreach(int t,data.keys()) qDebug() << data[t].date << data[t].working << data[t].percent;
        QString line("Месяц;\"Наработка, м3\";\"Наличие данных, %\"\r\n");
        file.write(line.toLocal8Bit());
        for (int i=1;i<12;i++) {
            if (data.contains(i)) {
                line = QString("%1;%2;%3\r\n").arg(monthNames[data[i].day]).arg(QString::number(((double)data[i].working * flow / 60),'f',2)).arg(QString::number((data[i].percent),'f',2));
                sum +=data[i].percent; s_flow += (double)data[i].working * flow / 60;
            } else {
                line = QString("%1;0.00;0.00\r\n").arg(i);
            };
            file.write(line.toLocal8Bit());
        }

        file.write(QString("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\"Производительность станции,м3/ч\";%1\r\n\"Максимально возможная наработка за год, м3\";%4\r\n\"Наработка за год,м3\";%2\r\n\"Наличие данных за год,%\";%3").arg(QString::number(flow,'f',2)).arg(QString::number(s_flow,'f',3)).arg(QString::number((sum / 12),'f',3)).arg(QString::number((flow * 24 * QDate(ui->y_year->value(),1,3).daysInYear()),'f',3)).toLocal8Bit());
        file.close();
    };
    return out;
}

bool dailyWorking::eventFilter(QObject *obj, QEvent *ev) {

}

void dailyWorking::rectClicked(QPoint point) {
    qDebug() << "dblclicked" << point.x() << point.y();
}

void dailyWorking::catchItem(QGraphicsItem *item) {
    qDebug() << item->boundingRect();
}

void dailyWorking::catchClick(QPointF point) {
    qDebug() << point << ui->view->itemAt(point.toPoint() - QPoint(4,0))->data(100);
    int hour = -1;
    if (ui->view->itemAt(point.toPoint() - QPoint(4,0)) && ui->view->itemAt(point.toPoint() - QPoint(4,0))->data(100).toInt() > -1) {
       hour = ui->view->itemAt(point.toPoint() - QPoint(4,0))->data(100).toInt();
    }

    if (hour > -1 && hour < 24) {
        //if (maxDate[hour] > QDate(2000,1,1)) day->setDate(maxDate[hour]);
        if (maxDate[hour] > QDate(2000,1,1)) {
            statW->setData(from->date(),to->date(),maxVal[hour],maxDate[hour],minVal[hour],minDate[hour],averVal[hour],lastVal[hour],day->date(),hour,(hour+1) % 24);
            statW->show();
        }
    }
}

void dailyWorking::customMenu(QPoint point) {
    qDebug() << point;
    //menu->setGeometry(point.x(),point.y(),menu->width(),menu->height());
    //menu->show();
    qDebug() << point << ui->view->itemAt(point)->data(100);
    int hour = -1;
    if (ui->view->itemAt(point) && ui->view->itemAt(point)->data(100).toInt() > -1) {
       hour = ui->view->itemAt(point)->data(100).toInt();
    }

    if (hour > -1 && hour < 24) {
        //if (minDate[hour] > QDate(2000,1,1)) day->setDate(minDate[hour]);
    }
}

void dailyWorking::sh_max_pr(int hour) {
    if (hour > -1 && hour < 24) {
        if (maxDate[hour] > QDate(2000,1,1)) {
            ui->showLast->setChecked(true);
            day->setDate(maxDate[hour]);
        }
    }
}

void dailyWorking::sh_min_pr(int hour) {
    if (hour > -1 && hour < 24) {
        if (minDate[hour] > QDate(2000,1,1)) {
            ui->showLast->setChecked(true);
            day->setDate(minDate[hour]);

        }
    }
}
