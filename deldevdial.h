#ifndef DELDEVDIAL_H
#define DELDEVDIAL_H

#include <QDialog>

namespace Ui {
class delDevDial;
}

class delDevDial : public QDialog
{
    Q_OBJECT
    
public:
    explicit delDevDial(QWidget *parent = 0);
    ~delDevDial();
    
private:
    Ui::delDevDial *ui;
};

#endif // DELDEVDIAL_H
