#include "downloadupdate.h"

downloadUpdate::downloadUpdate()
{
    currentDownload = NULL;
    connect(&manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(downloadFinished()));
    wid = new downloadWidget();
    wid->hide();
    connect(wid,SIGNAL(newFileName(QString)),this,SLOT(save(QString)));
    fileName = "last.exe";
}

void downloadUpdate::checkUpdate() {
    check = true;
    emit message("Проверка обновлений","Идет поиск новых версий программы",6);
    doDownload(QUrl("http://wilo-sk.ru/latest/last.ver"));
}

void downloadUpdate::download() {
    check = false;
    emit message("Проверка обновлений","Идет загрузка новых версий программы",6);
    doDownload(QUrl("http://wilo-sk.ru/latest/last.zip"));
}

void downloadUpdate::doDownload(const QUrl &url)
{
    QNetworkRequest request(url);
    if (currentDownload != NULL) {
        currentDownload->disconnect();
        delete currentDownload;
    }
    currentDownload = manager.get(request);
    currentDownload->ignoreSslErrors();
    connect(currentDownload,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(bytesChanged(qint64,qint64)));
#ifndef QT_NO_SSL
    connect(currentDownload, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)));
#endif
}

void downloadUpdate::downloadFinished()
{
    qDebug() << "download finished!!!";
    QUrl url = currentDownload->url();
    if (check) {
        check = false;
        if (currentDownload->error()) {
        qDebug() << "error: " << currentDownload->errorString();
        emit message("Проверка обновлений","Ошибка: " +currentDownload->errorString(),7);
        } else {
            newVer = QString(currentDownload->readAll());
            qDebug() << myVer << newVer;
            if (newer(newVer)) {
                //download();
                emit message("Проверка обновлений","Доступна новая версию программы",6);
                wid->reload();
                wid->showText("Новая версия программы АРМ SK-712 доступна для загрузки");
                wid->showText("Текущая версия: <font color='#f00'>" + myVer + "</font>");
                wid->showText("Доступная версия: <font color='#0f0'>" + newVer + "</font>");
                wid->showText("Обновить версию?");
                wid->lateVer = newVer;
            }
       }
    } else {
        if (currentDownload->error()) {
        qDebug() << "error: " << currentDownload->errorString();
        emit message("Проверка обновлений","Ошибка загрузки: " +currentDownload->errorString(),7);
        } else {
            QFile file(MAIN_PATH +  "last.zip");
            if (file.open(QIODevice::WriteOnly)) {
                file.write(currentDownload->readAll());
                wid->showText("<i>Сохранение:</i> <b>сохранено!</b> Закройте программу и установите загруженное обновление.");
                QZipReader reader(MAIN_PATH +  "last.zip");
                QDir().mkpath(MAIN_PATH + "_newer");
                reader.extractAll(MAIN_PATH + "_newer");
                QProcess * proc = new QProcess(0);
                QStringList arguments;
                arguments << "update" << newVer << myVer << fileName;
                proc->start(QDir().absolutePath() + "/proc.exe",arguments);
            } else {
                wid->showText("<i>Сохранение:</i> <b>не сохранено!</b> Проверьте наличие прав для записи файла.");
            }
            wid->setReady();
            file.close();
       }
    }
    //currentDownload->deleteLater();
}

void downloadUpdate::bytesChanged(qint64 bytes, qint64 total) {
    qDebug() << bytes << "of" << total;
    wid->setProgress(bytes,total);
}

void downloadUpdate::sslErrors(const QList<QSslError> &sslErrors)
{
#ifndef QT_NO_SSL
    foreach (const QSslError &error, sslErrors)
        qDebug() << "sslError: " << error.errorString();
#else
    Q_UNUSED(sslErrors);
#endif
}

bool downloadUpdate::newer(QString ver) {
    QRegExp verNum("([0-9]{1,3}\\.){1,3}[0-9]$");
    if (ver.contains(verNum) && myVer.contains(verNum)) {
        QStringList myV = myVer.split(".");
        QStringList newV = ver.split(".");
        for (int i=0;(i<myV.count()&&i<newV.count());i++) {
            int a = myV[i].toInt(); int b = newV[i].toInt();
            qDebug() << "newer: " << a << "and" << b;
            if (a > b) return false;
            else if (a < b) return true;
        }
        return false;

    } else {
        qDebug() << "newer: bad version";
        return false;
    }
}

void downloadUpdate::setCurrentVersion(QString ver) {
    myVer = ver;
}

void downloadUpdate::save(QString filename) {
    if (filename != "") {
        fileName = filename;
        download();
        wid->showText("<i>Загрузка обновления...</i>");
        wid->blockRefresh(true);
    };
}
