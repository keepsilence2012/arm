#include "ui.h"

QString UI::tcpToString(QByteArray byte) {
    int i,j,k;

    QString tmp,rdy=" RDY=",run=" RUN=",err=" ERR=",pwr = " PWR=", in=" >IN=",an=" >AN=",out="",sbm=" SBM=",ssm=",SSM=";
    QStringList errors;
    ;//qDebug() << byte.length();
    qDebug() << mdbPort::show(byte);
    //byte = byte.remove(0,9).left(32);
    byte = byte.left(32);
    if (byte.length() == 0) return QString("");

        //;//qDebug() << "rdy=" << (quint8)byte.at(1);
        j=1; for (i=0;i<6;i++) {if (byte.at(1) & j) rdy+=QString().setNum(i+1); else rdy+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(2) & j) run+=QString().setNum(i+1); else run+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(3) & j) err+=QString().setNum(i+1); else err+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(5) & j) pwr+=QString().setNum(i+1); else pwr+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(7) & j) in+="1"; else in+="0"; j=j*2;};

        an+= QString().setNum(((double)((quint8)byte.at(8))*256 + (quint8)byte.at(9)) / 100,'f',2);

        i = (quint8)byte.at(6) >> 5;
        if (i & 1) out="*ALARM*";
        else if (i & 4) out="*HAND MODE*";
        else if (i & 2) out="*READY*";
        else out="*OFF*";

        if (i&2) sbm+="1"; else sbm+="0";
        if (i&1) ssm+="1"; else ssm+="0";

        out += sbm+ssm+in+an+rdy+run+err+pwr;

        for (k=0;k<=7;k++) {
            i = (quint8)byte.at(17 + k*2);

            for (j=0;j<8;j++) {
                if (i & 1) {
                    tmp = (j != 0) ? QString().setNum(j) :  "0";
                    tmp.prepend((k==0) ? "0" : QString().setNum(k));
                    tmp.prepend("*E"); tmp.append("*");
                    errors.append(tmp);
                    qDebug() << tmp;
                };
                i = i >> 1;
            };
        };

        foreach(QString s,errors) {
            if (out.length() < 160) out += s + ", ";
        };

        out += " *END*";
        return out;
}

void UI::renderIncomingTcpQuey(QString ip, QString tcpQuery) {
    int i,j; bool correct = false;
    foreach (j,deviceArray.keys()) if (deviceArray[j].address == ip && deviceArray[j].type == "tcp") { i = j; correct = true; break;};

    //если он есть среди НС
    if (correct) {
        //Отключаем мигание

        //if (DEBUG_MODE)
            qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: С НОМЕРА " << ip  << " ПРИШЛО СООБЩЕНИЕ " << tcpQuery;
    //Если он корректен
        status deviceStat = StringToStat(QString(tcpQuery.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
        if (deviceStat.correct) {
            deviceArray[i].packsAll++;
            deviceArray[i].tcpLastReport = QDateTime().currentDateTime();
            _stopWaiting(i);
            _setPointerStatus(i,deviceStat.stat);
            if (deviceWindows.contains(i))
                if (!deviceWindows[i]->history->isVisible()) _setDeviceWindowStatus(i,deviceStat);
            //if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС КОРРЕКТЕН";
            //showD("TCP_QUERY: ");
            //showD(QString().setNum(QDateTime::currentMSecsSinceEpoch()).prepend("CUR_T: "));
            //showD(QString().setNum(timeFromLoading + 10000).prepend("time+10s: "));
            if (QDateTime::currentMSecsSinceEpoch() > (timeFromLoading + 10000))
            {
                if (needToSave(i,deviceStat)) {
                    deviceArray[i].outCurrentDate = QDateTime().currentDateTime();
                    if (statArray.contains(deviceStat.stat)) trayMessage(deviceArray[i].name,statArray[deviceStat.stat]);
                    //Проигрываем зву сообщения
                    if (deviceStat.fullText.contains("*ALARM*")) {
                        //PlaySoundA(QString(MAIN_PATH + "sound/alarm.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                        QSound::play(alarmSound);
                    } else {
                       // PlaySoundA(QString(MAIN_PATH + "sound/message.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                        QSound::play(messageSound);
                    };
                    _saveDeviceStat(i,QString(tcpQuery.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
                    if(deviceForms.contains(i)) deviceForms[i]->warning = (deviceStat.stat == "alarm");
                };
                deviceArray[i].stat = deviceStat;
                _setBuseStat(i);
                _setDeviceFormData(i);
                _sortDeviceList();
                _sortBuseList();
            };
        } else {
            if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС НЕВЕРНЫЙ!!!";
        };
    //Если время
    } else {
        if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: НЕТ УСТРОЙСТВА С НОМЕРОМ " << ip;
    };
}

void UI::renderIncomingTcpNQuey(quint16 index, QString tcpQuery) {
    if (!deviceArray.contains(index)) return;
    int i = index;
    //если он есть среди НС
    if (1) {
        //Отключаем мигание

        //if (DEBUG_MODE)
            //qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: С НОМЕРА " << ip  << " ПРИШЛО СООБЩЕНИЕ " << tcpQuery;
    //Если он корректен
        status deviceStat = StringToStat(QString(tcpQuery.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
        if (deviceStat.correct) {
            deviceArray[i].packsAll++;
            deviceArray[i].tcpLastReport = QDateTime().currentDateTime();
            _stopWaiting(i);
            _setPointerStatus(i,deviceStat.stat);
            if (deviceWindows.contains(i))
                if (!deviceWindows[i]->history->isVisible()) _setDeviceWindowStatus(i,deviceStat);
            //if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС КОРРЕКТЕН";
            //showD("TCP_QUERY: ");
            //showD(QString().setNum(QDateTime::currentMSecsSinceEpoch()).prepend("CUR_T: "));
            //showD(QString().setNum(timeFromLoading + 10000).prepend("time+10s: "));
            if (QDateTime::currentMSecsSinceEpoch() > (timeFromLoading + 10000))
            {
                if (needToSave(i,deviceStat)) {
                    deviceArray[i].outCurrentDate = QDateTime().currentDateTime();
                    if (statArray.contains(deviceStat.stat)) trayMessage(deviceArray[i].name,statArray[deviceStat.stat]);
                    //Проигрываем зву сообщения
                    if (deviceStat.fullText.contains("*ALARM*")) {
                        //PlaySoundA(QString(MAIN_PATH + "sound/alarm.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                        QSound::play(alarmSound);
                    } else {
                       // PlaySoundA(QString(MAIN_PATH + "sound/message.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                        QSound::play(messageSound);
                    };
                    _saveDeviceStat(i,QString(tcpQuery.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
                    if(deviceForms.contains(i)) deviceForms[i]->warning = (deviceStat.stat == "alarm");
                };
                deviceArray[i].stat = deviceStat;
                _setBuseStat(i);
                _setDeviceFormData(i);
                _sortDeviceList();
                _sortBuseList();
            };
        } else {
            if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС НЕВЕРНЫЙ!!!";
        };
    //Если время
    } else {
        //if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: НЕТ УСТРОЙСТВА С НОМЕРОМ " << ip;
    };
}

void UI::renderTcpResponse(QByteArray status, QBitArray channels, QString ip) {
    if (DEBUG_MODE) qDebug() << "ПОЛУЧЕНИЕ TCP-ПАКЕТА:" << tcpToString(status) << ip << "\r\n";
    renderIncomingTcpQuey(ip,tcpToString(status));
}

void UI::renderTcpNResponse(QByteArray status) {
    //qDebug() << "RESPONSE IN ARM!!! " << sender()->property("index").toUInt();
    renderIncomingTcpNQuey(sender()->property("index").toUInt(),tcpToString(status));
}

bool UI::needToSave(quint16 index, status stat) {
    bool need = false;
    if (deviceArray.contains(index)) {
        if (deviceArray[index].stat.stat != stat.stat) need = true;
        if (deviceArray[index].stat.errors != stat.errors) need = true;
        if (deviceArray[index].outCurrentDate < QDateTime(stat.date).addSecs(-3600)) need = true;
    };
    return need;
}
