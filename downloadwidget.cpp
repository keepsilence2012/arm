#include "downloadwidget.h"
#include "ui_downloadwidget.h"

downloadWidget::downloadWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::downloadWidget)
{
    ui->setupUi(this);
    setWindowTitle("Обновление АРМ SK-712");
    connect(ui->cancel,SIGNAL(clicked()),this,SLOT(close()));
    connect(ui->ok,SIGNAL(clicked()),this,SLOT(okPressed()));
    ui->progress->setVisible(false);
    lateVer = "latest";
    setWindowFlags((windowFlags() & ~Qt::WindowMaximizeButtonHint & ~Qt::WindowMinimizeButtonHint)| Qt::WindowStaysOnTopHint);

}

downloadWidget::~downloadWidget()
{
    delete ui;
}

void downloadWidget::setProgress(int per,int max) {
    if (per >= 0) {
        ui->progress->setVisible(true);
        ui->progress->setMaximum(max);
        ui->progress->setValue(per);
    }
    else ui->progress->setVisible(false);
}

void downloadWidget::showText(QString text) {
    if (text == "") {
        ui->textBrowser->clear();
    } else {
        ui->textBrowser->append(text);
    }
}

void downloadWidget::okPressed() {
    QString fileName("save");
    askDialog * dial = new askDialog(this);
    if (dial->exec() == QDialog::Accepted) {
        fileName += (dial->saveSettings()) ? " settings" : "";
        fileName += (dial->saveStations()) ? " stations" : "";
        emit newFileName(fileName);
    }
    delete dial;
}

void downloadWidget::reload() {
    ui->progress->setValue(0);
    ui->progress->setVisible(false);
    ui->textBrowser->clear();
    ui->cancel->setText("Отмена");
    ui->ok->setEnabled(true);
    show();
}

void downloadWidget::blockRefresh(bool on) {
    ui->ok->setDisabled(on);
}

void downloadWidget::setReady() {
    ui->cancel->setText("Готово");
}
