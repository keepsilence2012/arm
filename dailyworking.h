#ifndef DAILYWORKING_H
#define DAILYWORKING_H

#include <QWidget>
#include <QDebug>
#include <QList>
#include <QMessageBox>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QPen>
#include <QBrush>
#include <QDate>
#include <QCheckBox>
#include <QFileDialog>
#include <QGradient>
#include <QTimer>
#include <QCalendarWidget>
#include <calwid.h>
#include <statwidget.h>
#include <QMenu>

namespace Ui {
class dailyWorking;
}

class specialScene : public QGraphicsScene {
    Q_OBJECT
public:
    specialScene(QWidget * par = 0);
    ~specialScene();
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent * ev);

signals:
    void objClicked(QGraphicsItem *);
    void clicked(QPointF);
};

class dailyWorking : public QWidget
{
    Q_OBJECT

public:
    struct csvData {
        int working[24];
        int day;
        int month;
        int year;
        QString date;
    };

    struct m_csvData {
        QString date;
        int day;
        int working;
        double percent;
    };

    QList<double> maxVal,minVal,averVal,lastVal;

    QMenu * menu;
    QAction *showMax,*showMin;
    calWid * from, *to, *day;
    statWidget * statW;
    int pos_x;
    int pos_y;
    int off_x, off_y;
    int off_y_x,off_y_y;
    double flow;
    quint16 did;
    QPen maxPen;
    QPen graphPen;
    QPen activeRectPen;
    QPen passiveRectPen;
    QPen tagPen;
    QPen lastPen;
    QBrush activeRectBrush;
    QBrush lastBrush;
    QBrush passiveRectBrush;
    QBrush maxBrush;
    QList<QGraphicsRectItem *>  lines;
    QList<QString> monthNames;
    QList<QGraphicsRectItem *> rects;
    QList<QGraphicsRectItem *> last;
    QList<QGraphicsTextItem *> underText;
    QList<QGraphicsTextItem *> texts;
    QList<QGraphicsTextItem *> max_texts;
    QList<QGraphicsTextItem *> datas;
    QList<QGraphicsLineItem *> x_lines;
    QList<QGraphicsLineItem *> y_lines;
    QGraphicsTextItem * x_scale;
    QGraphicsTextItem * y_scale;
    QGraphicsLineItem * tagLine;
    specialScene scene;
    QGraphicsScene  m_scene,y_scene;
    QList<QGraphicsRectItem * > m_rects;
    QList<QGraphicsTextItem * > m_values;
    QList<QGraphicsTextItem * > m_texts;
    QList<QGraphicsTextItem * > m_h_texts;
    QList<QGraphicsLineItem * > m_v_lines;
    QList<QGraphicsLineItem *> m_h_lines;
    QGraphicsLineItem * m_max_line;
    QGraphicsTextItem * m_max_text;
    QGraphicsTextItem * m_x_scale, * m_y_scale;
    QList<QGraphicsRectItem * > y_rects;
    QList<QGraphicsTextItem * > y_values;
    QList<QGraphicsTextItem * > y_texts;
    QList<QGraphicsTextItem * > y_h_texts;
    QList<QGraphicsLineItem * > y_v_lines;
    QList<QGraphicsLineItem *> y_h_lines;
    QGraphicsLineItem * y_max_line;
    QGraphicsTextItem * y_max_text;
    QGraphicsTextItem * y_x_scale, * y_y_scale;
    QList<QDate> maxDate;
    QList<QDate> minDate;
    explicit dailyWorking(QWidget *parent = 0);
    ~dailyWorking();
    void setLines(QList<int> levels);
    void showOrHide();
    void setHeader();
    void setRects(QList<int> rectLevels);
    void setGraph(QList<double> l_lev, QList<QDate> m_date,QList<double> min_lev, QList<QDate> min_date, QList<double> r_lev, QList<double> la_lev, double working = -1, double percent = 0);
    void setMGraph(QList<double> lev, QList<double> per, double flow, int work);
    void setYGraph(QList<double> lev, QList<double> per, double flow, int work);
    bool exportToCsv(QMap<QString,csvData> data, QList<double> av, QList<double> max, QString name);
    bool m_export(QMap<int,m_csvData> data, QString name, double flow);
    bool y_export(QMap<int,m_csvData> data, QString name, double flow);
    bool eventFilter(QObject *obj, QEvent *ev);

signals:
    void dailyQuery(quint16,QDate,QDate,QDate);
    void monthQuery(quint16,int,int);
    void yearQuery(quint16,int);
    void y_saveQuery(quint16,int);
    void m_saveQuery(quint16,int,int);
    void saveQuery(quint16,QDate,QDate);
public slots:
    void testGraph(int lev);
    void sendDailyQuery();
    void sendSaveQuery();
    void changeMaxRange(QDate min);
    void changeMinRange(QDate max);
    void _averChecked();
    void _maxChecked();
    void _lastChecked();
    void _setDate();
    void _setMonthQuery();
    void m_save();
    void _setYearQuery();
    void y_save();
    void rectClicked(QPoint point);
    void catchItem(QGraphicsItem * item);
    void catchClick(QPointF point);
    void customMenu(QPoint point);
    void sh_min_pr(int hour);
    void sh_max_pr(int hour);
private:
    Ui::dailyWorking *ui;
};

#endif // DAILYWORKING_H
