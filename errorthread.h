#ifndef ERRORTHREAD_H
#define ERRORTHREAD_H

#include <QThread>
#include <QFile>

class errorThread : public QThread
{
    Q_OBJECT
public:
    explicit errorThread(QObject *parent = 0);
    void run();
signals:
    
public slots:
    
};

#endif // ERRORTHREAD_H
