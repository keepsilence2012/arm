#include "devicedialog.h"
#include "ui_devicedialog.h"

deviceDialog::deviceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deviceDialog)
{
    ui->setupUi(this);

    qDebug() << "Создание окна";
    devTab = ui->dev;
    winTab = ui->win;
    gsmWidget = ui->gsmWidget;
    tcpWidget = ui->tcpWidget;
    mdbWidget = ui->mdbWidget;
    index = ui->index;
    name = ui->name;
    type = ui->type;
    gsmNum = ui->gsmNum;
    tcpNum = ui->tcpNum;
    tcpAddr = ui->tcpAddr;
    mdbPort = ui->mdbCom;
    mdbAddr = ui->mdbAddr;
    mdbPage = ui->mdbPage;
    start = ui->start;
    ok = ui->okButton;
    cancel = ui->cancelButton;
    del = ui->deleteButton;
    alx = ui->alx;
    aly = ui->aly;
    alw = ui->alw;
    pumps = ui->pumps;
    dig = ui->dig;
    help = ui->help;
    digPic = ui->digPic;
    graph = ui->grahp;
    graphPic = ui->graphPic;
    flow = ui->flow;
    //view = ui->view;
    setObjectName("dialog");

    qDebug() << "Установка значений";
    index->setObjectName("index");
    //start->setObjectName("type");
    name->setObjectName("name");
    pumps->setObjectName("pumps");
    alx->setObjectName("al");
    alw->setObjectName("al");
    aly->setObjectName("al");

    tcpWidget->setVisible(false);
    mdbWidget->setVisible(false);
    devTab->setWindowTitle("Параметры соединения");
    winTab->setWindowTitle("Отображение информации");
    ui->tabWidget->setCurrentIndex(0);
    setWindowTitle("Создание НС");

    flowBoxes << ui->p2 << ui->p3 << ui->p4 << ui->p5 << ui->p6;
    flowGraphics << ui->fl2 << ui->fl3 << ui->fl4 << ui->fl5 << ui->fl6;

    foreach(QSerialPortInfo info,QSerialPortInfo::availablePorts()) ui->mdbCom->addItem(info.portName());

    ui->tabWidget->setTabShape(QTabWidget::Triangular);

    qDebug() << "Подключение сигналов";
    connect(type,SIGNAL(currentTextChanged(QString)),this,SLOT(typeChanged()));
    connect(ok,SIGNAL(clicked()),this,SLOT(okPr()));
    connect(cancel,SIGNAL(clicked()),this,SLOT(reject()));
    connect(del,SIGNAL(clicked()),this,SLOT(delDevice()));
    connect(dig,SIGNAL(toggled(bool)),this,SLOT(digCheck(bool)));
    connect(digPic,SIGNAL(clicked()),this,SLOT(digPicPr()));
    connect(graphPic,SIGNAL(clicked()),this,SLOT(graphPicPr()));

    connect(ui->flow,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));
    connect(ui->p2,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));
    connect(ui->p3,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));
    connect(ui->p4,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));
    connect(ui->p5,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));
    connect(ui->p6,SIGNAL(valueChanged(double)),this,SLOT(manyPumpsValueChanged()));

    connect(ui->onePump,SIGNAL(clicked()),this,SLOT(flowTypeChanged()));
    connect(ui->morePumps,SIGNAL(clicked()),this,SLOT(flowTypeChanged()));

    connect(index,SIGNAL(focused()),this,SLOT(indexPr()));
}

deviceDialog::~deviceDialog()
{
    delete ui;
}

void deviceDialog::typeChanged() {
    bool gsm = type->currentIndex() == 0;
    if (type->currentIndex() == 0) {
        gsmWidget->setVisible(true);
        tcpWidget->setVisible(false);
        mdbWidget->setVisible(false);
        showHelp("gsm_t");
    } else if (type->currentIndex() == 2) {
        gsmWidget->setVisible(false);
        tcpWidget->setVisible(true);
        mdbWidget->setVisible(false);
        showHelp("tcp_t");
    } else if (type->currentIndex() == 1) {
        gsmWidget->setVisible(false);
        tcpWidget->setVisible(false);
        mdbWidget->setVisible(true);
        showHelp("modbus_t");
    };

    ui->alw->setVisible(gsm);
    ui->alx->setVisible(gsm);
    ui->aly->setVisible(gsm);
    ui->alwLab->setVisible(gsm);
    ui->alxLab->setVisible(gsm);
    ui->alyLab->setVisible(gsm);

}

void deviceDialog::delDevice() {
    emit deleted((quint16)index->value());
}

void deviceDialog::okPr() {
    if (ui->name->text().count() <= 3) {QMessageBox::warning(this,"Неверные данные","Название НС должно содержать больше 3 символов");return;};
    if (type->currentIndex() == 0) {
        if (!ui->gsmNum->text().contains(QRegExp("^[0-9]{10,16}$"))) {QMessageBox::warning(this,"Неверные данные","Неправильно введен телефонный номер НС");return;};
        accept();
    } else if (type->currentIndex() == 1) {
        accept();
    } else if (type->currentIndex() == 2) {
        accept();
    };
}

void deviceDialog::digCheck(bool on) {
    graphPic->setStyleSheet((dig->isChecked()) ? ("border-style:solid;border-color:#000;border-width:1px;background:#fff;") : ("background:#fff;border-style:solid;border-color:#019b80;border-width:3px;"));
    digPic->setStyleSheet(!(dig->isChecked()) ? ("border-style:solid;border-color:#000;border-width:1px;background:#fff;") : ("background:#fff;border-style:solid;border-color:#019b80;border-width:3px;"));
}

quint8 deviceDialog::viewType() {
    return (dig->isChecked()) ? 0 : 1;
}

void deviceDialog::digPicPr() {
    dig->setChecked(true);
    digCheck(true);
}

void deviceDialog::graphPicPr() {
    graph->setChecked(true);
    digCheck(false);
}

void deviceDialog::showHelp(QString par) {
    if (par == "index") {
        help->clear();
        help->append("<b>Индекс</b> - номер, под которым НС будет добавлена в программу АРМ SK-712.");
        help->append("В программе не может быть двух станцих с одинаковыми номерами.");
        help->append("Вы можете указать желаемый номер или оставить его без изменений.");
    }
    else if (par == "type") {
        help->clear();
        help->append("<b>Тип подключения</b>");
        help->append("Укажите, как будет осуществляться связь с НС");
        help->append("<b>GSM / SMS</b>,через короткие сообщения, с помощью GSM-информатора в приборе управления SK-712");
        help->append("<b>RS-485</b>, по протоколу MODBUS RTU с помощью платы RS-485.");
        help->append("<b>TCP/IP</b>, по Ethernet по протоколу MODBUS TCP.");
    }
    else if (par == "gsm_t" || par == "gsmNum") {
        help->clear();
        help->append("<b font=\"14px\">GSM / SMS</b>");
        help->append("В поле <b>номер</b> укажите номер сим-карты, установленной в информатор (без пробелов)");
    }
    else if (par == "modbus_t" || par == "mdbAddr" || par == "mdbPage" || par == "mdbPort") {
        help->clear();
        help->append("<b font=\"14px\">RS-485</b>");
        help->append("");
        help->append("<b>Порт</b> - номер COM-порта, к которому подключен конвертер RS-485/USB");
        help->append("");
        help->append("<b>Адрес MODBUS</b> -");
        help->append("адрес НС в сети");
        //help->append("- SK-712/d-2-5,5 = <b>85</b>");
        help->append("(параметр PC9 = 1..247).");
        //help->append("");
        //help->append("<b>Страница MODBUS</b> -");
        //help->append("страница НС в сети:");
        //help->append("- SK-712/d-2-5,5 параметр <b>PC9</b>");
        //help->append("- остальные = <b>0</b>\n");
    }
    else if (par == "tcp_t" || par == "tcpAddr" || par == "tcpNum") {
        help->clear();
        help->append("<b font=\"14px\">TCP/IP</b>");
        help->append("<b>ip-адрес (домен)</b> - адрес НС в сети Ethernet");
        help->append("<b>Адрес MODBUS</b> - адрес НС в сети MODBUS (параметр PC9 = 1..247)");
    }
    else if (par == "name") {
        help->clear();
        help->append("<b>Имя НС</b>");
        help->append("Имя, под которым насосная станция должна будет занесена в программу.");
        help->append("<font color=red>*</font>Имя должно быть длиннее 3 букв.");
    }
    else if (par == "start") {
        help->clear();
        help->append("<b>Тип пуска</b>");
        help->append("Для приборов управления с частотным регулированием укажите тип <b>W</b>");
    }
    else if (par == "al") {
        help->clear();
        help->append("<b>Дискретные входы информатора</b>");
        help->append("<b>ALW,ALX,ALY</b> - дискретные входы GSM/SMS-информатора.");
    }
    else if (par == "pumps") {
        help->clear();
        help->append("<b>Количество насосов</b>");
        help->append("Необязательный параметр. \"Лишние\" насосы будут скрыты.");
    }
    else; //help->append(par);
}

void deviceDialog::indexPr() {
    showHelp("index");
}

void deviceDialog::namePr() {
    showHelp("name");
}

void deviceDialog::typePr() {
    showHelp("type");
}

void deviceDialog::pumpsPr() {
    showHelp("pumps");
}

void deviceDialog::alPr() {
    showHelp("al");
}

void deviceDialog::flowTypeChanged() {
    ui->p2->setDisabled(ui->onePump->isChecked());
    ui->p3->setDisabled(ui->onePump->isChecked());
    ui->p4->setDisabled(ui->onePump->isChecked());
    ui->p5->setDisabled(ui->onePump->isChecked());
    ui->p6->setDisabled(ui->onePump->isChecked());
    if (ui->morePumps->isChecked()) {
        ui->flowGraph->setStyleSheet("#flowGraph {background: #fff;}\r\n#fl1{background: #00987b;}\r\n#fl2{background: #00987b;}\r\n#fl3{background: #00987b;}\r\n#fl4{background: #00987b;}\r\n#fl5{background: #00987b;}\r\n#fl6{background: #00987b;}");
    } else {
        ui->flowGraph->setStyleSheet("#flowGraph {background: #efefef;}\r\n#fl1{background: #ccc;}\r\n#fl2{background: #ccc;}\r\n#fl3{background: #ccc;}\r\n#fl4{background: #ccc;}\r\n#fl5{background: #ccc;}\r\n#fl6{background: #ccc;}");
    }
}

void deviceDialog::manyPumpsValueChanged() {
    for (int i=0;i<5;i++) {
        int height = (int)(20 * flowBoxes[i]->value() / ui->flow->value());
        flowGraphics[i]->setGeometry(flowGraphics[i]->x(),120-height,flowGraphics[i]->width(),height);
    }
}
