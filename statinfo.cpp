#include "statinfo.h"
#include "ui_statinfo.h"

statInfo::statInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::statInfo)
{
    index = 0;
    ui->setupUi(this);
    setWindowTitle("Информация");
    setModal(true);
    lab = ui->label_2;
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(reject()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(drPr()));
}

statInfo::~statInfo()
{
    delete ui;
}

void statInfo::drPr() {
    emit drop(index);
}
