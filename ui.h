#ifndef UI_H
#define UI_H
//ПУТЬ К ПАПКЕ С ПРОГРАММОЙ

//ФАЙЛ С НОМЕРАМИ УСТРОЙСТВ
#define DEVICES_FILE QString("devices.cfg")
//ФАЙЛ С НАСТРОЙКАМИ
#define SETTINGS_FILE QString("settings.cfg")
//ФАЙЛ С ОШИБКАМИ
#define ERRORS_FILE QString("errors.csv")

//РЕЖИМ ДЕБАГА
#define DEBUG_MODE false

//ДЕФОЛТОВАЯ КАРТА
#define DEF_MAP QString("default.jpg")

#include <QMainWindow>
#include <QMap>
#include <QWidget>
#include <QList>
#include <QTimer>
#include <QDebug>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QDateTime>
#include <QFile>
#include <QDir>
#include <QLabel>
#include <QLineEdit>
#include <QThread>
#include <QSerialPort>
#include <QSound>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlDriver>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSerialPortInfo>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include "deviceform.h"
#include "devicepointer.h"
#include "devicewindow.h"
#include "deviceedit.h"
#include "devicebuse.h"
#include "exiteditmodedialog.h"
#include "editdialog.h"
#include "password.h"
#include "editpassword.h"
#include "deldial.h"
#include "about.h"
#include "windows.h"
#include "deldevdial.h"
#include <signal.h>
#include "tcp.h"
#include "tcp_2.h"
#include "mdb.h"
#include "pressure.h"
#include "pressure_2.h"
#include "devicedialog.h"
#include "statinfo.h"
#include "dailyworking.h"
#include "exportdialog.h"
#include "downloadupdate.h"
#include "changeslog.h"
#include "backupwindow.h"

namespace Ui {
class UI;
class deviceForm;
}
/*************************************************
        СТРУКТУРА ДЛЯ ДЕКДИРОВАНИЯ СООБЩЕНИЯ
*************************************************/
struct smsData {
    QString number;
    QString text;
    QString address;
    QDateTime date;
};

/*************************************************
        СТРУКТУРА ДЛЯ СТАТУСОВ НС
 *************************************************/
struct status {
    QString fullText; //Полный текст смс
    QString stat; //Обобщенный статус
    QDateTime date; //Дата статуса
    bool rdy[6]; //Насосы готвность
    bool run[6]; //Насосы в работе
    bool err[6]; // Насосы ошибка
    bool pwr[6]; // Насосы питание
    bool in[6]; //in0-in5
    bool daily; //Суточное или нет
    quint8 work[24]; //наработка
    bool sbm; //sbm
    bool ssm; //ssm
    double an; //аналоговый датчик
    bool alx,aly,alw;
    QList<quint8> errors; //Коды ошибок
    QMap<quint8,QString> errorList; //Расшифровка ошибок
    bool correct;
};

/****************************************
    СТРУКТУРА, ОПИСЫВАЮЩАЯ ПРИБОР
*****************************************/
struct deviceStruct {
    QString type; // ТИП ПОДКЛЮЧЕНИЯ НС gsm/tcp/modbus
    QString name; // ИМЯ НС
    quint16 index; // ВНУТРЕННИЙ ИНДЕКС НС
    QString address; // АДРЕС НС
    quint32 tcpPort; //tcp-порт
    quint16 pumps; // КОЛИЧЕСТВО НАСОСОВ (ПО УМОЛЧАНИЮ = 6)
    quint16 map; // НОМЕР КАРТЫ, НА КОТОРОЙ НАХОДИТСЯ УКАЗАТЕЛЬ НС
    double x; //КООРДИНАТА УКАЗАТЕЛЯ ПО ОСИ X (0..1, ОТНОСИТЕЛЬНАЯ ВЕЛИЧИНА)
    double y; //КООРДИНАТА УКАЗАТЕЛЯ ПО ОСИ Y (0..1, ОТНОСИТЕЛЬНАЯ ВЕЛИЧИНА)
    quint64 period; // ПЕРИОД ОЖИДАНИЯ ОТВЕТА, ПОСЛЕ КОТОРОГО ВЫВОДИТСЯ СИГНАЛ "НЕТ ОТВЕТА". ДЛЯ gsm указывается в минутах, в противном случае - в секундах
    QMap<quint8,QString> errors; // МАССИВ ОПИСАНИЯ КОДОВ ОШИБОК УСТРОЙСТВА
    QString alx;//ОПИСАНИЕ ВХОДА ALX ИНФОРМАТОРА (для gsm)
    QString aly;//ОПИСАНИЕ ВХОДА ALY ИНФОРМАТОРА (для gsm)
    QString alw;//ОПИСАНИЕ ВХОДА ALW ИНФОРМАТОРА (для gsm)
    quint64 timeoutTimer; //ВРЕМЯ ОЖИДАНИЯ ДО ВЫРАБОТКИ СИГНАЛА "НЕТ ОТВЕТА". УКАЗАНО В СЕКУНДАХ
    QString outCurrentPDU; // СТРОКА КОМАНДЫ УПРАВЛЕНИЯ (1 - включить, 0 - выключить, 5 - запросить состояние, 9 - сбросить таймер информатора, -1 - нет команды)
    qint8 outCurrentStat; // СОСТОЯНИЕ ОТПРАВКИ КОМАНДЫ  (0 - нет команды, 1 - в очереди, 2 - отправляется, 3 - ожидает достави, 4 - ожидает ответа,)
    QDateTime outCurrentDate; //ДАТА, НУЖНАЯ ДЛЯ ОТПРАВЛЕНИЯ СООБЩЕНИЯ
    QDateTime tcpLastReport; //ВРЕМЯ ПОСЛЕДНЕГО ОТЧЕТА ДЛЯ TCP
    qint16 outCurrentNum; //Номер сообщения
    quint8 outCurrentRepeat; //Количество повторов
    status stat; // ТЕКУЩИЙ СТАТУС
    quint64 packsAll; //Все пакеты
    quint64 packsBad; //Неправильные пакеты
    quint64 packsNull; //Битые пакеты
    quint16 mdbAddr; //Адрес MODBUS
    quint16 mdbPage; //Страница MODBUS
    QString start; //Тип пуска
    quint8 view; //Тип отображения
    double flow; //Поток, м3/час
    quint64 working; //Нарабокта

};


class UI : public QMainWindow
{
    Q_OBJECT
    
public:
    /*****************************************************
        ОПИСАНИЕ ПЕРЕМЕННЫХ И ФУНКЦИЙ ДЛЯ КНС
    ****************************************************/
    //Переменные
    QMap<quint8,QString> errors; //ОШИБКИ
    quint16 minIndex; //Минимальное значение индекса
    quint16 maxIndex; //Максимальное значение индекса
    QMap<quint16,deviceStruct> deviceArray; //МАССИВ НС
    QTimer gsmTimer;    //ТАЙМЕР ДЛЯ ЦИКЛА ОТПРАВКИ GSM-СООБЩЕНИЙ
    QTimer tcpTimer;    //ТАЙМЕР ДЛЯ ЦИКЛА ОТПРАВКИ TCP-СООБЩЕНИЙ
    QTimer modbusTimer; //ТАЙМЕР ДЛЯ ЦИКЛА ОТПРАВКИ MODBUS-СООБЩЕНИЙ
    QTimer timeTimer; //Таймер для изменения времени в списках
    quint64 gsmIterator; //ИТЕРАТОР ДЛЯ ЦИКЛА ОТПРАВКИ GSM-СООБЩЕНИЙ
    deviceStruct tempDevice; //Временный элемент хранения НС
    QMap<quint16,deviceform * > deviceForms; //указатели на формы НС в списке
    QMap<quint16,devicePointer * > devicePointers; //указатели НС на карте
    QMap<quint16,deviceWindow * > deviceWindows; // массив окон НС
    QMap<quint16,deviceBuse *> deviceBuses; //Массив с бусами
    QWidget * deviceList; //Список для устройств
    QWidget * deviceList2; //2й вариант списка
    QMap<QString,QString> statArray; //Расшифровка статусов
    deviceEdit * devEdit;
    QList<quint16> historyPeriods;

    //API
    void _readDeviceParameters(int deviceIndex, QString address); //Прочитать параметры устройства по номеру
    void _readDeviceMistakes(quint16 deviceIndex); //Прочитать коды ошибок устройства по номеру
    void _getDeviceIndexList(); //Прочитать список устройств
    void _setDeviceFormData(quint16 index); //Установить значения формы по структуре
    void _sortDeviceList(); //Сртировка списка НС
    void _setPointerStatus(quint16 index, QString stat); //Установка настроек указателя
    void _movePointerTo(quint16 index, double x,double y); //Передвинуть указатель по координатам
    void _makePointerBigger(quint16 index); //Сделать указатель крупнее
    void _makePointerSmaller(quint16 index); //Сделать указатель меньше
    void _setPointerCoords(quint16 index,quint16 x, quint16 y); //Задать координаты указателя
    void _createDeviceWindow(quint16 index); //Создание она НС
    void _setDeviceWindowStatus(quint16 index, status stat); //Выставление статуса в окне
    void _setBuseStat(quint16 index); //Установка статуса бусы
    void _sortBuseList(); //Сортировка списка бус
    void _loadFullHistory(quint16 index, qint16 count); //Загрузка полнейшей истории
    void _addDevice(quint16 i); //Добавить устройство
    void _startWaiting(quint16 index, QString text); //Включить ожидание
    void _stopWaiting(quint16 index); //Выключить ожидание
    void renderIncomingTcpQuey(QString ip, QString tcpQuery); //Обработка TCP-послания
    void renderIncomingTcpNQuey(quint16 index, QString tcpQuery);

    //Функции
    void loadDevices(); //Функция загрузки устройств
    void addStatus(quint16 index, status stat); //Добавить статус в прибор
    void setPointerStatus(quint16 index, QString stat); //Изменить статус указателя
    void setFormType(quint16 index); //Приводит форму в нужный нам вид



    /*****************************************************
        ОПИСАНИЕ ПЕРЕМЕННЫХ И ФУНКЦИЙ ДЛЯ КАРТЫ
    *****************************************************/
    //Переменные
    bool gsmHere,tcpHere,mdbHere;
    quint16 mapCount; //КОЛИЧЕСТВО КАРТ
    QStringList maps; //МАССИВ С ИМЕНАМИ КАРТ
    quint16 currentMap; //Номер текущек карты
    QLabel * currentMapImage; //Указатель на изображение карты
    QLabel * mapNumText;
    bool fullScreen;
    void _saveFullScreen(bool on);
    QPushButton * editMaps;

    //API
    void _loadMap(int mapNum); //Загрузка карты по ее номеру

    //Функции
    void showConIcons();


    /*****************************************************
        ОПИСАНИЕ ПЕРЕМЕННЫХ И ФУНКЦИЙ ДЛЯ НАСТРОЕК
    ******************************************************/
    //Переменные
    downloadUpdate *downUpd;
    bool updateFileCorrect(QString fileName);
    QString currentVersion;
    deviceDialog * dev;
    exportDialog * exportDial;
    bool sortByStatus; //Сортировка по состоянию
    QString loadedSettingsString; //Строка, в которую будут загружаться требуемые натройки
    bool enableChangings; // Флаг разрешения изменений изменения
    QWidget * editPanel; //Указатель на меню настроек
    exitEditModeDialog * exitingDialog;
    password * passwordWindow; //Окно пароля
    editPassword * editPasswordWindow; //окно смены пароля
    editDialog * EditDialog; //Диалоговое окно для перетаскивания и редактирование НС и арт
    delDial * deleteDialog; // ВОпрос об удалении станции с карты
    QList<QAction *> portActions; //Массив действий для выбора порта
    about * About;
    QString alarmSound,messageSound;

    //API

    void _cleanSettingsField(); //Очищает поле с настройками
    void _setMapSettings(QString setStr);  //Выкорячивает настройки карт из строки
    void _setPortSettings(QString setStr); //Загружает настройки порта для GSM из строки
    void _setChangingsEnabled(bool on); // Закрывает или открывает настройки
    void _loadCommonErrors(); //Загрузка общих ошибок
    void _savePass(QString newPass); //Сохранение нвого пароля
    quint16 _moreThan(quint16 index); //Количество приборов с индексом меньше данного
    quint16 _lessThan(quint16 index); //Количество приборов с индексом больше данного
    void _setDefPort(QString defPort); //Сохранение дефолтного порта
    QString loadDefPort(); //Загрузка COM-порта

    //Функции
    void loadSettings(); //Загружает строку настроек из файла
    void saveSettings(); //Сохраняет настройки в файл
    void setCascadeWindows(); //Установка положений окон каскадом

    /******************************************************
        ОПИСАНИЕ СТАТУСОВ ПРИБОРОВ
     ******************************************************/
    //Переменные
    QTimer sendingTimer;
    bool sendingValue;
    qint64 timeFromLoading;

    //API
    status StringToStat(QString str); //Формирование статуса из строки
    void _loadDeviceStat(quint16 index); //Загрузка статистики прибора из файла
    void _saveDeviceStat(quint16 number, QString status); //Сохранить сообщение в лог
    void _loadDailyStat(quint16 id, QDate min, QDate max,QDate current); //Запрос из базы суточных графиков

    //Функции
    void renderIncomingSms(smsData data); //Обработка входящего смс с необходимыми действиями

    /********************************************************************
               COM-порт для модема GSM
     *******************************************************************/
    //Переменные
    QSerialPort Port; //Сам порт
    QTimer portQueryTimer;
    QString dataBuffer;
    QString currentPort;
    QDateTime lastReport;
    QSerialPortInfo portInfo;
    bool waitingTerminalFlag;
    bool terminalFlag;
    bool waitingSmsApproveFlag;
    bool smsApproveFlag;
    bool cdsFlag;
    bool reloadFlag;
    bool waitingSmsFlag;
    int waitingSmsTimer;
    int waitingTerminalTimer;
    int smsApproveTimer;
    bool smsFlag;
    bool waitingDeliverySmsFlag;
    int waitingDeliverySmsTimer;
    bool cnmiFlag;
    int regStat;
    int stepCounter;
    int regFlag;
    int toDelete;
    bool enableSending;
    QString currentPdu;
    QString previousPdu;
    QRegExp cmglRegExp;
    QRegExp cmgsRegExp;
    QRegExp incomingSmsRegExp;
    QRegExp pduRegExp;
    QRegExp cnmiRegExp;
    QRegExp cregRegExp;
    QRegExp csqRegExp;

    QLabel * gsmConnectionPixmap; //Указатель на соединение (изображение)
    QLabel * gsmConnectionText; //Уазатель на изображение (текст)

    QLabel * gsmSignalPixmap; //Указатель на сигнал (изображение)
    QLabel * gsmSignalText; //Указатель на сигнал (текст)

    QLabel * gsmSmsNumber; //Указатель на смс (нмер)

    //API
    void _initPort(); //Инициализация порта
    void _portStart(); //Старт GSM-прта
    smsData decodePDU(QString pdu); //Декдирование PDU
    QString encodePDU(QString number,QString data); //Кодирование в PDU
    void _moveGsmIterator();


    //Функции
    void saveGsmWorking(status stat, quint16 devId); //Сохранение суточных графиков
    void sendGSMData(); //Отправка данных из прта
    void analyseGSMData(QString data); //Анализ данных порта
    void setGsmConStat(QString stat); //Установить статус соединения
    void setGsmSignLev(QString lev); //Установить уровень сигнала


/*********************************************************************
                TCP-работа
 ********************************************************************/
    //Переменные
    TCPDevices tcpPort;
    quint64 tcpWaiting;
    QTimer connectionTimer;
    QMap<quint16,TModbusTCPDevice *> tcpDev;
    //API
    QString tcpToString(QByteArray byte);
    bool needToSave(quint16 index,status stat);

    //Функции

/***********************************************************************
            MODBUS
*/
    //Переменный
    mdb mdbPort;
    QThread mdbThread;
    QTimer mdbTimer;
    QTimer packsTimer;
    quint64 mdbWaiting;

    //Функции



/*********************************************************************
                ТРЕЙ ПРОГРАММЫ
 ********************************************************************/
    //Переменные
    QSystemTrayIcon * tray;
    bool reallyClose;
    QTimer conTimer;
    bool conCount;

    //Функции
    void closeEvent(QCloseEvent *ev); //Отрабатываем закрытие окна
    void trayMessage(QString header, QString text, int type = 1);
    void systemLog(QString header,QString text);
    void actionLog(QString header,QString text);


    explicit UI(QWidget *parent = 0);
    ~UI();
    Ui::UI *ui;

//************************************************
//              БАЗЫ ДАННЫХ
//*************************************************
    //Переменные
    QSqlDatabase base;
    bool connected();
    //Функции
    void openBase();

//************************************************
//            Функции, действия и прочее
//************************************************

    QAction * closeProgramAction;
    QAction * closeWindowAction;
    QAction * showWindowAction;
    QMenu * trayMenu;
    QAction * reloadModemAction;

public slots:
    void addTcpDev(quint16 index,QString address,quint16 port,quint32 tcpPort);
    void addQueryFromDevice(quint16 index, QString deviceCommand); // Отправить сфрмировать сообщение для отправки
    void showPreviousMap(); //Показать предыдущую карту
    void showNextMap(); //Показать следущую карту
    void showChangings(); //Открываем доступ к редактированию
    void hovered(quint16 index); //Слот наведения мышки на указатель
    void left(quint16 index); //Убрать мышку с указателя
    void dblckick(quint16 index); //Обработка двойного щелчка
    void movePointer(quint16 index);//Обработка перемещения мыши
    void readGSMPortData(); //Чтение данных из порта по таймеру
    void gsmTimerCycle(); //Цикл работы с gsm-приборами.
    void tcpTimerCycle(); //Цикл работы с TCP
    void rejectDeviceEdit(); //Функция удаления окна редактирования
    void acceptDeviceEdit(); //Функция изменения НС через окно редактирования
    void errorDeviceEdit(QString err); //Обработка вывода ошибок от окна редактирования
    void deviceEditIndexChanged(int index);//Обработка смены индекса
    void addNewStationForm(); //Форма добавления новой станции
    void addNewStation(); //Добавить новую станцию
    void deleteDevice(); //Удалить НС
    void addSmsToQuery(quint16 index,QString command); //Добавить смс в очередь
    void changeTimeInList(); //Поменять время в списке
    void changeListView();
    void turnHistory(quint16 index);
    void showHistory(); //Показать элемент истории
    void hideHistory(); //Скрыть элемент истории
    void showFullHistory(quint16 index, qint16 count); //Поазать всю историю НС
    void showHistoryStat(quint16 index); //Расшифровать историю из архива
    void closeWindowSlot(); //Закрыть окно по действию
    void closeProgramSlot(); //Закрыть программу по действию
    void showWindowSlot(); //Открыть окно программы по действию
    void reloadModemSlot(); //Перезагрузка модема по действию
    void showExitSettingsDialog(); //Выход из режима редактирования
    void saveNewSettings(); //Сохранение введенных настроек
    void exitEditMode(); //Выход из режима редактирования
    void closeExitSettingsDialog(); //Закрыть диалог выхода из режима редатирования
    void addMapPressed(); //Нажата кнопка добавления карты
    void editMapPressed(); //Надата кнопка смены изображения карты
    void editMapOk(); //поменять изображение у карты
    void deleteMapPressed(); //Удалить карту
    void addDeviceToCurrentMapPressed(); //Перенести указатель НС на карту
    void addDeviceToCurrentMapOk(); //Перенести уазатель НС на карту
    void removeDeviceFromCurrentMapPressed(); //Удалить НС с текущей карты
    void removeDeviceFromCurrentMapOk(); //Удалить НС с текущей карты
    void savePassword(); //Сохранение формы
    void changePassword(); //Вывод формы смены пароля
    void showPasswordWindow(); //Показать форму пароля
    void closePasswordWindow(); //Скрыть диалог с паролем
    void goToEditMode(); //Войти в режим настроек
    void showEditPasswordWindow(); //Показать окно изменения пароля
    void closeEditPasswordWindow(); //Срыть окно изменения пароля
    void deleteDeviceFromMap(quint16 index); //Удалить НС с карты по перетаскиванию влево
    void closeDelDial(); //Закрыть окно с вопросом об удалении НС
    void deviceFormMovedToMap(quint16 index); //Панелька с карты перетасивается
    void deviceFormReleasedToMap(quint16 index); //Панелька с карты закончила свое перетаскивание
    void deviceBuseMovedToMap(quint16 index); //
    void deviceBuseReleasedToMap(quint16 index); //
    void fullScreenMode(bool on); //Переход в полноэкранный режим
    void showAbout(); //Показать окно "О программе"
    void closeAbout(); //Закрыть окно о программе
    void setAvailablePorts(); //Создать доступные порты
    void chooseAvailablePort(); //Выбрать порт по умолчанию
    void sendingTimerChanged(); //Функция для обрабтки таймера по миганию
    void openHelp(); //Открыть помощь
    void crashMemory(); //Сломать память
    void clearGsmQueue(); //Очистить очередь
    void setTcpStat(); //Выставляем TCP статус
    void renderTcpResponse(QByteArray status,QBitArray channels,QString ip);
    void renderTcpNResponse(QByteArray status);
    void renderMdbResponse(quint16 index,QString answer);
    void mdbTimerCycle();
    void changeSortType(bool ch);
    void resizeAll();
    void showEditDialog();
    void closeEditDialog();
    void setMdbStat();
    void mdbNoSignal(quint16 index, quint8 err);
    void showWindowInfo(quint16 index);
    void readConStat();
    void writeConStat();
    void parseTCPError(QString ip);
    void deleteDevice(quint16 index);
    void windowConTimer();
    void deviceWindowMoved();
    void deviceWindowReleased();
    void scrollList(int x);
    void showD(QString t);
    void getDaily(quint16 id, QDate min, QDate max, QDate current);
    void saveDaily(quint16 id, QDate min, QDate max);
    void getMonth(quint16 id,int month, int year);
    void m_save(quint16 id, int month, int year);
    void getYear(quint16 id, int year);
    void y_save(quint16 id, int year);
    void exportToCsv();
    void showChangesLog();
    void showBackup();
    void backUp(QString ver);
    void showBackupLog();

    void messageFromDownloader(QString h, QString t, int act);

    void getFocus(QWidget * old, QWidget * now);

signals:
    void showHelp(QString);
    void AddDevice(QString,quint16,quint16,quint8);
    void RemoveDevice(quint8);
    void mdbSetMessage(quint16,QString);
};

#endif // UI_H
