#ifndef CHANGESLOG_H
#define CHANGESLOG_H

#include <QDialog>

namespace Ui {
class changesLog;
}

class changesLog : public QDialog
{
    Q_OBJECT

public:
    explicit changesLog(QWidget *parent = 0);
    ~changesLog();
    void append(QString text);

private:
    Ui::changesLog *ui;
};

#endif // CHANGESLOG_H
