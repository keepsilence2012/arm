#ifndef STATINFO_H
#define STATINFO_H

#include <QWidget>
#include <QDialog>
#include <QLabel>

namespace Ui {
class statInfo;
}

class statInfo : public QDialog
{
    Q_OBJECT
    
public:
    explicit statInfo(QWidget *parent = 0);
    ~statInfo();
    quint16 index;
    QLabel * lab;
signals:
    void drop(quint16);
public slots:
    void drPr();
private:
    Ui::statInfo *ui;
};

#endif // STATINFO_H
