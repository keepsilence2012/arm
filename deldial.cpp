#include "deldial.h"
#include "ui_deldial.h"

delDial::delDial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::delDial)
{
    ui->setupUi(this);
    connect(ui->ok,SIGNAL(clicked()),this,SLOT(okpr()));
    connect(ui->no,SIGNAL(clicked()),this,SIGNAL(no()));
    setWindowTitle("Удаление НС");
    setModal(true);
    index = 0;
}

delDial::~delDial()
{
    delete ui;
}

void delDial::okpr() {
    emit ok(index);
}
