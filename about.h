#ifndef ABOUT_H
#define ABOUT_H

#include <QCloseEvent>
#include <QDialog>
#include <QWidget>
#include <QDate>

namespace Ui {
class about;
}

class about : public QDialog
{
    Q_OBJECT
    
public:
    explicit about(QWidget *parent = 0);
    ~about();
    void closeEvent(QCloseEvent *ev);
    Ui::about *ui;
    void setDate();
    void setVersion(QString ver);
signals:
    void closed();
};

#endif // ABOUT_H
