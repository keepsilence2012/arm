#-------------------------------------------------
#
# Project created by QtCreator 2013-12-17T12:28:46
#
#-------------------------------------------------

QT       += core gui gui-private serialport network sql multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gsm_1_1
TEMPLATE = app

SOURCES += main.cpp\
        ui.cpp \
    settings.cpp \
    controller.cpp \
    devices.cpp \
    maps.cpp \
    deviceform.cpp \
    devicepointer.cpp \
    status.cpp \
    port.cpp \
    pdu.cpp \
    devicewindow.cpp \
    work.cpp \
    deviceedit.cpp \
    devicebuse.cpp \
    exiteditmodedialog.cpp \
    editdialog.cpp \
    password.cpp \
    editpassword.cpp \
    deldial.cpp \
    about.cpp \
    deldevdial.cpp \
    tcp.cpp \
    tcpFunc.cpp \
    mdbFunc.cpp \
    mdb.cpp \
    devicedialog.cpp \
    statinfo.cpp \
    focusclasses.cpp \
    pressure.cpp \
    pressure_2.cpp \
    base.cpp \
    tcp_2.cpp \
    dailyworking.cpp \
    exportdialog.cpp \
    calwid.cpp \
    statwidget.cpp \
    downloadupdate.cpp \
    downloadwidget.cpp \
    askdialog.cpp \
    changeslog.cpp \
    backupwindow.cpp

HEADERS  += ui.h \
    deviceform.h \
    devicepointer.h \
    devicewindow.h \
    deviceedit.h \
    devicebuse.h \
    exiteditmodedialog.h \
    editdialog.h \
    password.h \
    editpassword.h \
    deldial.h \
    about.h \
    deldevdial.h \
    tcp.h \
    mdb.h \
    devicedialog.h \
    statinfo.h \
    focusclasses.h \
    pressure.h \
    pressure_2.h \
    tcp_2.h \
    dailyworking.h \
    exportdialog.h \
    calwid.h \
    statwidget.h \
    downloadupdate.h \
    downloadwidget.h \
    askdialog.h \
    changeslog.h \
    backupwindow.h

FORMS    += ui.ui \
    deviceform.ui \
    devicepointer.ui \
    devicewindow.ui \
    deviceedit.ui \
    devicebuse.ui \
    exiteditmodedialog.ui \
    editdialog.ui \
    password.ui \
    editpassword.ui \
    deldial.ui \
    about.ui \
    deldevdial.ui \
    devicedialog.ui \
    statinfo.ui \
    pressure.ui \
    pressure_2.ui \
    dailyworking.ui \
    exportdialog.ui \
    calwid.ui \
    statwidget.ui \
    downloadwidget.ui \
    askdialog.ui \
    changeslog.ui \
    backupwindow.ui

RESOURCES += \
    res.qrc
RC_FILE = C:/icon.rc

OTHER_FILES +=
