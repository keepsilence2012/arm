#ifndef PRESSURE_H
#define PRESSURE_H

#include <QDialog>
#include <QSlider>
#include <QList>
#include <QLabel>
#include <QPushButton>
#include <QDebug>

namespace Ui {
class pressure;
}

class pressure : public QDialog
{
    Q_OBJECT
    
public:
    explicit pressure(QWidget *parent = 0);
    ~pressure();
    QList<QSlider *> sliders;
    QList<QPushButton*> ups;
    QList<QPushButton*> downs;
    QList<QLabel *> presses;
public slots:
    void sliderMoved();
    void slideUp();
    void slideDown();
private:
    Ui::pressure *ui;
};

#endif // PRESSURE_H
