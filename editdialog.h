#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QListWidget>

namespace Ui {
class editDialog;
}

class editDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit editDialog(QWidget *parent = 0);
    ~editDialog();
    Ui::editDialog *ui;
    QListWidget * list;
signals:
    void ok();
    void no();
    void del();
};

#endif // EDITDIALOG_H
