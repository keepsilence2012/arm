#include "ui.h"

//Функция включения карты
void UI::_loadMap(int mapNum) {
    trayMessage("Переключение карт: ","Показана карта номер" + QString::number(mapNum),6);
    //Если номер корректный, делаем следущее
    if (mapNum < mapCount) {
        currentMap = mapNum;
        //Загружаем рисунок карты
        currentMapImage->setPixmap(QPixmap(MAIN_PATH + "maps/" + maps[mapNum]).scaled(currentMapImage->size(),Qt::IgnoreAspectRatio));

        //Включаем нужные нам указатели
        foreach(quint16 i, deviceArray.keys()) if (devicePointers.contains(i)) devicePointers[i]->setVisible(deviceArray[i].map == currentMap + 1);

        //Делаем запись о номере текущей карты
        mapNumText->setText("Карта " + QString::number(currentMap + 1) + " из " +QString::number(mapCount));
    }
}
