#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QTreeView>
#include <QTreeWidgetItem>
#include <QDate>
#include <calwid.h>

namespace Ui {
class exportDialog;
}

class exportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit exportDialog(QWidget *parent = 0);
    ~exportDialog();
    QTreeWidget * wid;
    void addItem(quint16 index, QString name);
    QList<quint16> checkedNums();
    calWid *toC, *fromC;
    QDate to();
    QDate from();
public slots:
    void selectItems();
    void tryToAccept();
    void toggleTriggered(QTreeWidgetItem * item);
private:
    Ui::exportDialog *ui;
};

#endif // EXPORTDIALOG_H
