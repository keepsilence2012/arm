#include "deldevdial.h"
#include "ui_deldevdial.h"

delDevDial::delDevDial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::delDevDial)
{
    ui->setupUi(this);
    setWindowTitle("Удаление НС");
    setModal(true);
}

delDevDial::~delDevDial()
{
    delete ui;
}
