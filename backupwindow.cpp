#include "backupwindow.h"
#include "ui_backupwindow.h"

backupWindow::backupWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::backupWindow)
{
    ui->setupUi(this);
    ui->list->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(ui->ok,SIGNAL(clicked()),this,SLOT(okPr()));
    connect(ui->list,SIGNAL(currentRowChanged(int)),this,SLOT(selCh()));
}

backupWindow::~backupWindow()
{
    delete ui;
}

void backupWindow::addItem(QString text, QString value) {
    ui->list->addItem(new QListWidgetItem(text));
    ui->list->item(ui->list->count() - 1)->setData(Qt::UserRole,QVariant(value));
    ui->list->setCurrentRow(0);
}

QString backupWindow::value() {
    return (ui->list->selectedItems().count()) ? ui->list->selectedItems()[0]->data(Qt::UserRole).toString() : "";
}

void backupWindow::okPr() {
    if (value() != "") accept();
}

void backupWindow::selCh() {
    qDebug() << "item changed";
    ui->ok->setEnabled(value() != "");
}
