#ifndef EDITPASSWORD_H
#define EDITPASSWORD_H

#include <QDialog>
#include <QLineEdit>
#include <QCloseEvent>

namespace Ui {
class editPassword;
}

class editPassword : public QDialog
{
    Q_OBJECT
    
public:
    explicit editPassword(QWidget *parent = 0);
    ~editPassword();
    Ui::editPassword *ui;
    QLineEdit * old;
    QLineEdit * new1;
    QLineEdit * new2;
    bool reallyClose;
    void closeEvent(QCloseEvent *ev);
signals:
    void ok();
    void no();
public slots:

};

#endif // EDITPASSWORD_H
