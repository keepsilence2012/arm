#include "ui.h"
#include <QApplication>
#include <QThread>
#include <windows.h>
#include <signal.h>

//Обработка битья памяти
void handler_sigfpe(int signum)
{
    //MessageBoxA(NULL,"Critical error","Program will be closed",MB_ICONSTOP);
    //QDialog dial(0);
    //if (dial.exec() == QDialog::Accepted) {
    //QMessageBox::warning(0,"error","error");
    // открепить обработчик и явно завершить приложение
    //signal(signum, SIG_DFL);
    exit(3);
    //} else exit(9);
    exit(9);
}

QThread * thread;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    UI w;
    signal(SIGSEGV,handler_sigfpe);
    w.show();
    if (argc > 1 && QString(argv[1]) == "after_update") w.showChangesLog();
    if (argc > 1 && QString(argv[1]) == "after_backup") w.showBackupLog();
    a.connect(&a,SIGNAL(focusChanged(QWidget*,QWidget*)),&w,SLOT(getFocus(QWidget*,QWidget*)));
    w.setCascadeWindows();
    return a.exec();
}
