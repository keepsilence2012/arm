#include "devicebuse.h"
#include "ui_devicebuse.h"

deviceBuse::deviceBuse(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deviceBuse)
{
    ui->setupUi(this);
    index = 0;
    text = ui->text;
    image = ui->image;
    moving = false;
}

deviceBuse::~deviceBuse()
{
    delete ui;
}

void deviceBuse::enterEvent(QEvent *ev) {
    Q_UNUSED(ev)
    emit hovered(index);
}

void deviceBuse::leaveEvent(QEvent *ev) {
    Q_UNUSED(ev)
    emit left(index);
}

void deviceBuse::mouseDoubleClickEvent(QMouseEvent *ev) {
    emit dbl(index);
}

void deviceBuse::mouseMoveEvent(QMouseEvent *ev) {
    if (ev->buttons() & Qt::LeftButton) {
        emit moved(index);
        moving = true;
    }
}

void deviceBuse::mouseReleaseEvent(QMouseEvent *ev) {
    setCursor(QCursor(Qt::PointingHandCursor));
    emit released(index);
    moving = false;
}
