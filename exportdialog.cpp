#include "exportdialog.h"
#include "ui_exportdialog.h"

exportDialog::exportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exportDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(tryToAccept()));
    connect(ui->tree,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(toggleTriggered(QTreeWidgetItem * )));
    connect(ui->tree,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(selectItems()));
    wid = ui->tree;
    ui->to->setMaximumDate(QDate::currentDate());
    ui->from->setMaximumDate(QDate::currentDate());
    ui->to->setMinimumDate(QDate(2014,1,1));
    ui->from->setMinimumDate(QDate(2014,1,1));
    wid->topLevelItem(0)->setCheckState(0,Qt::Unchecked);
    setWindowTitle("Экспорт наработки");
}

exportDialog::~exportDialog()
{
    delete ui;
}

void exportDialog::tryToAccept() {
    if (1) accept();
}

void exportDialog::addItem(quint16 index, QString name) {
    wid->setColumnCount(1);
    QTreeWidgetItem * item = new QTreeWidgetItem(wid->topLevelItem(0),index);
    item->setText(0,name);
    item->setCheckState(0,Qt::Unchecked);
    wid->topLevelItem(0)->setExpanded(true);
}
QList<quint16> exportDialog::checkedNums() {
    QList<quint16> out;
    for (int i=0;i<wid->topLevelItem(0)->childCount();i++) {
        if (wid->topLevelItem(0)->child(i)->checkState(0) == Qt::Checked) out.append(wid->topLevelItem(0)->child(i)->type());
    }

    return out;
}

void exportDialog::toggleTriggered(QTreeWidgetItem * item) {
    Q_UNUSED(item)
    wid->clearSelection();
}

QDate exportDialog::from() {
    return ui->from->date();
}


QDate exportDialog::to() {
    return ui->to->date();
}

void exportDialog::selectItems() {
//    bool checked = false, unchecked = false;
//    for (int i=0;i<wid->topLevelItem(0)->childCount();i++) {
//        if (wid->topLevelItem(0)->child(i)->checkState(0) == Qt::Checked) checked = true;
//        if (wid->topLevelItem(0)->child(i)->checkState(0) == Qt::Unchecked) unchecked = false;
//    }

//    if (checked && unchecked) wid->topLevelItem(0)->setCheckState(0,Qt::PartiallyChecked);
//    else if (checked) wid->topLevelItem(0)->setCheckState(0,Qt::Checked);
//    else wid->topLevelItem(0)->setCheckState(0,Qt::Unchecked);
}
