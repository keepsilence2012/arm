#include "about.h"
#include "ui_about.h"

about::about(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::about)
{
    ui->setupUi(this);
    connect(ui->close,SIGNAL(clicked()),this,SLOT(close()));
    setModal(true);
    setWindowTitle("О программе");
    setDate();
}

about::~about()
{
    delete ui;
}

void about::closeEvent(QCloseEvent *ev) {
    emit closed();
    ev->accept();
}

void about::setDate() {
    ui->date->setText("2013-" + QDate::currentDate().toString("yyyy") + " г ");
}

void about::setVersion(QString ver) {
    ui->version->setText("ver " + ver);
}
