#include "password.h"
#include "ui_password.h"

password::password(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::password)
{
    ui->setupUi(this);

    reallyClose = false;
    enterPressed = new QShortcut(Qt::Key_Enter,this);
    pass = QDate::currentDate().toString("yyyyMMdd");
    setModal(true);
    setWindowTitle("Пароль");
    connect(enterPressed,SIGNAL(activated()),ui->ok,SIGNAL(clicked()));
    connect(ui->ok,SIGNAL(clicked()),this,SLOT(okPressed()));
    connect(ui->no,SIGNAL(clicked()),this,SIGNAL(no()));
    setWindowFlags(windowFlags()| Qt::WindowStaysOnTopHint);
}

password::~password()
{
    delete ui;
}

void password::okPressed() {
    if (ui->pass->text() == pass || ui->pass->text() == QDate::currentDate().toString("yyyyMMdd")) {
        emit ok();
    } else {
        ui->pass->setText("");
    }
}

void password::closeEvent(QCloseEvent *ev) {
    if (reallyClose) {
        ev->accept();
    } else {
        emit no();
        ev->ignore();
    };
}
