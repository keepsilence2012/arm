#ifndef ASKDIALOG_H
#define ASKDIALOG_H

#include <QDialog>

namespace Ui {
class askDialog;
}

class askDialog : public QDialog
{
    Q_OBJECT

public:
    explicit askDialog(QWidget *parent = 0);
    ~askDialog();
    bool saveSettings();
    bool saveStations();

private:
    Ui::askDialog *ui;
};

#endif // ASKDIALOG_H
