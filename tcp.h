#ifndef TCP_H
#define TCP_H

#include <QMainWindow>
#include <QDebug>
#include <QTcpSocket>
#include <QDebug>
#include <QCloseEvent>
#include <QThread>
#include <QObject>
#include <QBitArray>
#include <QtCore/qmath.h>
#include <QTimer>
#include <QList>

//#define DEBUG
#define ERRORDEBUG
#define DEBUG_MODE true

#define ModbusTCP_PORT 55555
#define ModbusTCP_TIMEOUT 3000
#define Response_Wait_Counter 8
#define Update_Interval 1000
#define MAXTHREADS 20


namespace Ui {
class MyThread;
class WriteThread;
class StatusThread;
class ModbusTCP;
class TCPDevices;
}

class MyThread:public QThread
{
    Q_OBJECT
signals:
    void error(QString, int);
public:
    QByteArray socket_req(QString debugstr, QByteArray req, QString rip, int prt, int tmout);
    MyThread(QObject * parent = 0);
};

class WriteThread:public MyThread
{
    Q_OBJECT
signals:
    void success(int par_num_h, int par_num_l, int par_set_h, int par_set_l);
    void fail(int par_num_h, int par_num_l, int par_set_h, int par_set_l);
public:
     WriteThread(quint8 addr, QObject * parent = 0);
    //
    bool buzy;
    quint8 address;
    void run();
    void load(QString sip, int prt, int tmout, int par_num_h, int par_num_l, int par_set_h, int par_set_l);
private:
    QString ip;
    int port;
    int Timeout;
    QByteArray Request;
    QByteArray Response;
};

class StatusThread : public MyThread
 {
    Q_OBJECT

signals:
    void sys_st(QByteArray);
public:
     StatusThread(quint8 addr, QObject * parent = 0);
    //
    void load(QString sip, int prt, int tmout);
    void run();
    quint8 address;
private:
    QString ip;
    QTcpSocket *Socket;
    bool buzy;
    int port;
    int Timeout;
};

class ModbusTCP : public QObject
{
    Q_OBJECT
signals:
    void error(QString, int);
    void status_send(QByteArray, QBitArray, QString);
public slots:
    void err(QString ip, int code);
    void load();
    void del();
    void makeChan(int code);
    void set_par(int par_num_h, int par_num_l, int par_set_h, int par_set_l);
    void system_status(QByteArray stat);
    void WriteFail(int par_num_h, int par_num_l, int par_set_h, int par_set_l);
    void WriteSuccess(int par_num_h, int par_num_l, int par_set_h, int par_set_l);
public:
    void ch_response(int byte_num);
    int find_free_write_thread();
    void ch_switch(int ch_num, int ch_set);
    void ch_off();
    //
    ModbusTCP(QString ip_set, quint8 addr, QObject * parent = 0);
    ~ModbusTCP();
    //
    QByteArray status;
    QBitArray Channels;
    QTimer status_timer;
    QTcpSocket *Socket;
    bool buzy;
    QString ip;
    quint8 address;
    int ch_wait;
    //
    QList<WriteThread *> WriteReq;
    StatusThread * cycle;
private:
    int port;
    int Timeout;
};

class TCPDevices : public QObject
{
    Q_OBJECT
public:
    TCPDevices(QObject * par = 0);
    //
    QList<ModbusTCP *> MDBTCP;
    //
    bool start;
    int ip_match(QString ip);
public slots:
    void err(QString ip, int code);
    void add(QString ip, quint8 addr);
    void del(QString ip);
    void makeChan(QString ip, int code);
    void ch_switch(QString ip, int ch_num, int ch_set);
    void ch_off(QString ip);
    void set_par(QString ip, int par_num_h, int par_num_l, int par_set_h, int par_set_l);
    void got_status(QByteArray Status, QBitArray Channels, QString ip);
signals:
    void status(QByteArray, QBitArray, QString);
    void error(QString, int);
};
#endif // TCP_H
