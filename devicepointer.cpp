#include "devicepointer.h"
#include "ui_devicepointer.h"

devicePointer::devicePointer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::devicePointer)
{
    ui->setupUi(this);

    //Устанавливаем указатели
    pointer = ui->label;
    index = ui->label_2;
}

devicePointer::~devicePointer()
{
    delete ui;
}

void devicePointer::enterEvent(QEvent *ev) {
    Q_UNUSED(ev);
    emit hovered(index->text().toUInt());
}

void devicePointer::leaveEvent(QEvent *ev) {
    Q_UNUSED(ev);
    emit left(index->text().toUInt());
}

void devicePointer::mouseDoubleClickEvent(QMouseEvent *ev) {
    Q_UNUSED(ev);
    emit dblclick(index->text().toUInt());
}

void devicePointer::mouseMoveEvent(QMouseEvent *ev) {
    if (ev->buttons() & Qt::LeftButton) {
        emit moved(index->text().toUInt());
        setCursor(QCursor(Qt::ClosedHandCursor));
    }
}

void devicePointer::mouseReleaseEvent(QMouseEvent *ev) {
    setCursor(QCursor(Qt::PointingHandCursor));
}
