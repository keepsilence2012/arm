#include "ui.h"
//Функция инициализации прта
void UI::_initPort() {
    portQueryTimer.setInterval(200);
    cdsFlag = false;
    currentPdu = "";
    cmgsRegExp.setPattern("CMGS:\\s([0-9]{1,3})");
    incomingSmsRegExp.setPattern("CMT:\\s([0-9]{0,2}),([0-9]{1,3})");
    pduRegExp.setPattern("([0-9A-Fa-f]{10,352})");
    cmglRegExp.setPattern("CMGL:\\s([0-9]{1,2})");
    cnmiRegExp.setPattern("CNMI:\\s[0-9]{1},([0-9]{1}),[0-9]{1},([0-9]{1}),[0-9]{1}");
    cregRegExp.setPattern("CREG:\\s([0-9]),([0-9])");
    csqRegExp.setPattern("CSQ:\\s([0-9]{1,2})");

    waitingTerminalFlag = false;
    terminalFlag = false;
    waitingSmsApproveFlag = false;
    smsApproveFlag = false;
    waitingSmsFlag = false;
    waitingSmsTimer = 0;
    waitingTerminalTimer = 0;
    smsApproveTimer = 0;
    smsFlag = false;
    reloadFlag = false;
    waitingDeliverySmsFlag = false;
    waitingDeliverySmsTimer = 0;
    cnmiFlag = false;
    regStat = 0;
    regFlag = 0;
    stepCounter = 0;
    connect(&portQueryTimer,SIGNAL(timeout()),this,SLOT(readGSMPortData()));
}
//Старт порта
void UI::_portStart() {
    bool check = false;
    if (QSerialPortInfo::availablePorts().count() == 0);
    else {
        if (Port.open(QIODevice::ReadWrite)) {check = true; lastReport = QDateTime().currentDateTime();};
        _setPortSettings("");
        if (check) {
            if (DEBUG_MODE) qDebug() << "СТАРТ ПОРТА: pot started!";
            portQueryTimer.start();
        } else {
            trayMessage("Невозможно подлючиться к порту по умолчанию","Выберите другой порт!",3);
        };
    };
}
//Установить уровень GSM - соединения
void UI::setGsmConStat(QString stat) {
    if (stat == "0") {
        gsmConnectionPixmap->setPixmap(QPixmap(":/images/noConnectionBig.png").scaled(gsmConnectionPixmap->geometry().width(),gsmConnectionPixmap->geometry().height()));
        gsmConnectionPixmap->setToolTip("Нет соединения");
        gsmConnectionText->setText("Нет\nсоединения");
        gsmSignalPixmap->setEnabled(false);
        enableSending = false;
    } else if (stat == "1") {
        gsmConnectionPixmap->setPixmap(QPixmap(":/images/connectionOnBig.png").scaled(gsmConnectionPixmap->geometry().width(),gsmConnectionPixmap->geometry().height()));
        gsmConnectionPixmap->setToolTip("Подключено к сети");
        gsmConnectionText->setText("Соединение\nустановлено");
        gsmSignalPixmap->setEnabled(true);
        enableSending = true;
    } else {
        gsmConnectionPixmap->setPixmap(QPixmap(":/images/connectionOffBig.png").scaled(gsmConnectionPixmap->geometry().width(),gsmConnectionPixmap->geometry().height()));
        gsmConnectionPixmap->setToolTip("Ошибка соединения");
        gsmConnectionText->setText("Подключение\nк сети");
        gsmSignalPixmap->setEnabled(false);
        enableSending = false;
    };
}

//Установить уровень GSM-сигнала
void UI::setGsmSignLev(QString lev) {
    if (lev.toInt() == 0) {
        gsmSignalPixmap->setPixmap(QPixmap(":/images/signal0Big.png").scaled(gsmSignalPixmap->geometry().width(),gsmSignalPixmap->geometry().height()));
        gsmSignalPixmap->setToolTip("Нет сигнала");
        gsmSignalText->setText("Нет\nсигнала");
    }
    else if (lev.toInt() < 8) {
        gsmSignalPixmap->setPixmap(QPixmap(":/images/signal25Big.png").scaled(gsmSignalPixmap->geometry().width(),gsmSignalPixmap->geometry().height()));
        gsmSignalPixmap->setToolTip("Плохой сигнал");
        gsmSignalText->setText("Плохой\nсигнал");
    }
    else if (lev.toInt() < 16) {
        gsmSignalPixmap->setPixmap(QPixmap(":/images/signal50Big.png").scaled(gsmSignalPixmap->geometry().width(),gsmSignalPixmap->geometry().height()));
        gsmSignalPixmap->setToolTip("Средний сигнал");
        gsmSignalText->setText("Средний\nсигнал");
    }
    else if (lev.toInt() < 24) {
        gsmSignalPixmap->setPixmap(QPixmap(":/images/signal75Big.png").scaled(gsmSignalPixmap->geometry().width(),gsmSignalPixmap->geometry().height()));
        gsmSignalPixmap->setToolTip("Хороший сигнал");
        gsmSignalText->setText("Хороший\nсигнал");
    }
    else {
        gsmSignalPixmap->setPixmap(QPixmap(":/images/signal100Big.png").scaled(60,30));
        gsmSignalPixmap->setToolTip("Отличный сигнал");
        gsmSignalText->setText("Отличный\nсигнал");
    };
}

//Чтение данных из прта по таймеру
void UI::readGSMPortData() {
    portQueryTimer.stop();
    dataBuffer += QString(Port.readAll());
    //qDebug() << "port data " <<dataBuffer;
    if (dataBuffer.contains("\r\n") || dataBuffer.contains(QRegExp("^>"))) {
        lastReport = QDateTime().currentDateTime();
        //if (DEBUG_MODE) qDebug() << dataBuffer.split("\r\n").first();
        analyseGSMData(dataBuffer.split("\r\n").first());
        dataBuffer = dataBuffer.section("\r\n",1);
    };

    if ((QDateTime().currentMSecsSinceEpoch() - lastReport.toMSecsSinceEpoch()) > 60000) {
        //Что делать, если порт наебнулся
        if (enableSending) trayMessage("Нет ответа от GSM-модема","Проверьте, правильно ли указан COM-порт. Если COM-порт указан правильно, попробуйте перезагрузить модем.",3);
        setGsmConStat("0");
        setGsmSignLev("0");
    }
    else {
        //Работает
    };

    sendGSMData();
}

//Анализ данных из порта
void UI::analyseGSMData(QString data) {
    QString s;
    //qDebug() << data;
    int i;
    if (data.contains(QRegExp("^>"))) {
        waitingTerminalFlag = false;
        terminalFlag = true;
        waitingTerminalTimer = 0;

    } else if (data.contains(cmgsRegExp)) {
        waitingSmsApproveFlag = false;
        smsApproveFlag = true;
        smsApproveTimer = 0;
        if (deviceArray.contains(gsmIterator) && deviceArray[gsmIterator].outCurrentStat == 2) {
            deviceArray[gsmIterator].outCurrentStat = 3;
            deviceArray[gsmIterator].outCurrentNum = cmgsRegExp.cap(1).toInt();
        }
        if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: sms" << "1" << cmgsRegExp.cap(1);  // ЗДЕСЬ СООБЩЕНИЕ ОТПРАВЛЕНО!!!
    } else if (data.contains("ERROR") && waitingSmsApproveFlag ) {
        //smsApproveFlag = true;
        //smsApproveTimer = 0;
        waitingSmsApproveFlag = false;
        if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: sms" << "1" << "ERROR";
        if (deviceArray.contains(gsmIterator)) deviceArray[gsmIterator].outCurrentStat = 1;
    } else if (data.contains(cmglRegExp)) {
        toDelete = cmglRegExp.cap(1).toInt();
    } else if (data.contains(pduRegExp)) {
        s = pduRegExp.cap(1);
        i = QString().append(s.at(0)).append(s.at(1)).toInt();
        s.clear();
        s.append(pduRegExp.cap(1).at((i+1)*2)).append(pduRegExp.cap(1).at((i+1)*2 + 1));
        //qDebug() << i << ";" << pduRegExp.cap(1) << ";" << s;
        if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: " << s << ";" << s.toInt(0,16) << ";" << pduRegExp.cap(1);
        i = s.toInt(0,16);
        if ((i & 3) == 0)
        {
            //Если сообщение - входящее
            smsFlag = true;
            renderIncomingSms(decodePDU(pduRegExp.cap(1)));
            if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: sms" << "0" << pduRegExp.cap(1); //ВХОДЯЩЕЕ СМС
        } else if ((i & 3) == 2)
        {
            smsFlag = true;
            waitingSmsApproveFlag = false;
            s.clear();
            i = QString().append(pduRegExp.cap(1).at(0)).append(pduRegExp.cap(1).at(1)).toInt();
            s.append(pduRegExp.cap(1).at((i+2)*2)).append(pduRegExp.cap(1).at((i+2)*2 + 1));
            if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: sms" << "2" << s.toInt(0,16); //Отчет о доставке сообщения
            //Выставляем состояние, статус и тп
            foreach (quint16 j, deviceArray.keys()) {
                //Если состояние - 3 и номер сообщения,

                if (deviceArray[j].outCurrentStat == 3 && deviceArray[j].outCurrentNum == s.toInt(0,16)) {
                    deviceArray[j].outCurrentStat = 4;
                    deviceWindows[j]->SetWindowTitle(deviceArray[j].name + " - *Ожидание ответа*");
                    deviceArray[j].outCurrentNum = -1;
                    deviceArray[j].outCurrentDate = QDateTime().currentDateTime();
                    if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: сообщение доставлено!";
                };
            };
        };
    } else if (data.contains(QRegExp("SYSSTART"))) {
        //if (DEBUG_MODE) qDebug() << "status" << "reloaded";
        tray->showMessage("GSM-модем","Модем успешно перезагружен.");
    } else if (data.contains(cnmiRegExp)) {
        if (cnmiRegExp.cap(1) != "2" || cnmiRegExp.cap(2) == "0") cnmiFlag = true;
        //if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: status" << "cnmi" << cnmiRegExp.cap(1); //НАСТРОЙКИ CNMI
    } else if (data.contains(cregRegExp)) {
        //if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: status" << "reg" << cregRegExp.cap(2); //ИНФРМАЦИЯ О СОЕДИНЕНИИ
        regFlag = cregRegExp.cap(2).toInt();
        setGsmConStat(cregRegExp.cap(2));
    } else if (data.contains(csqRegExp)) {
        //if (DEBUG_MODE) qDebug() << "АНАЛИЗ ДАННЫХ GSM: status" << "reg" << csqRegExp.cap(1); //ИНФРМАЦИЯ О СОЕДИНЕНИИ
        setGsmSignLev(csqRegExp.cap(1));
    };
}

//Отправка данных из прта
void UI::sendGSMData() {
    if (reloadFlag) {
           Port.write("AT+CFUN=1,1\r\n");
           reloadFlag = false;
    } else if (terminalFlag) {
        if (currentPdu.count() > 1 && regFlag == 1) {
            //currentPdu.append("→\r\n");
            Port.write((currentPdu + QString("")).toUtf8());
           // Port.write("→");
            waitingSmsApproveFlag = true;
            smsApproveTimer = 0;
            terminalFlag = false;
            currentPdu = "";
        } else {
            //"could not send sms" errorCode; НУ НЕ МОГУ БЛЯДЬ ОТОСЛАТЬ СМС!!!
            Port.write("→\r\n");
            terminalFlag = false;
            currentPdu = "";
        };
    } else if (waitingTerminalFlag) {
        if (waitingTerminalTimer < 20) waitingTerminalTimer ++;
        else {
            waitingTerminalFlag = false;
            //"no terminal" errorCode НЕТ ТЕРМИНАЛА
            currentPdu = "";
        };
    } else if(waitingSmsApproveFlag) {
        if (smsApproveTimer < 600) {smsApproveTimer++;}
        else {
            smsApproveTimer = 0;
            if (DEBUG_MODE) qDebug() << "ОТПРАВКА В ПОРТ GSM: waiting " << smsApproveTimer;
            waitingSmsApproveFlag = false;
            //"no sending approve" errorCode НЕТ ПОДТВЕРЖДЕНИЯ ОТПРАВКИ
            currentPdu = "";
        };
    } else if (smsApproveFlag) {
        smsApproveFlag = false;
        //"sms sended" errorCode СМС ОТПРАВЛЕНА
    } else if (smsFlag) {
        smsFlag = false;
        if (toDelete > 0) {
            Port.write(QString("AT+CMGD=" + QString().setNum(toDelete) + "\r\n").toUtf8().data());
            toDelete = 0;
        };
        Port.write("AT+CNMA=0\r\n");
        if (DEBUG_MODE) qDebug() << "ОТПРАВКА В ПОРТ GSM: СООБЩЕНИЕ ПОЛУЧЕНО";
        //"sms recieved" errorCode СООБЩЕНИЕ ПОЛУЧЕНО
    } else if (currentPdu.count() > 1) {
        if (DEBUG_MODE) qDebug() << "ОТПРАВКА В ПОРТ GSM: СООБЩЕНИЕ ОТПРАВЛЯЕТСЯ";
        Port.write((QString("AT+CMGS=") + QString().setNum(currentPdu.count() / 2 - 1) + QString("\r")).toUtf8());
        waitingTerminalFlag = true;
        waitingTerminalTimer = 0;
    } else if (cnmiFlag) {
        Port.write("AT+CNMI=2,2,0,1,1\r\n");
        cnmiFlag = false;
    } else if (stepCounter % 20 == 0) {
        Port.write("AT+CREG?\r\n");
    } else if (stepCounter % 20 == 5) {
        Port.write("AT+CSQ\r\n");
    } else if(stepCounter % 20 == 10) {
        Port.write("AT+CMGL=4\r\n");
    } else if (stepCounter %20 == 15) {
        Port.write("AT+CNMI?\r\n");
    };

    stepCounter = (stepCounter + 1) % 20;
    portQueryTimer.start();
}

//Функция кодирования в смс
QString UI::encodePDU(QString number, QString data) {
    QString out;
    out = "002100";
    QString length;
    length.clear();
    length.setNum(number.length(),16);
    if (length.length() == 1) length.prepend("0");
    out += length.toUpper();
    if (number.length() % 2 == 1) number.append("F");

    length.clear();
    int i = 0;
    while (i <= number.length()) {
        length.append(number[i+1]);
        length.append(number[i]);
        i+=2;
    }

    out += "91";
    out += length;
    out += QString("0000");

    length.clear();
    length.setNum(data.length(),16);
    if (length.length() == 1) length.prepend("0");
    out += length.toUpper();

    i = 0;
    length.clear();
    QString symbol;
    symbol.setNum(data.at(0).unicode(),2);
    while (symbol.length() < 7) symbol.prepend("0");
    QString nextSymbol;
    QString numSymbol;
    QString rew;
    while (i < data.length()) {
        if(i < (data.length() - 1)) {
            nextSymbol.setNum(data.at(++i).unicode(),2);
            while (nextSymbol.length() < 7) nextSymbol.prepend("0");
            while (symbol.length() < 8) {
                symbol.prepend(nextSymbol.at(nextSymbol.length()-1));
                nextSymbol.chop(1);
            };
            numSymbol.setNum(symbol.toInt(0,2),16);
            while (numSymbol.length() < 2) numSymbol.prepend("0");

            length.append(numSymbol);

            symbol = nextSymbol;
            if (i % 8 == 7 && i < (data.length() - 1)) {
                symbol.setNum(data.at(++i).unicode(),2);
                while (symbol.length() < 7) symbol.prepend("0");

            };
        }
        else {
            if (i % 8 != 7) {
                while(symbol.length() < 8) symbol.prepend("0");
                numSymbol.setNum(symbol.toInt(0,2),16);
                while (numSymbol.length() < 2) numSymbol.prepend("0");
                length.append(numSymbol);
            };
            i++;
        };
    };
    out += length;
    return out.remove(QRegExp("\\0"));
}
