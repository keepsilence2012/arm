#ifndef DELDIAL_H
#define DELDIAL_H

#include <QDialog>

namespace Ui {
class delDial;
}

class delDial : public QDialog
{
    Q_OBJECT
    
public:
    explicit delDial(QWidget *parent = 0);
    ~delDial();
    Ui::delDial *ui;
    quint16 index;
signals:
    void ok(quint16);
    void no();
public slots:
    void okpr();
};

#endif // DELDIAL_H
