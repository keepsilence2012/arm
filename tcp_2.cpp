#include "tcp_2.h"

void TModbusTCPDevice::answer(int id,QByteArray req, QByteArray resp){
    if (resp.size()>7){
        if (resp[7]==4){
            if (resp.size()>8) {
                Status.resize((unsigned char) resp[8]);
                for (int temp=0;temp<Status.size();temp++)
                    Status[temp]=resp[temp+9];
                emit data(Status);
               // qDebug() << "STATUS RESPONSE: " << Status;
            }
        }
    }
}

TModbusTCPDevice::TModbusTCPDevice(QString inip, quint16 mda, int inport, QObject *par) : QObject(par) {
    ip=inip;
    port=inport;
    mdbAddr = mda;
    connection=new TModbusTCPConnection(inip, inport);
    connect(connection,SIGNAL(error(QString)),this,SIGNAL(error(QString)));
    connect(connection->process,SIGNAL(answer(int, QByteArray, QByteArray)),this,SLOT(answer(int, QByteArray, QByteArray)));
    status_timer.setInterval(ModbusTCP_Update_Interval);
    connect(&status_timer, SIGNAL(timeout()), this, SLOT(read()));
    status_timer.start();
}
TModbusTCPDevice::~TModbusTCPDevice() {
    disconnect();
    status_timer.stop();
    delete connection;
    qDebug() << "TCP DEVICE " << ip << "DELETED!";
}

void TModbusTCPDevice::read() {
    QByteArray req;
    req.resize(12);
    req[0]=0;
    req[1]=0;
    req[2]=0;
    req[3]=0;
    req[4]=0;
    req[5]=6;
    req[6]=mdbAddr;
    req[7]=4;
    req[8]=0;
    req[9]=0;
    req[10]=0;
    req[11]=ModbusStatus;
    this->connection->addQ(req);
    qDebug() << "Data tried to read" << ip << port;
}

void TModbusTCPDevice::ch_on() {
    QByteArray req;
    req.resize(12);
    req[0]=0;
    req[1]=0;
    req[2]=0;
    req[3]=0;
    req[4]=0;
    req[5]=6;
    req[6]=mdbAddr;
    req[7]=6;
    req[8]=0;
    req[9]=0;
    req[10]=0;
    req[11]=63;
    this->connection->addQ(req);
}

void TModbusTCPDevice::ch_off() {
    QByteArray req;
    req.resize(12);
    req[0]=0;
    req[1]=0;
    req[2]=0;
    req[3]=0;
    req[4]=0;
    req[5]=6;
    req[6]=mdbAddr;
    req[7]=6;
    req[8]=0;
    req[9]=0;
    req[10]=0;
    req[11]=0;
    this->connection->addQ(req);
}

TModbusTCPConnection::TModbusTCPConnection(QString inip, int inport, QObject *par) : QObject(par) {
    id=0;
    ip=inip;
    port=inport;
    process=new TModbusTCPProcess(inip, inport);
    connect(process,SIGNAL(error(QString)),this,SIGNAL(error(QString)));
}

TModbusTCPConnection::~TModbusTCPConnection() {
    process->disconnect();
    process->needToRun = false;
    while (process->isRunning()){};
    delete process;
    disconnect();
}

int TModbusTCPConnection::addQ(QByteArray req){
    int temp;
    temp=process->next(process->last);
    if ((!(process->dialog.at(temp)->buzy))&&(process->connected)){
        qDebug() << "last: " << process->last << " processing: " << process->processing;
        process->last=temp;
        process->dialog.at(process->last)->Request.resize(req.size());
        for (temp=0;temp<req.size();temp++){
            process->dialog.at(process->last)->Request[temp]=req[temp];
        }
        process->dialog.at(process->last)->id=id;
        process->dialog.at(process->last)->buzy=true;
        id++;
        return id-1;
    }
    else{
        emit error("Can't find free query place");
        return -1;
    }
}

TModbusTCPProcess::TModbusTCPProcess(QString inip, int inport, QObject * par) : QThread(par) {
    connected=false;
    int temp;
    needToRun = true;
    ip=inip;
    port=inport;
    last=-1;
    processing=0;
    for (temp=0;temp<MaxQuery;temp++){
        dialog.append(new TDialog);
    }
    tmout=OPEN_TIMEOUT;
    start();
}
TModbusTCPProcess::~TModbusTCPProcess() {
    disconnect();
    while (dialog.length()) {dialog.last()->disconnect();delete dialog.last();dialog.removeLast();}
}

int TModbusTCPProcess::next(int n) {
    int temp;
    if (n<(MaxQuery-1))
        temp=n+1;
    else
        temp=0;
    return temp;
}

bool TModbusTCPProcess::socket_open(QTcpSocket *Socket) {
    Socket->connectToHost(ip, port);
    if(Socket->waitForConnected(tmout)) {
#ifdef DEBUG
qDebug() << "Connected to" << ip << ":" << port;
#endif
        return 1;
    }
    else {
#ifdef DEBUG
qDebug() << "Error: Connection timeout to" << ip << ":" << port;
#endif
        emit error("Connection timeout");
        Socket->close();
        return 0;
    }
}

int TModbusTCPProcess::socket_send(QTcpSocket *Socket, QByteArray req, QByteArray *resp) {
    Socket->write(req);
#ifdef DEBUG
    qDebug() << "Writed to" << ip << ":" << port;
#endif
    if (Socket->waitForReadyRead(tmout)) {
#ifdef DEBUG
qDebug() << "READ DATA STARTED... for " << ip << ":" << port;
#endif
        *resp = Socket->readAll();
#ifdef DEBUG
qDebug() << "READ DATA FINISHED for " << ip << ":" << port;
#endif
        return 1;
    }
    else {
#ifdef DEBUG
qDebug() << "Error: Answer timeout.";
#endif
emit error("Answer timeout");
        return -1;
    }
}

void TModbusTCPProcess::run() {
    TCPSocket = new QTcpSocket(0);
    connect(TCPSocket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connected=socket_open(TCPSocket);
    while(needToRun){
        msleep(100);
        connected=(TCPSocket->state()==QAbstractSocket::ConnectedState);
        while(TCPSocket->state()!=QAbstractSocket::ConnectedState && needToRun){
            socket_open(TCPSocket);
        }
        if (dialog.at(processing)->buzy){
            if (!(socket_send(TCPSocket,dialog.at(processing)->Request,&dialog.at(processing)->Response))){
                emit error("Send error");
            }
            emit answer(dialog.at(processing)->id,dialog.at(processing)->Request,dialog.at(processing)->Response);
            dialog.at(processing)->buzy=false;
            processing=next(processing);
        }
    }
    TCPSocket->disconnectFromHost();
    exit(0);
}

void TModbusTCPProcess::disconnected(){
    emit error("Disconnected");
    qDebug() << "disconnected";
}

TDialog::TDialog(QObject * par){
    buzy=false;
}
