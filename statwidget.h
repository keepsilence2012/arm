#ifndef STATWIDGET_H
#define STATWIDGET_H

#include <QWidget>
#include <QMainWindow>
#include <QDate>
#include <QDialog>

namespace Ui {
class statWidget;
}

class statWidget : public QDialog
{
    Q_OBJECT

public:
    explicit statWidget(QWidget *parent = 0);
    ~statWidget();
    void setData(QDate startDate, QDate stopDate, double maxVal,QDate maxDate, double minVal, QDate minDate, double averVal, double lastVal, QDate lastDate, int startHour, int stopHour);
    int hour;
    QDate minDate,maxDate;

signals:
    void showMinDate(int,QDate);
    void showMaxDate(int,QDate);
public slots:
    void sh_min_pr();
    void sh_max_pr();

private:
    Ui::statWidget *ui;
};

#endif // STATWIDGET_H
