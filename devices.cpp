#include "ui.h"

//функция загрузки списка устройств в массив
void UI::loadDevices() {
    gsmHere = false, tcpHere = false, mdbHere = false;
    int i;
    //Заполняем массив индексами и номерами
    _getDeviceIndexList();
    timeFromLoading = QDateTime::currentMSecsSinceEpoch();

    //Загружаем очередь сообщений
    foreach(int i,deviceArray.keys()) {
        if (deviceArray[i].type == "gsm") gsmHere = true;
        else if (deviceArray[i].type == "tcp") tcpHere = true;
        else if (deviceArray[i].type == "modbus") mdbHere = true;
        _readDeviceMistakes(i); //Читаем ошибки
        _loadDeviceStat(i); //Вычитываем статистику приборов
        _addDevice(i);
    }
    //Сортируем список приборов
    readConStat();
    showConIcons();
    _sortDeviceList();
    _sortBuseList();
    trayMessage("Загрузка НС","Данные НС загружены",2);
}

//Добавить устройство
void UI::_addDevice(quint16 i) {
    deviceArray[i].outCurrentDate = QDateTime();
    deviceArray[i].outCurrentNum = 0;
    deviceArray[i].outCurrentPDU = "";
    deviceArray[i].outCurrentRepeat = 0;
    deviceArray[i].outCurrentStat = 0;
    if (maxIndex < i) maxIndex = i;
    if (minIndex > i) minIndex = i;

    deviceForms.insert(i, new deviceform(deviceList)); //Добавляем новую форму
    deviceForms[i]->show();
    _setDeviceFormData(i); //Вносим в форму считанные данные
    //Соединяем сигналы формы с функциями
    connect(deviceForms[i],SIGNAL(hovered(quint16)),this,SLOT(hovered(quint16)));
    connect(deviceForms[i],SIGNAL(left(quint16)),this,SLOT(left(quint16)));
    connect(deviceForms[i],SIGNAL(dblclick(quint16)),this,SLOT(dblckick(quint16)));
    connect(deviceForms[i],SIGNAL(moved(quint16)),this,SLOT(deviceFormMovedToMap(quint16)));
    connect(deviceForms[i],SIGNAL(released(quint16)),this,SLOT(deviceFormReleasedToMap(quint16)));
     //Добавляем указатель на карту
    devicePointers.insert(i, new devicePointer(this));
    _setPointerStatus(i,deviceArray[i].stat.stat);
    _movePointerTo(i,deviceArray[i].x,deviceArray[i].y);

    //Создаем окно приборов
    _createDeviceWindow(i);
    //Выставляем его состояние
    _setDeviceWindowStatus(i,deviceArray[i].stat);

    //делаем бусы
    deviceBuses.insert(i,new deviceBuse(deviceList2));
    deviceBuses[i]->show();
    _setBuseStat(i);
    connect(deviceBuses[i],SIGNAL(dbl(quint16)),this,SLOT(dblckick(quint16)));
    connect(deviceBuses[i],SIGNAL(hovered(quint16)),this,SLOT(hovered(quint16)));
    connect(deviceBuses[i],SIGNAL(left(quint16)),this,SLOT(left(quint16)));
    connect(deviceBuses[i],SIGNAL(moved(quint16)),this,SLOT(deviceBuseMovedToMap(quint16)));
    connect(deviceBuses[i],SIGNAL(released(quint16)),this,SLOT(deviceBuseReleasedToMap(quint16)));

    //Соединяем сигналы указателя с функциями

    connect(devicePointers[i],SIGNAL(hovered(quint16)),this,SLOT(hovered(quint16)));
    connect(devicePointers[i],SIGNAL(left(quint16)),this,SLOT(left(quint16)));
    connect(devicePointers[i],SIGNAL(dblclick(quint16)),this,SLOT(dblckick(quint16)));
    connect(devicePointers[i],SIGNAL(moved(quint16)),this,SLOT(movePointer(quint16)));

    if (deviceArray[i].type == "tcp") {
        //tcpPort.add(deviceArray[i].address,deviceArray[i].mdbAddr);
        addTcpDev(i,deviceArray[i].address,deviceArray[i].mdbAddr,deviceArray[i].tcpPort);
    };

    if (deviceArray[i].type == "modbus") {
        //trayMessage("(" + QString::number(i) + ") " + deviceArray[i].name,"Модуль MODBUS RTU отсутствует. Все НС, подключенные к системе по протоколу MODBUS RTU, не будут доступны.",10);
        if (DEBUG_MODE) qDebug() << deviceArray[i].address;
        mdbPort.addDevice(deviceArray[i].address,i,deviceArray[i].mdbAddr,deviceArray[i].mdbPage);
    };
}

//Добавить статус
void UI::addStatus(quint16 index, status stat) {
    int i;
    //Если такой номер есть
    if (deviceArray.contains(index)) {
        //Если прибор GSM
        if (deviceArray[index].type == "gsm") {
            //Если сообщение по дате больше или равно статуса
            if (deviceArray[index].stat.date < stat.date)
                deviceArray[index].stat = stat;  //Добавляем в статус
                //Сохраняем в архив
            //_saveDeviceStat(deviceArray[index].address,stat.fullText);
        } else if (deviceArray[index].type == "tcp") {
            //Если прибор TCP\IP
            //Если дата больше текущей
            if (deviceArray[index].stat.date < stat.date)
            {
                //Если состояние не поменялось и не менялись коды ошибкок и с момента последнего отчета прошло не более часа
                if (stat.stat == deviceArray[index].stat.stat &&
                        stat.errors == deviceArray[index].stat.errors &&
                        ((QDateTime::currentMSecsSinceEpoch() - deviceArray[index].stat.date.toMSecsSinceEpoch()) < 3600000)
                    ) {
                //Меняем цифры
                    deviceArray[index].stat.an = stat.an;
                    for (i=0;i<6;i++) deviceArray[index].stat.rdy[i] = stat.rdy[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.run[i] = stat.run[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.err[i] = stat.err[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.in[i] = stat.in[i];
                    deviceArray[index].stat.sbm = stat.sbm;
                } else {
                //Если состояние изменилось или оды ошибок изменились или прошел час
                    //Перезаписываем его
                    deviceArray[index].stat = stat;
                    //_saveDeviceStat(index,stat.fullText);
                };
            };
        } else if (deviceArray[index].type == "modbus") {
            //Если прибор MODBUS
            //Если дата больше текущей
            if (deviceArray[index].stat.date < stat.date)
            {
                //Если состояние не поменялось и не менялись коды ошибкок и с момента последнего отчета прошло не более часа
                if (stat.stat == deviceArray[index].stat.stat &&
                        stat.errors == deviceArray[index].stat.errors &&
                        ((QDateTime::currentMSecsSinceEpoch() - deviceArray[index].stat.date.toMSecsSinceEpoch()) < 3600000)
                    ) {
                //Меняем цифры
                    deviceArray[index].stat.an = stat.an;
                    for (i=0;i<6;i++) deviceArray[index].stat.rdy[i] = stat.rdy[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.run[i] = stat.run[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.err[i] = stat.err[i];
                    for (i=0;i<6;i++) deviceArray[index].stat.in[i] = stat.in[i];
                    deviceArray[index].stat.sbm = stat.sbm;
                } else {
                //Если состояние изменилось или оды ошибок изменились или прошел час
                    //Перезаписываем его
                    deviceArray[index].stat = stat;
                    //_saveDeviceStat(index,stat.fullText);
                };

            } else {
            //Если какая-то непонятная фигня
            };
        };
    };
}
//Сортировка списка бус
void UI::_sortBuseList() {
    quint16 count = 0;
    foreach(quint16 i,deviceBuses.keys()) if (!deviceBuses[i]->moving) {
        deviceBuses[i]->setGeometry(34 * (count % 6), 34 * (count / 6), 30, 30);
        count++;
    };
}
//Утановка статуса бус
void UI::_setBuseStat(quint16 index) {
    if (deviceArray.contains(index) && deviceBuses.contains(index)) {
        //if (DEBUG_MODE) qDebug() << "СТАТУС БУС: УСТАНАВЛИВАЕТСЯ СТАТУС БУСЫ " << index;
        deviceBuses[index]->index = index;
        deviceBuses[index]->text->setText(QString::number(index));
        deviceBuses[index]->text->setToolTip(deviceArray[index].name);
        if (deviceArray[index].stat.stat == "ready") {
            deviceBuses[index]->image->setPixmap(QPixmap(":/images/buseReady2.png"));
        }  else if (deviceArray[index].stat.stat == "handMode") {
            deviceBuses[index]->image->setPixmap(QPixmap(":/images/buseHandMode2.png"));
        }  else if (deviceArray[index].stat.stat == "off") {
            deviceBuses[index]->image->setPixmap(QPixmap(":/images/buseOff2.png"));
        }  else if (deviceArray[index].stat.stat == "alarm") {
            deviceBuses[index]->image->setPixmap(QPixmap(":/images/buseAlarm2.png"));
        }  else  {
            deviceBuses[index]->image->setPixmap(QPixmap(":/images/buseTimeout.png"));
        };
    }
}

//Сортировка списка НС
void UI::_sortDeviceList() {
    if (sortByStatus) {
        quint16 i = 0;
        foreach(quint16 index, deviceForms.keys()) qDebug() << "ИНДЕКС: " << index << ", СОСТОЯНИЕ: " <<deviceArray[index].stat.stat;
        //Сначала выстраиваем те, у которых алармы
        foreach(quint16 index, deviceForms.keys()) if (deviceArray[index].stat.stat == "alarm" && !deviceForms[index]->moving) {deviceForms[index]->setGeometry(0,50 * i,200,50); i++;};
        if (DEBUG_MODE) qDebug() << "СОРТИРОВКА СПИСКА: АВАРИИ: " << i;
        //Потом выставляем другие
        foreach(quint16 index, deviceForms.keys()) if ((deviceArray[index].stat.stat == "ready" ||
                                                   deviceArray[index].stat.stat == "off" ||
                                                   deviceArray[index].stat.stat == "handMode") && !deviceForms[index]->moving)
        {deviceForms[index]->setGeometry(0,50 * i,200,50); i++;};
        if (DEBUG_MODE) qDebug() << "СОРТИРОВКА СПИСКА: В РАБОТЕ: " << i;
        foreach(quint16 index, deviceForms.keys()) if (deviceArray[index].stat.stat == "timeout" && !deviceForms[index]->moving) {deviceForms[index]->setGeometry(0,50 * i,200,50); i++;};
        if (DEBUG_MODE) qDebug() << "СОРТИРОВКА СПИСКА: НЕТ ОТВЕТА: "<< i;
        deviceList->resize(200,50 * (deviceForms.count() + 1));
    } else {
        quint16 i = 0;
        foreach(quint16 index, deviceForms.keys()) if (!deviceForms[index]->moving) {deviceForms[index]->setGeometry(0,50 * i,200,50); i++;};
        deviceList->resize(200,50 * (deviceForms.count() + 1));
    };
}

//Устанавливаем статус указателя
void UI::_setPointerStatus(quint16 index, QString stat) {
    QString pix;
    if (DEBUG_MODE) qDebug() << index << ": " << stat;
    if (stat == "ready") pix = ":/images/pointer4.png";
    else if (stat == "off") pix = ":/images/offPoint4.png";
    else if (stat == "handMode") pix = ":/images/handPoint4.png";
    else if (stat == "timeout") pix = ":/images/offPoint4.png";
    else if (stat == "alarm") pix = ":/images/errPoint4.png";
    //выбираем картинку для уазателя
    //Если такой указатель есть
    if (devicePointers.contains(index)) {
    //Устанавливаем его картинку
        devicePointers[index]->pointer->setPixmap(QPixmap(pix).scaled(devicePointers[index]->pointer->geometry().width(),devicePointers[index]->pointer->geometry().height()));
    //Устанавливаем его номер
        devicePointers[index]->index->setText(QString::number(index));
    };
}

//Передвигаем указатель
void UI::_movePointerTo(quint16 index, double x,double y) {
    //Если такой уазатель есть в природе
    if (devicePointers.contains(index)) {
      //Двигаем его на карту
        devicePointers[index]->setGeometry(x * currentMapImage->width() + currentMapImage->pos().x(),
                                           y * currentMapImage->height() + currentMapImage->pos().y(),
                                           devicePointers[index]->width(),
                                           devicePointers[index]->height());
    };

    if (DEBUG_MODE) qDebug() << devicePointers[index]->geometry();
}

//Задаем оординаты указателя
void UI::_setPointerCoords(quint16 index, quint16 x, quint16 y) {

    //Если координаты указателя вписываются в карту, двигаем его
    if ( x > currentMapImage->x() && x < (currentMapImage->x() + currentMapImage->width() - 60) &&
        y > (currentMapImage->y() + 21) && y < (currentMapImage->y() + currentMapImage->height() - 60 + 21)) {
        //Меняем его координаты и карту
        deviceArray[index].x = (double)(x - currentMapImage->x()) / currentMapImage->width();
        deviceArray[index].y = (double)(y - currentMapImage->y()) / currentMapImage->height();
        //перемещаем указатель
        _movePointerTo(index,deviceArray[index].x,deviceArray[index].y);
    } else if ( (x <= currentMapImage->x() + 5) &&
                y > (currentMapImage->y() + 21) && y < (currentMapImage->y() + currentMapImage->height() - 60 + 21)) {
        //Спрашиваем об удалении
        deviceArray[index].x = (double)(x - currentMapImage->x() + 20) / currentMapImage->width();
        deviceArray[index].y = (double)(y - currentMapImage->y()) / currentMapImage->height();
        //перемещаем указатель
        _movePointerTo(index,deviceArray[index].x,deviceArray[index].y);
        deleteDialog = new delDial(this);
        deleteDialog->index = index;
        deleteDialog->show();
        connect(deleteDialog,SIGNAL(ok(quint16)),this,SLOT(deleteDeviceFromMap(quint16)));
        connect(deleteDialog,SIGNAL(no()),this,SLOT(closeDelDial()));
    };
}

//Сделать указатель больше
void UI::_makePointerBigger(quint16 index) {
    //Если указатель есть
    if ( devicePointers.contains(index)) {
        //Увеличиваем его и чуть-чуть передвигаем
        devicePointers[index]->setGeometry(devicePointers[index]->pos().x(),devicePointers[index]->pos().y() - 20, 60, 60);
        //Увеличиваем картинку
        devicePointers[index]->pointer->setGeometry(0,0,60,60);
        //devicePointers[index]->pointer->setPixmap(QPixmap(":/images/pointer3.png").scaled(60,60));
        //Сдвигаем текст
        devicePointers[index]->index->move(15,10);

    };
}

//Сделать указатель меньше
void UI::_makePointerSmaller(quint16 index) {
    //Если указатель есть
    if ( devicePointers.contains(index)) {
        //Увеличиваем его и чуть-чуть передвигаем
        devicePointers[index]->setGeometry(devicePointers[index]->pos().x(),devicePointers[index]->pos().y() + 20, 40, 40);
        //Увеличиваем картинку
        devicePointers[index]->pointer->setGeometry(0,0,40,40);
        //devicePointers[index]->pointer->setPixmap(QPixmap(":/images/pointer3.png").scaled(40,40));
        //Сдвигаем текст
        devicePointers[index]->index->move(5,5);

    };
}

//API-чтение параметров из файла устройства
void UI::_readDeviceParameters(int deviceIndex, QString address) {
    //Файл с параметрами прибора
    //OLOLOLO
    QFile file(MAIN_PATH + "devices/" + QString::number(deviceIndex) + "/current.config");
    //Два массива строк для работы
    QStringList sl,cl;

    deviceStruct dev;
    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ПАРАМЕТРОВ НС: ПРОЧИТАНО!";
        //ЕСЛИ ФАЙЛ ОТКРЫЛСЯ, ВЫГРУЖАЕМ ИЗ НЕГО ВСЮ ИНФОРМАЦИЮ
        sl = QString(file.readAll()).split("\r\n");
        foreach (QString c, sl) {
            cl = c.split("=");
            //ЕСЛИ ЕСТЬ НУЖНЫЕ НАМ ПАРАМЕТРЫ, ПЕРЕБИРАЕМ ИХ
            if (cl.count() == 2) {
                if (cl[0].contains("name")) dev.name = cl[1]; //ИМЯ
                else if (cl[0].contains("address")) dev.address = cl[1];
                else if (cl[0].contains("type")) dev.type = cl[1]; //ТИП
                else if (cl[0].contains("map")) dev.map = (cl[1].toInt() > 0 && cl[1].toInt() <= mapCount) ? cl[1].toInt() : 0; //КАРТА
                else if (cl[0].contains("cordX")) dev.x = (cl[1].toFloat() > 0 && cl[1].toFloat() < 1) ? cl[1].toFloat() : ((double)(qrand() % 100) / 100); //Х
                else if (cl[0].contains("cordY")) dev.y = (cl[1].toFloat() > 0 && cl[1].toFloat() < 1) ? cl[1].toFloat() : ((double)(qrand() % 100) / 100); //У
                else if (cl[0].contains("t_per")) dev.period = (cl[1].toUInt() > 0) ? cl[1].toUInt() : 1440; //ПЕРИОD ОПРОСА
                else if (cl[0].contains("alx")) dev.alx = cl[1]; //ALX
                else if (cl[0].contains("aly")) dev.aly = cl[1]; //ALY
                else if (cl[0].contains("alw")) dev.alw = cl[1]; //ALW
                else if (cl[0].contains("pumps")) dev.pumps = (cl[1].toInt() > 0 && cl[1].toUInt() < 7) ? cl[1].toUInt() : 6; //НАСОСЫ
                else if (cl[0].contains("start")) dev.start = (cl[1] == "w") ? "w" : "d";
                else if (cl[0].contains("mdbAddr")) dev.mdbAddr = (cl[1].toInt() > 0) ? cl[1].toInt() : 85;
                else if (cl[0].contains("mdbPage")) dev.mdbPage = 0;//dev.mdbPage = (cl[1].toInt() > 0) ? cl[1].toInt() : 0;
                else if (cl[0].contains("view")) dev.view = (cl[1].toInt() == 1) ? 1 : 0;
            };
        };
        file.close();
        //ДОБАВЛЯЕМ ДАННЫЕ В МАССИВ
        //dev.address = address;
        dev.index = deviceIndex;
        dev.timeoutTimer = dev.period;
        deviceArray[deviceIndex] = dev;
    } else {
        //ЕСЛИ ФАЙЛ НЕ ОТКРЫВАЕТСЯ
        if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ПАРАМЕТРОВ: НЕТ ФАЙЛА КОНФИГУРАЦИИ ДЛЯ ПРИБОРА С НОМЕРОМ " << address << ".\nУстройство не будет добавлено!";
        deviceArray.remove(deviceIndex);
    };
}

//API-чтение кодов ошибок из устройства
void UI::_readDeviceMistakes(quint16 deviceIndex) {
    QRegExp commonReg("^E([0-9]{2,2})$");
    QRegExp pumpReg("^Ex([0-9])$");
    int i; QString st; //Временные переменные
    //Если такое устройство есть
    if (deviceArray.contains(deviceIndex)) {
    //Забьем массив "рыбой"
        for (i=0;i<100;i++) deviceArray[deviceIndex].errors.insert(i,QString("Код ошибки ") + QString::number(i));
    //Файл с ошибками
        //OLOLOLO
        QFile file(MAIN_PATH + "devices/" + QString::number(deviceIndex) + "/errors.csv");
        if (file.exists() && file.open(QIODevice::ReadOnly)) {
            //Если файл есть и он открылся, считываем все его данные
            QStringList fullText = QString(file.readAll()).split("\r\n");
            foreach(QString s,fullText) {

                //Для каждой строки
                if (s.split(";").count() > 2) {
                    QStringList sl = s.split(";");
                    if (DEBUG_MODE) qDebug() << sl;
                    //Если строка соответствует регулярке, поехали
                    if (sl[0].contains(commonReg)) {
                        //Если указана общая ошибка
                        st.clear(); for (i=2;i<sl.count();i++) st.append(sl[i]); //Формируем строку с текстом ошибки
                        deviceArray[deviceIndex].errors.insert(commonReg.cap(1).toUInt(),st); //Добавляем ее в базу
                    } else if (sl[0].contains(pumpReg)) {
                        //Если это ошибка насоса
                        st.clear(); for (i=2;i<sl.count();i++) st.append(sl[i]); //Формируем строку с текстом ошибки
                        for (i=1;i<=6;i++) deviceArray[deviceIndex].errors.insert(i*10 + pumpReg.cap(1).toUInt(),QString(st).replace("%X",QString::number(i))); //Добавляем ее в базу
                    } else {
                        if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ОШИБОК: СТРОКА НЕ СОДЕРЖИТ ДАННЫХ ПО НАСОСУ";
                    };
                };
            }
            //Для остальных - добавим код "НЕИЗВЕСТНАЯ ОШИБКА"
            if (DEBUG_MODE) qDebug() << deviceArray[deviceIndex].errors;
        } else if (!file.exists()){
            if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ОШИБОК: НЕВОЗМОЖНО ЗАГРУЗИТЬ ОШИБКИ ДЛЯ ПРИБОРА " << deviceArray[deviceIndex].address;
            file.setFileName(MAIN_PATH + "config/errors.csv");
            if (file.open(QIODevice::ReadOnly)) {
                QStringList fullText = QString(file.readAll()).split("\r\n");
                foreach(QString s,fullText) {
                     //Для каждой строки
                    if (s.split(";").count() > 2) {
                        QStringList sl = s.split(";");
                        if (DEBUG_MODE) qDebug() << sl;
                        //Если строка соответствует регулярке, поехали
                        if (sl[0].contains(commonReg)) {
                            //Если указана общая ошибка
                            st.clear(); for (i=2;i<sl.count();i++) st.append(sl[i]); //Формируем строку с текстом ошибки
                            deviceArray[deviceIndex].errors.insert(commonReg.cap(1).toUInt(),st); //Добавляем ее в базу
                        } else if (sl[0].contains(pumpReg)) {
                            //Если это ошибка насоса
                            st.clear(); for (i=2;i<sl.count();i++) st.append(sl[i]); //Формируем строку с текстом ошибки
                            for (i=1;i<=6;i++) deviceArray[deviceIndex].errors.insert(i*10 + pumpReg.cap(1).toUInt(),QString(st).replace("%X",QString::number(i))); //Добавляем ее в базу
                        } else {
                            if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА ОШИБОК: СТРОКА НЕ СОДЕРЖИТ ДАННЫХ ПО НАСОСУ";
                        };
                    };
                };
            };
            //file.copy();
        };
        file.close();
    };
}

//Выставление значений в пункт списка НС
void UI::_setDeviceFormData(quint16 index) {
    //Если индекс есть в параметрах
    if (deviceArray.contains(index) && deviceForms.contains(index)) {
        //Выставляем имя
        deviceForms[index]->name->setText(deviceArray[index].name);
        //Выставляем номер карты
        if (deviceArray[index].map > 0) deviceForms[index]->map->setText("Карта " + QString::number(deviceArray[index].map));
        else deviceForms[index]->map->setText("-");
        //выставляем индекс
        deviceForms[index]->index->setText(QString::number(index));
        //выставляем статус
        if (deviceArray[index].stat.stat == "ready") deviceForms[index]->status->setPixmap(QPixmap(":/images/on.png").scaled(20,20));
        else if (deviceArray[index].stat.stat == "handMode") deviceForms[index]->status->setPixmap(QPixmap(":/images/handMode.png").scaled(20,20));
        else if (deviceArray[index].stat.stat == "off") deviceForms[index]->status->setPixmap(QPixmap(":/images/off.png").scaled(20,20));
        else if (deviceArray[index].stat.stat == "timeout") deviceForms[index]->status->setPixmap(QPixmap(":/images/errSmallQ.png").scaled(20,20));
        else if (deviceArray[index].stat.stat == "alarm") deviceForms[index]->status->setPixmap(QPixmap(":/images/errSmallQ.png").scaled(20,20));

        //выставляем указатель
        if (deviceArray[index].stat.stat == "ready") deviceForms[index]->pointer->setPixmap(QPixmap(":/images/pointer4.png").scaled(50,40));
        else if (deviceArray[index].stat.stat == "handMode") deviceForms[index]->pointer->setPixmap(QPixmap(":/images/handPoint4.png").scaled(50,40));
        else if (deviceArray[index].stat.stat == "off") deviceForms[index]->pointer->setPixmap(QPixmap(":/images/offPoint4.png").scaled(50,40));
        else if (deviceArray[index].stat.stat == "timeout") deviceForms[index]->pointer->setPixmap(QPixmap(":/images/offPoint4.png").scaled(50,40));
        else if (deviceArray[index].stat.stat == "alarm") deviceForms[index]->pointer->setPixmap(QPixmap(":/images/errPoint4.png").scaled(50,40));

        //выставляем дату
        quint32 hours = ((QDateTime::currentDateTime().toMSecsSinceEpoch() - deviceArray[index].stat.date.toMSecsSinceEpoch()) / 3600000);
        quint32 minutes = ((QDateTime::currentDateTime().toMSecsSinceEpoch() - deviceArray[index].stat.date.toMSecsSinceEpoch()) % 3600000) / 60000;
        QString Hours = QString::number(hours); QString Minutes = QString::number(minutes);
        while (Hours.count() < 2) Hours.prepend("0"); while (Minutes.count() < 2) Minutes.prepend("0");

        if (deviceArray[index].type == "gsm") {
            if (hours < 100) deviceForms[index]->date->setText(Hours + "ч" + Minutes + "мин.наз.");
            else deviceForms[index]->date->setText("--:--");
        } else if (deviceArray[index].type == "tcp") {
            deviceForms[index]->date->setText("TCP\\IP");
        } else if (deviceArray[index].type == "modbus") {
            deviceForms[index]->date->setText("MODBUS");
        };

    }
}

//функция для считывания списка занесенных в систему устройств
//void UI::_getDeviceIndexList() {
//    QFile devFile(MAIN_PATH + "config/" + DEVICES_FILE);
//    if (!devFile.open(QIODevice::ReadOnly)) {
//        //ЕСЛИ ФАЙЛ ПО КАКИМ-ТО ПРИЧИНАМ НЕ ОТКРЫЛСЯ
//    } else {
//        QStringList s_l = QString(devFile.readAll()).split("\r\n"); //СЧИТЫВАЕМ ВЕСЬ ФАЙЛ И ДЕЛИМ ЕГО ПОСТРОЧНО
//        foreach (QString sl, s_l) {
//            if (sl.contains(QRegExp("^[0-9]{1,3}$"))) { //Проверяем, есть ли в этой строке информация о приборе
//                quint16 index = sl.toInt();
//                //Создаем временную структуру и добавляем в нее только индекс и номер
//                deviceStruct device;
//                device.index = index;
//                //Добавляем временную структуру в массив, если ни индекса, ни адреса такого нет
//                if (!(deviceArray.keys().contains(index))) {
//                    //Если значений нигде не было
//                    deviceArray.insert(index,device);
//                }
//                else {
//                    //Если значения повторяются
//                };

//                //Если включен режим отладки, выводим сообщение о добавлении
//                if (DEBUG_MODE) qDebug() << "ЧТЕНИЕ СПИСКА УСТРОЙСТВ: Устройство под номером " << index << " добавлено";
//            };
//        }
//    };
//    devFile.close();
//    return;
//}

//функция для считывания списка занесенных в систему устройств
void UI::_getDeviceIndexList() {
    QSqlQuery query("",QSqlDatabase::database("con1"));
    QRegExp portRegExp(":([0-9]{1,5})$");
    if (query.exec("SELECT did,address,name,type,map,cordx,cordy,t_per,alx,aly,alw,pumps,start,mdbaddr,mdbpage,comment,view,flow FROM devices")) {
        while (query.next()) {
            quint8 index = query.value(0).toUInt();
            deviceStruct dev; dev.index = index;
            if (query.value(1).toString().contains(portRegExp)) {
                dev.tcpPort = portRegExp.cap(1).toUInt();
                qDebug() << "CUSTOM PORT: " << dev.tcpPort;
                dev.address = query.value(1).toString().remove(portRegExp);
            } else {
                dev.tcpPort = 502;
                dev.address = query.value(1).toString();
            }
            dev.name = query.value(2).toString();
            dev.type = query.value(3).toString();
            dev.map = query.value(4).toUInt();
            dev.x = query.value(5).toDouble();
            dev.y = query.value(6).toDouble();
            dev.period = query.value(7).toInt();
            dev.alx = query.value(8).toString();
            dev.aly = query.value(9).toString();
            dev.alw = query.value(10).toString();
            dev.pumps = query.value(11).toUInt();
            dev.start = query.value(12).toString();
            dev.mdbAddr = query.value(13).toUInt();
            dev.mdbPage = query.value(14).toUInt();
            dev.view = query.value(16).toUInt();
            dev.flow = query.value(17).toDouble();
            dev.timeoutTimer = dev.period;
            dev.working = 0;
            qDebug() << "СЧИТАННОЕ ИЗ БАЗЫ УСТРЙСТВО: " << dev.index << dev.address << dev.name << dev.type << dev.map << dev.x << dev.y << dev.period << dev.alx << dev.aly << dev.alw << dev.pumps << dev.start << dev.mdbAddr << dev.mdbPage << dev.view << dev.flow;
            if (!deviceArray.keys().contains(index)) deviceArray.insert(index,dev);
        };
    } else {
        trayMessage("АРМ SK-712","Не удалось загрузить список устройств: ",3);
    }
    return;
}

//Обработка входящих смс с необходимыми действиями
void UI::renderIncomingSms(smsData data) {
    //Парсим его

    //ищем номер устройства с таким номером
    int i,j; bool correct = false;
    foreach (j,deviceArray.keys()) if (deviceArray[j].address == data.address) { i = j; correct = true; break;};

    //если он есть среди НС
    if (correct) {
        //Отключаем мигание
        deviceArray[i].packsAll++;
        if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: С НОМЕРА " << data.address  << " ПРИШЛО СООБЩЕНИЕ " << data.text;
    //Если он корректен
        status deviceStat = StringToStat(QString(data.text).prepend(" ").prepend(QDateTime(data.date).toString("yyyy/MM/dd hh:mm:ss")));
        if (deviceStat.correct) {
            _stopWaiting(i);
            //deviceWindows[i]->onButton->setDisabled(false);
            //deviceWindows[i]->offButton->setDisabled(false);
            //deviceWindows[i]->resetButton->setDisabled(false);
            //deviceWindows[i]->waiting = false;

            //if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС КОРРЕКТЕН";
            if (deviceStat.daily) saveGsmWorking(deviceStat,i);
            deviceArray[i].stat = deviceStat;
            _setPointerStatus(i,deviceStat.stat);
            _setDeviceWindowStatus(i,deviceStat);
            _setDeviceFormData(i);
            deviceForms[i]->warning = (deviceArray[i].stat.stat == "alarm");
            _setBuseStat(i);
            _saveDeviceStat(i,data.text.prepend(" ").prepend(data.date.toString("yyyy/MM/dd hh:mm:ss")));

            if (deviceArray[i].outCurrentStat != 0) {
                deviceArray[i].outCurrentStat = 0;
                deviceArray[i].outCurrentDate = QDateTime();
                deviceArray[i].outCurrentNum = 0;
                deviceArray[i].outCurrentRepeat = 0;
                deviceArray[i].outCurrentPDU = "";
            };
            _sortDeviceList();
            if (statArray.contains(deviceArray[i].stat.stat)) trayMessage(deviceArray[i].name,statArray[deviceArray[i].stat.stat]);
            //Проигрываем зву сообщения
            if (deviceStat.fullText.contains("*ALARM*")) {
                //PlaySoundA(QString(MAIN_PATH + "sound/alarm.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                QSound::play(alarmSound);
            } else {
               // PlaySoundA(QString(MAIN_PATH + "sound/message.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                QSound::play(messageSound);
            };
        } else {
            if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС НЕВЕРНЫЙ!!!";
        };
    //Если время
    } else {
        if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: НЕТ УСТРОЙСТВА С НОМЕРОМ " << data.address;
    };
}

//Создание окна приборов
void UI::_createDeviceWindow(quint16 index) {
    if (deviceArray.contains(index)) {
        //Если есть устройство с таким номером в списке
        deviceWindows[index] = new deviceWindow(0); //Создаем окно приборов
        deviceWindows[index]->SetWindowTitle(deviceArray[index].name); //Именуем его
        deviceWindows[index]->index = index;
        deviceWindows[index]->flow = deviceArray[index].flow;
        if (deviceArray[index].type == "gsm") {
            deviceWindows[index]->alxLab->setToolTip(deviceArray[index].alx);
            deviceWindows[index]->alxLab_2->setToolTip(deviceArray[index].alx);
            deviceWindows[index]->alyLab->setToolTip(deviceArray[index].aly);
            deviceWindows[index]->alyLab_2->setToolTip(deviceArray[index].aly);
            deviceWindows[index]->alwLab->setToolTip(deviceArray[index].alw);
            deviceWindows[index]->alwLab_2->setToolTip(deviceArray[index].alw);
        }
        deviceWindows[index]->setType(deviceArray[index].type);
        connect(deviceWindows[index],SIGNAL(Action(quint16,QString)),this,SLOT(addQueryFromDevice(quint16,QString)));
        connect(deviceWindows[index],SIGNAL(History(quint16)),this,SLOT(turnHistory(quint16)));
        connect(deviceWindows[index],SIGNAL(ShowHistory(quint16,qint16)),this,SLOT(showFullHistory(quint16,qint16)));
        connect(deviceWindows[index],SIGNAL(ShowHistoryStat(quint16)),this,SLOT(showHistoryStat(quint16)));
        connect(deviceWindows[index],SIGNAL(info(quint16)),this,SLOT(showWindowInfo(quint16)));
        connect(deviceWindows[index],SIGNAL(dailyQuery(quint16,QDate,QDate,QDate)),this,SLOT(getDaily(quint16,QDate,QDate,QDate)));
        connect(deviceWindows[index],SIGNAL(monthQuery(quint16,int,int)),this,SLOT(getMonth(quint16,int,int)));
        connect(deviceWindows[index],SIGNAL(yearQuery(quint16,int)),this,SLOT(getYear(quint16,int)));
        connect(deviceWindows[index],SIGNAL(moved(quint16)),this,SLOT(deviceWindowMoved()));
        connect(deviceWindows[index],SIGNAL(saveQuery(quint16,QDate,QDate)),this,SLOT(saveDaily(quint16,QDate,QDate)));
        connect(deviceWindows[index],SIGNAL(m_saveQuery(quint16,int,int)),this,SLOT(m_save(quint16,int,int)));
        connect(deviceWindows[index],SIGNAL(y_saveQuery(quint16,int)),this,SLOT(y_save(quint16,int)));
        emit deviceWindows[index]->ShowHistory(index,15);
        deviceWindows[index]->setGraphics(deviceArray[index].view);
        if (DEBUG_MODE) qDebug() << "СОЗДАНИЕ ОКНА НС: ОКНО ДЛЯ ПРИБОРА С НОМЕРОМ " << deviceArray[index].address << " СОЗДАНО!";
    } else {
        //Если такого устройства нет
        if (DEBUG_MODE) qDebug() << "СОЗДАНИЕ ОКНА НС: НЕТ ПРИБОРА С ИНДЕКСОМ "  << index;
    }
}

//Выставление статуса в окне
void UI::_setDeviceWindowStatus(quint16 index, status stat) {
   int i;
    //Если таой прибор есть
    if (deviceWindows.contains(index)) {
        if(deviceArray[index].pumps < 6) {
            deviceWindows[index]->pumpSelector[deviceArray[index].pumps-1]->setVisible(true);
            deviceWindows[index]->pumps->setTitle("Насосы(" + QString::number(deviceArray[index].pumps) + ")");
        };
        //Выставляем входы-выходы и насосы
        for (i=0;i<6;i++) {
            deviceWindows[index]->in[i]->setText((stat.in[i]) ? "1" : "0");
            deviceWindows[index]->in_g[i]->setEnabled(stat.in[i]);
            deviceWindows[index]->rdy[i]->setPixmap(QPixmap((stat.rdy[i]) ? ":/images/rdySmallN.png" : ":/images/rdySmallDisN.png"));
            deviceWindows[index]->run[i]->setPixmap(QPixmap((stat.run[i]) ? ":/images/runSmallN.png" : ":/images/runSmallDisN1.png"));
            deviceWindows[index]->err[i]->setPixmap(QPixmap((stat.err[i]) ? ":/images/errSmallN.png" : ":/images/errSmallDisN.png"));
            deviceWindows[index]->pwr[i]->setPixmap(QPixmap((stat.pwr[i]) ? ":/images/pwrSmallN.png" : ":/images/pwrSmallDisN.png"));
            //Если значение больше, чем количество насосов
            if (i >= deviceArray[index].pumps) {
                deviceWindows[index]->rdy[i]->setPixmap(QPixmap(":/images/rdySmallUnN.png"));
                deviceWindows[index]->run[i]->setPixmap(QPixmap(":/images/runSmallUnN.png"));
                deviceWindows[index]->err[i]->setPixmap(QPixmap(":/images/errSmallUnN1.png"));
                deviceWindows[index]->pwr[i]->setPixmap(QPixmap(":/images/pwrSmallUnN.png"));
            };
        }

        //Заполняем окно состояния
        deviceWindows[index]->text->clear();
        if (deviceWindows[index]->history->isVisible()) deviceWindows[index]->text->append("<font color='#999' aling=center>История</font>");
        if (deviceWindows[index]->history->isVisible() && deviceWindows[index]->historyList->count() == 0) {
            deviceWindows[index]->text->append("<font color='#999' size=1.4 align=left>Нет записей за выбранный период</font>");
            return;
        };
        if (stat.stat == "ready") {
            deviceWindows[index]->text->append("<font color='#090' size=1.4 align=left>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *ГОТОВ*</font>");
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - ГОТОВ К РАБОТЕ");
        } else if (stat.stat == "off") {
            deviceWindows[index]->text->append("<font color='#666'size=1.4>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *ВЫКЛЮЧЕН*</font>");
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - ВЫКЛЮЧЕН");
        } else if (stat.stat == "handMode") {
            deviceWindows[index]->text->append("<font color='#990'size=1.4>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *В РУЧНОМ РЕЖИМЕ*</font>");
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - В РУЧНОМ РЕЖИМЕ");
        }  else if (stat.stat == "timeout") {
            deviceWindows[index]->text->append("<font color='#666'size=1.4>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *НЕТ ОТВЕТА*");
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - НЕТ ОТВЕТА");
        }  else if (stat.stat == "alarm") {
            deviceWindows[index]->text->append("<font color='#f00'size=1.4>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *АВАРИЯ*</font>");
            deviceWindows[index]->SetWindowTitle(deviceArray[index].name + " - АВАРИЯ");
            foreach (quint8 err, stat.errors) {
                deviceWindows[index]->text->append("<font color='#900' size=1>*E" + ((err > 9) ? QString("") : QString("0")) + QString::number(err) + "*: " + deviceArray[index].errors[err] + "</font>");
            }
        } else {
            deviceWindows[index]->text->append("<font color='#ccc' size=1.4>" + stat.date.toString("yyyy/MM/dd hh:mm:ss") + "   *НЕИЗВЕСТНОЕ СОСТОЯНИЕ*</font>");
        }

        if (stat.fullText.contains("*NO INFO*"))  deviceWindows[index]->text->append("<font color='#900' size=1>*GSM-информатор: Нет ответа от НС</font>");
        if (stat.fullText.contains("*NO POWER*"))  deviceWindows[index]->text->append("<font color='#900' size=1>*GSM-информатор: Потеря электроснабжения</font>");
        if (stat.fullText.contains("*LO BATTERY*"))  deviceWindows[index]->text->append("<font color='#900' size=1>*GSM-информатор: Разряжен аккумулятор</font>");
        //Выставляем SBM|SSM и AN
        deviceWindows[index]->an->setText(QString().setNum(stat.an,'f',1));
        deviceWindows[index]->an_g->setText(QString().setNum(stat.an,'f',1));
        deviceWindows[index]->sbm->setText((stat.sbm) ? "1" : "0");
        deviceWindows[index]->sbm_g->setEnabled(stat.sbm);
        deviceWindows[index]->sbm_g->setToolTip("SBM=" + QString((stat.sbm) ? "1" : "0"));
        deviceWindows[index]->ssm_g->setEnabled(stat.ssm);
        deviceWindows[index]->ssm_g->setToolTip("SSM=" + QString((stat.ssm) ? "1" : "0"));
        deviceWindows[index]->ssm->setText((stat.ssm) ? "1" : "0");
        deviceWindows[index]->winCon->setDisabled(stat.stat == "timeout");
        deviceWindows[index]->con->setText((stat.stat != "timeout") ? "1" : "0");
        deviceWindows[index]->conStat = (stat.stat != "timeout");
        //Если тип gsm, выставляем ALX-ALW
        if (deviceArray[index].type == "gsm") {
            deviceWindows[index]->alx->setText((stat.alx) ? "1" : "0");
            deviceWindows[index]->alx_2->setText((stat.alx) ? "1" : "0");
            deviceWindows[index]->aly->setText((stat.aly) ? "1" : "0");
            deviceWindows[index]->aly_2->setText((stat.alx) ? "1" : "0");
            deviceWindows[index]->alw->setText((stat.alw) ? "1" : "0");
            deviceWindows[index]->alw_2->setText((stat.alx) ? "1" : "0");
            if (stat.alw) {
                if (deviceArray[index].alw.count() > 0) deviceWindows[index]->text->append("<font color='#900' size=1>Сигнал по дискретному входу W</font>");
                else deviceWindows[index]->text->append("<font color='#900' size=1>" + deviceArray[index].alw + "</font>");

            };

            if (stat.alx) {
                if (deviceArray[index].alx.count() > 0) deviceWindows[index]->text->append("<font color='#900' size=1>Сигнал по дискретному входу X</font>");
                else deviceWindows[index]->text->append("<font color='#900' size=1>" + deviceArray[index].alx + "</font>");

            };

            if (stat.alw) {
                if (deviceArray[index].aly.count() > 0) deviceWindows[index]->text->append("<font color='#900' size=1>Сигнал по дискретному входу Y</font>");
                else deviceWindows[index]->text->append("<font color='#900' size=1>" + deviceArray[index].aly + "</font>");

            };
        };
    } else {
        if (DEBUG_MODE) qDebug() << "ИЗМЕНЕНИЕ СТАТУСА ОКНА: НЕТ ОКНА С ИНДЕКСОМ " << index;
    };
}

quint16 UI::_moreThan(quint16 index) {
    quint16 j=0;
    foreach(quint16 i,deviceArray.keys()) if (i > index) j++;
    return j;
}

quint16 UI::_lessThan(quint16 index) {
    quint16 j=0;
    foreach(quint16 i,deviceArray.keys()) if (i < index) j++;
    return j;
}

void UI::setCascadeWindows() {
    foreach(quint16 index,deviceArray.keys()) {
        deviceWindows[index]->setGeometry(pos().x()+ 200 + 20 * _lessThan(index),
                                          pos().y() + 100 + 5 * _lessThan(index),
                                          deviceWindows[index]->width(),
                                          deviceWindows[index]->height());
    };
}

void UI::saveGsmWorking(status stat, quint16 devId) {
    qDebug() << "SAVING GSM WORKING: DEV " << devId;
    if (stat.correct && stat.daily) {
        int i;
        bool need = false;
        QDateTime dt(QDateTime::currentDateTime());
        QString out("INSERT OR REPLACE INTO work (year, month, day, hour, work, did, uid) VALUES ");
        for (i=0;i<24;i++) {
            if (stat.work[i] != -1 && stat.work[i] != 255) {
                if (need) out.append(",");
                out.append(QString("('%1', '%2','%3','%4','%5','%6','%7')").arg(dt.date().year()).arg(dt.date().month()).arg(dt.date().day()).arg(dt.time().hour()).arg(stat.work[i]).arg(devId).arg(QString(dt.toString("yyyy/MM/dd/hh")).append("&d").append(QString::number(devId))));
                need = true;
                qDebug() << "WORKING" << dt << i << stat.work[i];
            }
            dt = dt.addSecs(-3600);
        }

        QSqlQuery query("",QSqlDatabase::database("con1"));
        if (query.exec(out)) {
            trayMessage("APM SK-712","Суточная СМС сохранена",3);
        } else {
            QString error = query.lastError().text();
            qDebug() << "WRITING ERROR!!!";
            qDebug() << error;
            trayMessage("APM SK-712","Не удалось сделать запись в БД:" + error,2);
        };
    }


}

