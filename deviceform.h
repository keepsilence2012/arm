#ifndef DEVICEFORM_H
#define DEVICEFORM_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QMouseEvent>
#include <QMoveEvent>

namespace Ui {
class deviceForm;
}

class deviceform : public QWidget
{
    Q_OBJECT
    
public:
    explicit deviceform(QWidget *parent = 0);
    ~deviceform();
    Ui::deviceForm *ui;

    //Нужные уазатели
    QLabel * index; //Номер НС
    QLabel * pointer; //Указатель
    QLabel * status; //Состояние
    QLabel * map; //Карта
    QLabel * date; //Дата статуса
    QLabel * name; //Имя
    bool warning;
    bool moving;
    void enterEvent(QEvent *ev); //Наведение мыши
    void leaveEvent(QEvent *ev); //Убратие мыши
    void mouseDoubleClickEvent(QMouseEvent *ev);//Двойной щелчок
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

    //Флаг сигнала тревоги
    bool alarm;
signals:
    void hovered(quint16); //Наведен курсор
    void left(quint16); // курсор убран
    void dblclick(quint16); //Двойной щелчок
    void moved(quint16);
    void released(quint16);
};

#endif // DEVICEFORM_H
