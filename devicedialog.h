#ifndef DEVICEDIALOG_H
#define DEVICEDIALOG_H

#include<QComboBox>
#include <QDebug>
#include <QDialog>
#include <QFocusEvent>
#include <QList>
#include <QLineEdit>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QTextBrowser>
#include <QWidget>
#include "focusclasses.h"

namespace Ui {
class deviceDialog;
}


class deviceDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit deviceDialog(QWidget *parent = 0);
    ~deviceDialog();
    Ui::deviceDialog *ui;
    QWidget * devTab;
    QWidget * winTab;
    QWidget * tcpWidget;
    QWidget * mdbWidget;
    QWidget * gsmWidget;
    QSpinBox * index;
    QLineEdit * name;
    QComboBox * type;
    QLineEdit * gsmNum;
    QLineEdit * tcpNum;
    QSpinBox * tcpAddr;
    QComboBox * mdbPort;
    QSpinBox * mdbAddr;
    QSpinBox * mdbPage;
    QComboBox * start;
    QPushButton * ok;
    QPushButton * cancel;
    QPushButton * del;
    QLineEdit * alx;
    QLineEdit * aly;
    QLineEdit * alw;
    QSpinBox * pumps;
    QDoubleSpinBox * flow;
    QList<QDoubleSpinBox *> flowBoxes;
    QList<QWidget * > flowGraphics;
    QComboBox * view;
    QRadioButton * dig;
    QRadioButton * graph;
    QPushButton * digPic;
    QPushButton * graphPic;
    QTextBrowser * help;
    quint8 viewType();
signals:
    void deleted(quint16);
public slots:
    void typeChanged();
    void okPr();
    void digPicPr();
    void graphPicPr();
    void delDevice();
    void digCheck(bool on);
    void showHelp(QString par);

    void indexPr();
    void namePr();
    void typePr();
    void pumpsPr();
    void alPr();

   void flowTypeChanged();
   void manyPumpsValueChanged();

};

#endif // DEVICEDIALOG_H
