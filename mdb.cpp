#include "mdb.h"


mdbPort::mdbPort(QObject *parent) : QObject(parent) {
    port.setParent(this);
    iterator = 0;
    connect(&timer,SIGNAL(timeout()),this,SLOT(read()),Qt::DirectConnection);
    timer.setInterval(5000);
    timer.start();
    skip = false;
}

mdbPort::mdbPort(QString pn, QObject *parent) : QObject(parent) {
    port.setParent(this);
    pName = pn;
    iterator = 0;
    connect(&timer,SIGNAL(timeout()),this,SLOT(read()),Qt::DirectConnection);
    timer.setInterval(5000);
    timer.start();
    skip = false;
}

mdbPort::~mdbPort() {
    myThread.quit();
    myThread.wait();
}

void mdbPort::showPEr(QSerialPort::SerialPortError er) {
    qDebug() << "PORT ERROR!!!" << er;
    port.disconnect();
    if (port.isOpen()) port.close();
    qDebug() << "disconnected";
    //runPort(port.portName());
    QTimer::singleShot(1000,this,SLOT(runP()));
}

void mdbPort::runP() {
    runPort(port.portName());
}

bool mdbPort::runPort(QString name) {
    if (name != pName) {
        return false;
    }
    qDebug() << "открытие порта";
    bool result = true;
    if (port.isOpen()) return true;
    port.setPortName(name);
    if (port.open(QIODevice::ReadWrite)) {
        result = (port.setBaudRate(QSerialPort::Baud9600)
                  && port.setDataBits(QSerialPort::Data8)
                  && port.setFlowControl(QSerialPort::NoFlowControl)
                  && port.setStopBits(QSerialPort::TwoStop)
                  && port.setParity(QSerialPort::NoParity));
         connect(&port,SIGNAL(error(QSerialPort::SerialPortError)),this,SLOT(showPEr(QSerialPort::SerialPortError)));
    } else {
        result = false;
        qDebug() << "Невозможно открыть порт";
        return false;
    };
    qDebug() << ((result) ? "Порт открыт с соответствующими настройками" : "Невозможно настроить порт");
}

void mdbPort::read() {
    if (!port.isOpen()) {
        showPEr(QSerialPort::UnknownError);
        //return;
    };
    QByteArray answer = port.readAll();
    qDebug() << show(answer);
    if (devices.count() == 0) return;
    if (answer.count() == 0) {
        qDebug() << "COM12 открыт: " << port.isOpen();
        qDebug() << "Нет пакетов от порта";
        if (devices.count() > iterator) {
            if (!devices[iterator].bad) {
                skip = true;
                devices[iterator].bad = true;
            } else {
                emit noSignal(devices[iterator].index,1);
            };

        };
    } else if (crc(answer)) {
        qDebug() << "Неверная контрольная сумма";
        if (devices.count() > iterator) {
            if (!devices[iterator].bad) {
                skip = true;
                devices[iterator].bad = true;
            } else {
                emit noSignal(devices[iterator].index,2);
            };

        };
    } else if (!skip){
        //qDebug() << show(answer);
        if (devices.count() > iterator) {
            if ((quint8)answer.at(1) == 4) emit data(devices[iterator].index,mdbToString(answer));
            if ((quint8)answer.at(1) == 6) qDebug() << (quint8)answer.at(2) << "Изменен";
            devices[iterator].bad = false;
        } else qDebug() << "BIG ITERATOR: " << iterator;
    };
    if (skip) skip = false; else iterator = (iterator+1) % devices.count();
    send();
}

void mdbPort::send() {
    if (devices.count() > 0) {
        if (devices.count() > iterator) {
            qDebug() << "СООБЩЕНИЕ ОТПРАВЛЯЕТСЯ :" << show(make(iterator));;
            port.write(make(iterator));
            if (devices[iterator].message != "5") devices[iterator].message = "5";
        } else iterator = 0;
    };
}

QByteArray mdbPort::make(quint16 index) {
    if (devices.count() > index) {
        QByteArray b;
        b.resize(6); b[0] = devices[index].addr;
        //b[1] = (devices[index].status == 6) ? 6 : 4; b[2] = devices[index].page; for (int i=1;i<4;i++) b[2+i] = (s.data << 8*i) >> 24;
        if (devices[index].message == "1") {
            b[1] = 6; b[2] = devices[index].page; b[3] = 0; b[4] = 0; b[5] = 0x3F;
        } else if (devices[index].message == "0") {
            b[1] = 6; b[2] = devices[index].page; b[3] = 0; b[4] = 0; b[5] = 0x00;
        } else if (devices[index].message == "5") {
        b[1] = 4; b[2] = devices[index].page; b[3] = 0; b[4] = 0; b[5] = 16;
        };
        quint16 sum = crc(b); b.resize(8); b[6] = sum & 0x00FF; b[7] = sum >> 8;
        //qDebug() << show(b);
        return b;
    }
}

void mdbPort::setMessage(QString pn, quint16 index, QString message) {
    if (pn != pName) {
        return;
    }
    for(int i=0;i<devices.count();i++) {
        if (devices[i].index == index) {
            devices[i].message = message;
            return;
        };
    }
}

QString mdbPort::show(QByteArray byte) {
    QString out,o;
    for (int i = 0;i<byte.length();i++) {
        o = QString().setNum((quint8)byte.at(i),16);
        while (o.count() < 2) o.prepend("0");
        out += o;
    }
    return out;
}

mdb::mdb(QObject * parent) : QObject(parent)
{

    qRegisterMetaType<device>("device");
    connect(this,&mdb::RemoveDevice,this,&mdb::removeDevice);
    connect(this,&mdb::AddDevice,this,&mdb::addDevice);
    connect(this,&mdb::SetMessage,this,&mdb::setMessage);
}


void mdb::addDevice(QString port, quint16 index, quint16 address, quint8 page) {
    quint8 addr = address & 0xff;
    foreach(mdbPort * p,ports) {
        foreach(device dev,p->devices) if (dev.index == index) {
            return;
            qDebug() << "device at" << port << "index" << index << "address" << address << "already exists";
        }
    };

    if (ports.contains(port)) {
        qDebug() << "port exists. Adding device";
        device dev; dev.addr = addr; dev.index = index; dev.page = page; dev.message = "5"; dev.bad = false;
        AddDeviceToPort(dev, port);
    } else {
        qDebug() << "no such port. Creating";
        addPort(port);
        addDevice(port,index,addr,page);
    };
}

void mdb::addPort(QString name) {
    if (!(ports.contains(name))) {
        if (ports[name] != 0) {
            disconnect(ports[name]);
            disconnect(portThreads[name]);
            delete ports[name];
            delete portThreads[name];
        }
        ports.insert(name,new mdbPort(name));
        portThreads.insert(name,new QThread());
        connect(ports[name],SIGNAL(data(quint16,QString)),this,SIGNAL(Data(quint16,QString)));
        connect(ports[name],SIGNAL(noSignal(quint16,quint8)),this,SIGNAL(noSignal(quint16,quint8)));
        connect(this,&mdb::RunPort,ports[name],&mdbPort::runPort);
        connect(this,&mdb::AddDeviceToPort,ports[name],&mdbPort::addDevice);
        connect(this,&mdb::SetMessageToPort,ports[name],&mdbPort::setMessage);
        connect(this,&mdbPort::destroyed,[this,name](){
           ports.remove(name);
        });
        ports[name]->moveToThread(portThreads[name]);
        portThreads[name]->start();
        connect(portThreads[name],&QThread::finished,ports[name],&QObject::deleteLater);


        emit RunPort(name);

        qDebug() << "port" << name << "added";
    };
}

void mdb::deletePort(QString name) {
    if (ports.contains(name)){
        portThreads[name]->quit();
        portThreads[name]->wait();

    }
}

void mdb::gotData(quint16 index, QString dat) {
    emit Data(index,dat);
}

void mdb::removeDevice(quint16 index) {
    foreach(mdbPort * p,ports) {
        foreach(device dev,p->devices) if (dev.index == index) {
            p->removeDevice(index);
            return;
        };
    };
}

void mdb::setMessage(quint16 index,QString message) {
    foreach(mdbPort * port,ports) {
        foreach(device dev, port->devices) {
            if (dev.index == index) {
                emit SetMessageToPort(port->pName,index,message);

            };
        };
    }
}
