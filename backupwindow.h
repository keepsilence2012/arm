#ifndef BACKUPWINDOW_H
#define BACKUPWINDOW_H

#include <QDebug>
#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>

namespace Ui {
class backupWindow;
}

class backupWindow : public QDialog
{
    Q_OBJECT

public:
    void addItem(QString text, QString value);
    explicit backupWindow(QWidget *parent = 0);
    ~backupWindow();
    QString value();
private slots:
    void okPr();
    void selCh();
private:
    Ui::backupWindow *ui;
};

#endif // BACKUPWINDOW_H
