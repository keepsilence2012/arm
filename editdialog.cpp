#include "editdialog.h"
#include "ui_editdialog.h"

editDialog::editDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editDialog)
{
    ui->setupUi(this);
    connect(ui->ok,SIGNAL(clicked()),this,SIGNAL(ok()));
    connect(ui->no,SIGNAL(clicked()),this,SIGNAL(no()));
    list = ui->listWidget;
}

editDialog::~editDialog()
{
    delete ui;
}
