#ifndef PASSWORD_H
#define PASSWORD_H

#include <QDialog>
#include <QDate>
#include <QCloseEvent>
#include <QShortcut>

namespace Ui {
class password;
}

class password : public QDialog
{
    Q_OBJECT
    
public:
    explicit password(QWidget *parent = 0);
    ~password();
    Ui::password *ui;
    QString pass;
    bool reallyClose;
    QShortcut * enterPressed;
    void closeEvent(QCloseEvent *ev);
signals:
    void ok();
    void no();
public slots:
    void okPressed();
};

#endif // PASSWORD_H
