#include "changeslog.h"
#include "ui_changeslog.h"

changesLog::changesLog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changesLog)
{
    ui->setupUi(this);
    setWindowTitle("Обновление версии АРМ SK-712");
    setModal(true);
}

changesLog::~changesLog()
{
    delete ui;
}

void changesLog::append(QString text) {
    ui->textBrowser->append(text);
}
