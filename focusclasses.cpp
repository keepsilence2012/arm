#include "focusclasses.h"

devSpinBox::devSpinBox(QWidget *parent) : QSpinBox(parent) {
    setFocusPolicy(Qt::StrongFocus);
}

void devSpinBox::focusInEvent(QFocusEvent *event) {
    emit focused();
    QWidget::setFocus(Qt::MouseFocusReason);
    repaint();
    event->accept();
}
