#ifndef DEVICEBUSE_H
#define DEVICEBUSE_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>

namespace Ui {
class deviceBuse;
}

class deviceBuse : public QWidget
{
    Q_OBJECT
    
public:
    explicit deviceBuse(QWidget *parent = 0);
    ~deviceBuse();
    quint16 index;
    Ui::deviceBuse *ui;
    QLabel * text;
    QLabel * image;
    bool moving;
    void enterEvent(QEvent *ev);
    void leaveEvent(QEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
signals:
    void hovered(quint16);
    void left(quint16);
    void dbl(quint16);
    void moved(quint16);
    void released(quint16);

};

#endif // DEVICEBUSE_H
