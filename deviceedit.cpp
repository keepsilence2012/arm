#include "deviceedit.h"
#include "ui_deviceedit.h"

deviceEdit::deviceEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::deviceEdit)
{
    ui->setupUi(this);

    alx = ui->alx;
    aly = ui->aly;
    alw = ui->alw;
    num1 = ui->num1;
    num2 = ui->num2;
    num = ui->num;
    name = ui->name;
    index = ui->index;
    pumps = ui->pumps;
    type = ui->type;
    mdbAddr = ui->mdbAddr;
    mdbPort = ui->mdbPort;
    gsmWidget = ui->gsmAddress;
    tcpWidget = ui->tcpAddress;
    mdbWidget = ui->mdbAddress;

    pumps->setRange(1,6);
    index->setMinimum(1);
    delButton = ui->del;
    ui->tcpAddress->setVisible(false);
    ui->mdbAddress->setVisible(false);
    foreach(QSerialPortInfo info,QSerialPortInfo::availablePorts()) mdbPort->addItem(info.portName());
    mdbAddr->setRange(1,254);



    connect(ui->ok,SIGNAL(clicked()),this,SLOT(checkAccept()));
    connect(ui->cancel,SIGNAL(clicked()),this,SIGNAL(rejected()));
    connect(ui->del,SIGNAL(clicked()),this,SIGNAL(deleteDevice()));
    connect(num1,SIGNAL(textChanged(QString)),this,SLOT(num1Changed()));
    connect(num2,SIGNAL(textChanged(QString)),this,SLOT(num2Changed()));
    connect(ui->type,SIGNAL(currentTextChanged(QString)),this,SLOT(typeChanged()));
}

deviceEdit::~deviceEdit()
{
    delete ui;
}

void deviceEdit::checkAccept() {
    if (ui->type->currentText() == "gsm") {
        if (ui->num1->text().contains(QRegExp("^[0-9]{3}$")) && ui->num2->text().contains(QRegExp("^[0-9]{6,10}"))) {
            if (ui->name->text().count() > 3) {
                emit accepted();
            } else {
                emit Error("Название НС должно содержать более 3 знаков!");
            };
        } else {
            emit Error("Неправильно введен номер!");
        };
    } else if (ui->type->currentText() == "tcp") {
        if (ui->name->text().count() <= 3) {
            emit Error("Название НС должно содержать более 3 знаков!");
        }
        else if (ui->num->text().contains(QRegExp("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}(:[0-9]{1,10}){0,1}$")) ||
                ui->num->text().contains(QRegExp("^([0-9a-zA-Z_\\-\\.]){1,128}(:[0-9]{1,10}){0,1}$"))){
            emit accepted();
        } else {
            emit Error("Адрес НС введен некорректно!");
        };
    } else if (ui->type->currentText() == "modbus") {
        if (ui->name->text().count() <= 3) {
            emit Error("Название НС должно содержать более 3 знаков!");
        } else {
            emit accepted();
        };
    };
}

void deviceEdit::num1Changed() {
    num1->setText(num1->text().remove(QRegExp("\\D")));
    if (num1->text().count() >= 3) {
        num1->setText(num1->text().left(3));
        num2->setFocus();
    }
}

void deviceEdit::num2Changed() {
    num2->setText(num2->text().remove(QRegExp("\\D")));
}
void deviceEdit::typeChanged() {
    if (ui->type->currentText() == "gsm") {
        ui->tcpAddress->setVisible(false);
        ui->gsmAddress->setVisible(true);
        ui->mdbAddress->setVisible(false);
        num->clear();
        ui->alio->setVisible(true);
    } else if (ui->type->currentText() == "tcp") {
        ui->tcpAddress->setVisible(true);
        ui->gsmAddress->setVisible(false);
        ui->mdbAddress->setVisible(false);
        num1->clear(); num2->clear();
        ui->alio->setVisible(false);
    } else if (ui->type->currentText() == "modbus"){
        ui->tcpAddress->setVisible(false);
        ui->gsmAddress->setVisible(false);
        ui->mdbAddress->setVisible(true);
        num->clear();
        num1->clear(); num2->clear();
        ui->alio->setVisible(false);
    };
}
