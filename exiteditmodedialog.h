#ifndef EXITEDITMODEDIALOG_H
#define EXITEDITMODEDIALOG_H

#include <QDialog>

namespace Ui {
class exitEditModeDialog;
}

class exitEditModeDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit exitEditModeDialog(QWidget *parent = 0);
    ~exitEditModeDialog();
    Ui::exitEditModeDialog *ui;
signals:
    void ok();
    void no();
    void cancel();
};

#endif // EXITEDITMODEDIALOG_H
