#include <mdb.h>

QString mdbPort::mdbToString(QByteArray byte) {
    int i,j,k;

    QString tmp,rdy=" RDY=",run=" RUN=",err=" ERR=",pwr = " PWR=", in=" >IN=",an=" >AN=",out="",sbm=" SBM=",ssm=",SSM=";
    QStringList errors;
    ;//qDebug() << byte.length();
    if (byte.length() == 0) return QString("");
        byte = byte.remove(0,3); byte.resize(64);
        //;//qDebug() << "rdy=" << (quint8)byte.at(1);
        j=1; for (i=0;i<6;i++) {if (byte.at(1) & j) rdy+=QString().setNum(i+1); else rdy+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(2) & j) run+=QString().setNum(i+1); else run+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(3) & j) err+=QString().setNum(i+1); else err+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(5) & j) pwr+=QString().setNum(i+1); else pwr+="_"; j=j*2;};
        j=1; for (i=0;i<6;i++) {if (byte.at(7) & j) in+="1"; else in+="0"; j=j*2;};

        an+= QString().setNum(((double)((quint8)byte.at(8))*256 + (quint8)byte.at(9)) / 100,'f',2);

        i = (quint8)byte.at(6) >> 5;
        if (i & 1) out="*ALARM*";
        else if (i & 4) out="*HAND MODE*";
        else if (i & 2) out="*READY*";
        else out="*OFF*";

        if (i&2) sbm+="1"; else sbm+="0";
        if (i&1) ssm+="1"; else ssm+="0";

        out += sbm+ssm+in+an+rdy+run+err+pwr;

        for (k=0;k<=7;k++) {
            i = (quint8)byte.at(17 + k*2);

            for (j=0;j<8;j++) {
                if (i & 1) {
                    tmp = (j != 0) ? QString().setNum(j) :  "0";
                    tmp.prepend((k==0) ? "0" : QString().setNum(k));
                    tmp.prepend("*E"); tmp.append("*");
                    errors.append(tmp);
                    //qDebug() << tmp;
                };
                i = i >> 1;
            };
        };

        foreach(QString s,errors) {
            if (out.length() < 160) out += s + ", ";
        };

        out += " *END*";
        return out;
}

quint16 mdbPort::crc(QByteArray byte) {
    int i = 0, len = byte.length();
    quint16 c = 0xFFFF; i = 0;
    while (len--) c = (c >> 8) ^ Crc16Table[(c ^ byte[i++])&0xFF];
    return c;
}

void mdbPort::addDevice(device dev, QString pn) {
    if (pName != pn) return;
    devices.append(dev);
}

void mdbPort::removeDevice(quint16 index) {
    for (int i=0;i<devices.length();i++) {
        if (devices[i].index == index) {
            devices.removeAt(i);
            skip = true;
            return;
        };
    }
}
