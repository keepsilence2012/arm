#include "statwidget.h"
#include "ui_statwidget.h"

statWidget::statWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::statWidget)
{
    ui->setupUi(this);
    setModal(true);

    hour = -1;
    minDate = QDate(2000,1,1);
    maxDate = QDate(2000,1,1);

    connect(ui->min,SIGNAL(clicked()),this,SLOT(sh_min_pr()));
    connect(ui->max,SIGNAL(clicked()),this,SLOT(sh_max_pr()));

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->lastDate->setVisible(false);
    ui->lastVal->setVisible(false);
    ui->label_4->setVisible(false);

}

statWidget::~statWidget()
{
    delete ui;
}

void statWidget::setData(QDate startDate, QDate stopDate, double maxVal, QDate maxDate, double minVal, QDate minDate, double averVal, double lastVal, QDate lastDate, int startHour, int stopHour) {
    this->maxDate = maxDate;
    this->minDate = minDate;
    this->hour = startHour;
    ui->maxVal->setText((maxVal > -1) ? QString::number(maxVal) : QString("н/д"));
    ui->maxDate->setText((maxVal > -1) ? maxDate.toString("dd.MM.yyyy") : QString("-"));

    ui->minVal->setText((minVal > -1) ? QString::number(minVal) : QString("н/д"));
    ui->minDate->setText((minVal > -1) ? minDate.toString("dd.MM.yyyy") : QString("-"));

    ui->lastVal->setText((lastVal > -1) ? QString::number(lastVal) : QString("н/д"));
    ui->lastDate->setText((lastVal > -1) ? lastDate.toString("dd.MM.yyyy") : QString("-"));

    ui->averVal->setText((lastVal > -1) ? QString::number(averVal,'f',2) : QString("н/д"));

    ui->startTime->setText(QString("%1:00").arg(startHour));
    ui->stopTime->setText(QString("%1:00").arg(stopHour));

    ui->startDate->setText(startDate.toString("dd.MM.yyyy"));
    ui->stopDate->setText(stopDate.toString("dd.MM.yyyy"));

    setWindowTitle(QString("Статистика наработки с %1:00ч по %2:00ч").arg(startDate.toString("dd.MM.yyyy")).arg(stopDate.toString("dd.MM.yyyy")));
}

void statWidget::sh_min_pr() {
    emit showMinDate(hour,maxDate);
    close();
}

void statWidget::sh_max_pr() {
    emit showMaxDate(hour,maxDate);
    close();
}
