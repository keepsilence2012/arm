#include "ui.h"
/********************************************************************************************
        API-ФУНКЦИИ
*******************************************************************************************/
//Фунция, выорчивающая настройки карт из файла
void UI::_setMapSettings(QString setStr) {
    // Делим строки по разделителю
    setStr.remove("system_maps:");
    QStringList l = setStr.split(";");
    //Если строка не подхдит нам по параметрам, устанавливаем дефолтные значения
    if (l.count() < 2 || l[0].toInt() < 1) {
        //Ставим одну карту с дефолтовым рисунком
        maps.clear();
        maps.append(DEF_MAP);
        mapCount = 1;
    } else {
        //Если строка нормальная, будем грузить настройи
        mapCount = l[0].toInt();
        //Для каждой карты мы загрузим ее имя. Если его нет, или оно некорректное, ставим дефолтовое
        for(int i = 1; i<=mapCount;i++) {
            if (l.count() > i && QFile(MAIN_PATH + "maps/" + l[i]).exists()) {
                maps.append(l[i]);
            } else {
                maps.append(DEF_MAP);
            };
        }
    };

    if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА НАСТРОЕК КАРТ: Карт в системе: " << mapCount;
    if (DEBUG_MODE) foreach(QString s, maps) qDebug() << s;
}

//Функция загрузки настроек для COM-порта модема
void UI::_setPortSettings(QString setStr) {
    if (setStr.split(";").count() != 5) {
        //Если строка не проходит по формату, загружаем дефолтовые значения
        _setPortSettings("9600;8;1;0;0");
    }
    else {
        //Если все ок, будем грузить значения
        //СКОРОСТЬ ПОРТА
        if (setStr.split(";")[0] == "1200") Port.setBaudRate(QSerialPort::Baud1200);
        else if (setStr.split(";")[0] == "2400") Port.setBaudRate(QSerialPort::Baud2400);
        else if (setStr.split(";")[0] == "4800") Port.setBaudRate(QSerialPort::Baud4800);
        else if (setStr.split(";")[0] == "9600") Port.setBaudRate(QSerialPort::Baud9600);
        else if (setStr.split(";")[0] == "19200") Port.setBaudRate(QSerialPort::Baud19200);
        else if (setStr.split(";")[0] == "38400") Port.setBaudRate(QSerialPort::Baud38400);
        else if (setStr.split(";")[0] == "57600") Port.setBaudRate(QSerialPort::Baud57600);
        else if (setStr.split(";")[0] == "115200") Port.setBaudRate(QSerialPort::Baud115200);
        else  _setPortSettings("9600;8;1;0;0");
        //ЧИСЛО БИТОВ В ПАКЕТЕ
        if(setStr.split(";")[1] == "5") Port.setDataBits(QSerialPort::Data5);
        else if(setStr.split(";")[1] == "6") Port.setDataBits(QSerialPort::Data6);
        else if(setStr.split(";")[1] == "7") Port.setDataBits(QSerialPort::Data7);
        else if(setStr.split(";")[1] == "8") Port.setDataBits(QSerialPort::Data8);
        else _setPortSettings("9600;8;1;0;0");
        //ЧИСЛО СТОПОВЫХ БИТОВ
        if (setStr.split(";")[2] == "1") Port.setStopBits(QSerialPort::OneStop);
        else if (setStr.split(";")[2] == "1.5") Port.setStopBits(QSerialPort::OneAndHalfStop);
        else if (setStr.split(";")[2] == "2") Port.setStopBits(QSerialPort::TwoStop);
        else _setPortSettings("9600;8;1;0;0");
        //ЧЕТНОСТЬ
        if (setStr.split(";")[3] == "0") Port.setParity(QSerialPort::NoParity);
        else if (setStr.split(";")[3] == "1") Port.setParity(QSerialPort::EvenParity);
        else if (setStr.split(";")[3] == "2") Port.setParity(QSerialPort::OddParity);
        else if (setStr.split(";")[3] == "3") Port.setParity(QSerialPort::SpaceParity);
        else if (setStr.split(";")[3] == "4") Port.setParity(QSerialPort::MarkParity);
        else _setPortSettings("9600;8;1;0;0");
        //УПРАВЛЕНИЕ ПОТОКОМ
         if (setStr.split(";")[4] == "0") Port.setFlowControl(QSerialPort::NoFlowControl);
         else if (setStr.split(";")[4] == "1") Port.setFlowControl(QSerialPort::HardwareControl);
         else if (setStr.split(";")[4] == "2") Port.setFlowControl(QSerialPort::SoftwareControl);
         else _setPortSettings("9600;8;1;0;0");
    };
}

//Закрытие или открытие настроек

void UI::_setChangingsEnabled(bool on) {
    //Выставляем флаг настроек
        enableChangings = on;
        editMaps->setVisible(!on);
    //Включаем или выключаем меню редактирвания
        editPanel->setVisible(on);

}

//Фунция загрузки общих ошибок
void UI::_loadCommonErrors() {
    //Файл с ошибками
    QFile file(MAIN_PATH + "config/" + ERRORS_FILE);
    //Если файл открывается

        //Выгружаем его в переменную

        //Для каждой строки, если строка делится
            //Делим ее
            //Записываем в глобальный массив
}

/****************************************************************************
    ОСНОВНЫЕ ФУНКЦИИ МОДУЛЯ НАСТРОЕК
***************************************************************************/

//Функция, загружающая строку с настройками из файла
void UI::loadSettings() {
    QString systemPassword = "";
    //Файл с настройками
    QFile file(MAIN_PATH + "config/" + SETTINGS_FILE);

    if (!file.open(QIODevice::ReadOnly)) {
        //ЕСЛИ ФАЙЛ ОТКРЫТЬ НЕЛЬЗЯ
        trayMessage("Загруза настроек", " Невозможно загрузить настройки",3);
    }
    else {
        //Считываем весь файл
        QStringList data(QString(file.readAll()).split("\r\n"));
        //Для каждой строки порверяем, чем она является
        foreach (QString str, data)
            if (str.contains("system_maps:")) {
                _setMapSettings(str); //Если строка про карты, выгружаем их в глобальные переменные
            } else if (str.contains("system_port:")) {
                _setPortSettings(str); //Если строка про порт, загружаем его настройки
            } else if (str.contains("system_password:")) {
                str.remove("system_password:");
                systemPassword = QString(QByteArray::fromBase64(str.toUtf8()));
                if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА НАСТРОЕК: Текущий пароль: " << systemPassword;

            } else if (str.contains("system_defPort:")) {
                QString defPort(str);
                Port.close();
                defPort.remove("system_defPort:");
                Port.setPortName(defPort);
            } else if (str.contains("system_tcpWaiting:")) {
                str.remove("system_tcpWaiting:");
                if (str.toInt() >= 5) tcpWaiting = str.toInt();
                if (DEBUG_MODE) qDebug() << "ЗАГРУЗКА НАСТРОЕК: Время ожидания tcp-пакета: " << tcpWaiting << " сек";
            } else if (str.contains("system_fullScreen:")) {
                str.remove("system_fullScreen:");
                fullScreen = (str.toInt() == 1);
            };
        trayMessage("Загруза настроек: ", "Успешно",2);
    };
    if (systemPassword.length() == 0) {
        systemPassword = "sd;lksdj;flaekwjr;lsdkfjaselkj'evmowie';tbouwem'to;ebtump'bpeortuierm'p;";
        trayMessage("Ошибка!","Невозможно загрузить пароль. Используйте пароль по умолчанию",3);
    };
    passwordWindow = new password(this);
    passwordWindow->pass = systemPassword;
    file.close();
}

//Функция для сохранения настроек по файлам
void UI::saveSettings() {
    // ФАЙЛ SETTINGS.CFG
    QFile file(MAIN_PATH + "config/settings.cfg");
    if (file.open(QIODevice::ReadOnly)) {
        QStringList data(QString(file.readAll()).split("\r\n"));
        file.close();
        for(int i=0;i<data.count();i++) {
            //Сохраняем карты
            if (data[i].contains("system_maps:")) {
                data[i] = "system_maps:" + QString::number(mapCount) + ";";
                for(int j=0;j<maps.count();j++) data[i].append(maps[j]).append(";");
            };
            //Сохраняем настройки порта
            if (data[i].contains("system_port:")) {
                data[i] = "system_port:9600;8;1;0;0";
            };
            //Сохраняем текущий пароль
            if (data[i].contains("system_password:")) {
                data[i] = QString(QString("system_password:").toUtf8() + passwordWindow->pass.toUtf8().toBase64());
            };
            //Сохраняем адрес СОМ-порта
            if (data[i].contains("system_defPort:")) {
                data[i] = "system_defPort:" + Port.portName();
            };
            if (data[i].contains("system_tcpWaiting:")) {
                data[i] = "system_tcpWaiting:" + QString::number(tcpWaiting);
            };
            if (data[i].contains("system_fullScreen:")) {
                data[i] = "system_fullScreen:";
                if (fullScreen) data[i].append("1"); else data[i].append("0");
            }
        };
        if (file.open(QIODevice::WriteOnly)) {
            foreach(QString s,data) file.write(s.append("\r\n").toUtf8());
            file.close();
            trayMessage("Сохранение настроек","Изменения успешно сохранены.",3);
        }
        else {
            trayMessage("Ошибка сохранения","Невозможно произвести запись в файл настроек. Изменения настрек не будут сохранены.",3);
        };
    } else {
        trayMessage("Ошибка сохранения","Невозможно открыть файл настроек. Изменения настроек не будут сохранены.",3);
    };
    // ФАЙЛ DEVICES.CFG
    //OLOLOLO
    file.setFileName(MAIN_PATH + "config/devices.cfg");
    if (file.open(QIODevice::WriteOnly)) {
        foreach(quint16 index, deviceArray.keys()) {
            file.write(QString(QString::number(index) + "\r\n").toUtf8());
        }
        file.close();
        trayMessage("Сохранение настроек","Список приборов успешно сохранен.",3);
    } else {
        trayMessage("Ошибка сохранения","Невозможно отрыть файл конфигурации приборов. Изменения параметров приборов не будут сохранены.",3);
    };
    // ФАЙЛЫ ПРИБОРОВ
    QSqlQuery query("",QSqlDatabase::database("con1"));
    if (!query.exec("DELETE FROM devices")) {
        trayMessage("APM SK-712","Не удалось очистить базу приборов: " + query.lastError().text(),3);
        return;
    }

    if (deviceArray.count() == 0) {
        trayMessage("СОХРАНЕНИЕ УСТРОЙСТВ","НЕТ УСТРОЙСТВ В СИСТЕМЕ!",2);
        if (query.exec("DELETE FROM status")) {
            qDebug() << "УДАЛЕНИЕ ИСТОРИИ: ИСТОРИЯ УДАЛЕНА!!!";
        } else {
            trayMessage("APM SK-712","ОШИБКА УДАЛЕНИЯ ЛИШНИХ ЗАПИСЕЙ: " + query.lastError().text(),2);
        };
        return;
    }
    QString queryString = QString("INSERT OR REPLACE INTO devices (did,address,name,type,map,cordx,cordy,t_per,alx,aly,alw,pumps,start,mdbaddr,mdbpage,comment,view,flow) VALUES");
    foreach(deviceStruct dev, deviceArray) {
        queryString.append(QString("(%1,'%2','%3','%4',%5,%6,%7,%8,'%9','%10','%11',%12,'%13',%14,%15,'%16',%17,%18),").arg(dev.index).arg(((dev.tcpPort != 502 && dev.type == "tcp") ? dev.address.append(":").append(QString::number(dev.tcpPort)) : dev.address)).arg(dev.name).arg(dev.type).arg(dev.map).arg(dev.x).arg(dev.y).arg(dev.period).arg(dev.alx).arg(dev.aly).arg(dev.alw).arg(dev.pumps).arg(dev.start).arg(dev.mdbAddr).arg(dev.mdbPage).arg("").arg(dev.view).arg(dev.flow));
    }
    queryString.truncate(queryString.count() - 1);
    if (!query.exec(queryString)) {
        qDebug() << "ОШИБКА СОХРАНЕНИЯ: " << query.lastError();
        trayMessage("APM SK-712","Не удалось сохранить настройки приборов: " + query.lastError().text(),3);
    } else {
        qDebug() << "СОХРАНЕНИЕ ПРИБОРОВ: СОХРАНЕНО!!!";
    }
    qDebug() << "СТРОКА СОХРАНЕНИЯ: " << queryString;

    if (query.exec("DELETE FROM status WHERE device NOT IN (SELECT did FROM devices)")) {
        qDebug() << "УДАЛЕНИЕ ИСТОРИИ: ИСТОРИЯ УДАЛЕНА!!!";
    } else {
        trayMessage("APM SK-712","ОШИБКА УДАЛЕНИЯ ЛИШНИХ ЗАПИСЕЙ: " + query.lastError().text(),2);
    };

}

void UI::_cleanSettingsField() {
    loadedSettingsString.clear();
}

void UI::_savePass(QString newPass) {
    QFile file(MAIN_PATH + "config/settings.cfg");
    QStringList fileData;
    if (file.open(QIODevice::ReadOnly)) {
        fileData = QString(file.readAll()).split("\r\n");
        file.close();
        for (int i=0;i<fileData.count();i++) if (fileData[i].contains("system_password:")) fileData[i] = QString(QString("system_password:").toUtf8() + newPass.toUtf8().toBase64());
        if (file.open(QIODevice::WriteOnly)) {
            foreach(QString s,fileData) file.write(s.append("\r\n").toUtf8());
            file.close();
            passwordWindow->pass = newPass;
            trayMessage("Изменение пароля","Пароль успешно изменен.",3);
        } else {
            trayMessage("Ошибка сохранения пароля","Невозможно сделать запись файл настроек. Пароль не будет изменен.",3);
        };
    } else {
        trayMessage("Ошибка сохранения пароля","Невозможно открыть файл настроек. Изменения не будет изменен.",3);
    };
}

void UI::_saveFullScreen(bool on) {
    QString newPass;
    if (on) newPass = "1"; else newPass = "0";
    bool yes = false;
    QFile file(MAIN_PATH + "config/settings.cfg");
    QStringList fileData;
    if (file.open(QIODevice::ReadOnly)) {
        fileData = QString(file.readAll()).split("\r\n");
        file.close();
        for (int i=0;i<fileData.count();i++) if (fileData[i].contains("system_fullScreen:")) {fileData[i] = QString(QString("system_fullScreen:").toUtf8() + newPass.toUtf8()); yes = true;};
        if (!yes) fileData.append(QString(QString("system_fullScreen:").toUtf8() + newPass.toUtf8()));
        if (file.open(QIODevice::WriteOnly)) {
            foreach(QString s,fileData) file.write(s.append("\r\n").toUtf8());
            file.close();
            trayMessage("Изменение размера по умолчанию","Размер успешно изменен.",3);
        } else {
            trayMessage("Ошибка сохранения размера по умолчанию","Невозможно сделать запись файл настроек. Размер по умолчанию не будет изменен.",3);
        };
    } else {
        trayMessage("Ошибка сохранения  размера по умолчанию","Невозможно открыть файл настроек. Изменения не будет изменен.",3);
    };
}

void UI::_setDefPort(QString defPort) {
    QFile file(MAIN_PATH + "config/settings.cfg");
    QStringList fileData;
    if (file.open(QIODevice::ReadOnly)) {
        fileData = QString(file.readAll()).split("\r\n");
        file.close();
        for (int i=0;i<fileData.count();i++) if (fileData[i].contains("system_defPort:")) fileData[i] = QString(QString("system_defPort:").toUtf8() + defPort.toUtf8());
        if (file.open(QIODevice::WriteOnly)) {
            foreach(QString s,fileData) file.write(s.append("\r\n").toUtf8());
            file.close();
        } else {
            //trayMessage("Ошибка сохранения пароля","Невозможно сделать запись файл настроек. Пароль не будет изменен.");
        };
    } else {
        //trayMessage("Ошибка сохранения пароля","Невозможно открыть файл настроек. Изменения не будет изменен.");
    };
}

void UI::systemLog(QString header, QString text) {
    QFile file(MAIN_PATH + "logs/system_log.txt");
    if (file.open(QIODevice::Append)) {
        file.write(QString(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss") + " " + header + ": " + text + "\r\n").toUtf8());
        file.close();
        if (file.size() > (qint64)1024000) {
            file.rename(MAIN_PATH + "logs/system_log-" + QDateTime::currentDateTime().toString("yyyy-mm-dd-hh-mm") + ".txt");
            QFile file2(MAIN_PATH + "logs/system_log.txt");
            file2.open(QIODevice::WriteOnly);
            file2.write(QString("*****Системный лог*****\r\n").toUtf8());
            file2.close();
        };
    } else {
        trayMessage("Ошибка записи","Нет доступа к системному логу по адресу " + file.fileName());

    };
}

void UI::actionLog(QString header, QString text) {
    QFile file(MAIN_PATH + "logs/action_log.txt");
    if (file.open(QIODevice::Append)) {
        file.write(QString(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss") + " " + header + ": " + text + "\r\n").toUtf8());
        if (file.size() > (qint64)1024000) {
            file.rename(MAIN_PATH + "logs/action_log-" + QDateTime::currentDateTime().toString("yyyy-mm-dd-hh-mm") + ".txt");
            QFile file2(MAIN_PATH + "logs/action_log.txt");
            file2.open(QIODevice::WriteOnly);
            file2.write(QString("*****Лог действий оператра*****\r\n").toUtf8());
            file2.close();
        };
    } else {
        trayMessage("Ошибка записи","Нет доступа к логу оператора");
    };
    file.close();
}
