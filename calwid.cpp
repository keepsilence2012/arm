#include "calwid.h"
#include "ui_calwid.h"

calWid::calWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::calWid)
{
    ui->setupUi(this);

    X = 0; Y = 0;


    cal = new QCalendarWidget(this);
    if (this->parentWidget()->height() > (Y + 230)) {
        cal->setGeometry(0,25,250,200);
        down = true;
    } else {
        down = false;
        cal->setGeometry(0,0,250,200);
    }

    cal->setGridVisible(true);
    cal->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

    qDebug() <<this->parentWidget()->height() <<  Y + 230 << "DOWN " << down;
    dateFocused = false;
    calFocused = false;
    dropped = false;
    firstDrop = true;
    setGeometry(X,Y,116 ,25);
    ui->dateEdit->setGeometry(0,0,116,22);

    ui->dateEdit->installEventFilter(this);
    cal->installEventFilter(this);

    connect(cal,SIGNAL(clicked(QDate)),ui->dateEdit,SLOT(setDate(QDate)));
    connect(ui->dateEdit,SIGNAL(dateChanged(QDate)),cal,SLOT(setSelectedDate(QDate)));
    connect(cal,SIGNAL(activated(QDate)),this,SLOT(activateDate()));
    connect(ui->dateEdit,SIGNAL(dateChanged(QDate)),this,SIGNAL(dateChanged(QDate)));

    cal->close();

    qDebug() << "СОЗДАНИЕ CALWID:" << geometry();
}

calWid::~calWid()
{
    cal->close(); delete cal;
    delete ui;
}

void calWid::setDateRange(QDate from, QDate to) {
    cal->setDateRange(from,to);
    ui->dateEdit->setMinimumDate(from);
    ui->dateEdit->setMaximumDate(to);
}

bool calWid::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == ui->dateEdit && ev->type() == QEvent::FocusIn) {
        dateFocused = true;
        qDebug() << "calWid::eventFilted called" << dateFocused << calFocused << dropped;
        checkFocus();
    } else if (obj == ui->dateEdit && ev->type() == QEvent::FocusOut) {
        dateFocused = false;
        qDebug() << "calWid::eventFilted called" << dateFocused << calFocused << dropped;
        checkFocus();
    } else if (obj == cal && ev->type() == QEvent::Enter) {
        calFocused = true;
        cal->setFocus();
        qDebug() << "calWid::eventFilted called" << dateFocused << calFocused << dropped;
        checkFocus();
    } else if (obj == cal && ev->type() == QEvent::Leave) {
        if (1) {
            calFocused = false;
            qDebug() << "calWid::eventFilted called" << dateFocused << calFocused << dropped;
            checkFocus();
        }
    };
    return false;
}

void calWid::checkFocus() {
    if ((dateFocused || calFocused) && (dropped || firstDrop)) {
        if (down) {
            setGeometry(X,Y,250,225);
            ui->dateEdit->setGeometry(0,0,116,22);
        }
        else {
            setGeometry(X,Y - 200,250,225);
            ui->dateEdit->setGeometry(0,200,116,22);
        }

        cal->show();
        dropped = false;
        firstDrop = false;
        qDebug() << "calWid new pos" << geometry();
    } else QTimer::singleShot(200,this,SLOT(drop()));
}

void calWid::drop() {
    if (!dropped && !dateFocused && !calFocused) {

        if (down) {
            setGeometry(X,Y,116,25);
            ui->dateEdit->setGeometry(0,0,116,22);
        } else {
            setGeometry(X,Y,116,25);
            ui->dateEdit->setGeometry(0,0,116,22);
        }
        setFocus();
        cal->close();
        dropped = true;
        qDebug() << "calWid new pos" << geometry();
    }
}

void calWid::activateDate() {
    calFocused = false;
    drop();
}

QDate calWid::date() {
    return ui->dateEdit->date();
}

void calWid::setDate(QDate d) {
    cal->setCurrentPage(d.year(),d.month());
    ui->dateEdit->setDate(d);
}

void calWid::setPos(int x, int y) {
    X = x; Y = y;


    if (this->parentWidget()->height() > (Y + 230)) {
        cal->setGeometry(0,25,250,200);
        down = true;
    } else {
        down = false;
        cal->setGeometry(0,0,250,200);
    }

        qDebug() << "SETPOS: " << this->parentWidget()->height() <<  Y + 230 << "DOWN " << down;
    if (dropped) {
        if (down) {
            setGeometry(X,Y,250,225);
            ui->dateEdit->setGeometry(0,0,116,22);
        }
        else {
            setGeometry(X,Y - 200,250,225);
            ui->dateEdit->setGeometry(0,200,116,22);
        }
    } else {
        if (down) {
            setGeometry(X,Y,116,25);
            ui->dateEdit->setGeometry(0,0,116,22);
        } else {
            setGeometry(X,Y,116,25);
            ui->dateEdit->setGeometry(0,0,116,22);
        }
    }
    updateGeometry();
    update();
}
