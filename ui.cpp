#include "ui.h"
#include "ui_ui.h"

UI::UI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UI)
{
    ui->setupUi(this);

    //Линковка нужных нам указателей
    statArray.insert("alarm","АВАРИЯ");statArray.insert("handMode","РУЧНОЙ РЕЖИМ");statArray.insert("ready","ГОТОВ");statArray.insert("off","ВЫКЛЮЧЕН");statArray.insert("timeout","НЕТ ОТВЕТА");
    currentMapImage = ui->mapImage;
    mapNumText = ui->mapNumLabel;
    editPanel = ui->editPanel;
    deviceList = new QWidget(ui->dList);
    deviceList2 = new QWidget(ui->dList);
    tray = new QSystemTrayIcon(QIcon(":/images/logo1.png"));
    tray->show();
    fullScreen = false;
    reallyClose = false;
    timeFromLoading = 0;
    deviceList2->setGeometry(ui->dList->geometry());
    deviceList2->setVisible(false);
    ui->changeList->raise();
    ui->changeSortType->raise();
    gsmConnectionPixmap = ui->reg;
    gsmConnectionText = ui->statLabel;
    gsmSignalPixmap = ui->signal;
    gsmSignalText = ui->signLabel;
    gsmSmsNumber = ui->incom;
    conCount = false;
    editMaps = ui->editMaps;
    //pressure * pres = new pressure();
    //pres->show();
    //pressure_2 * pres_2 = new pressure_2();
    //pres_2->show();

    trayMessage("Загрузка программы: ","Старт",2);

    //Загрузка начальных условий
    sortByStatus = false;
    ui->crash->setVisible(false);
    //ui->changeSortType->setChecked(true);
    tcpWaiting = 90;
    mdbWaiting = 90;
    enableSending = false;
    maxIndex = 0;
    minIndex = 63000;
    historyPeriods.append(1);historyPeriods.append(3);historyPeriods.append(7);historyPeriods.append(30);
    loadSettings(); //Загружаем настройки
    openBase(); //Включаем БД
    _setChangingsEnabled(false); //Закрываем настройки от редактирования
    _initPort(); //Инициализируем настройки прта для GSM

    //Включаем первую карту
    _loadMap(0);

    //Запускаем каналы связи
    _portStart(); //Это GSM

    setAvailablePorts();

    //Настраиваем звуки
    alarmSound = QString(MAIN_PATH + "sound/alarm.wav");
    messageSound = QString(MAIN_PATH + "sound/message.wav");


    //Создаем действия
    closeProgramAction = ui->closeAction;
    closeWindowAction = ui->hideAction;
    showWindowAction = ui->showAction;
    trayMenu = new QMenu();
    tray->setContextMenu(trayMenu);
    trayMenu->addAction(closeWindowAction);
    trayMenu->addAction(showWindowAction);
    trayMenu->addAction(closeProgramAction);
    reloadModemAction = ui->reloadModem;
    ui->scroll->setRange(1,64);
    currentVersion = "1.2.0.7";
    downUpd = new downloadUpdate();
    downUpd->setCurrentVersion(currentVersion);

    QTimer::singleShot(5000,downUpd,SLOT(checkUpdate()));

    //Двигаем modbus в отдельный тред
    mdbPort.moveToThread(&mdbThread);

    //Привязываем СИГНАЛЫ и СЛОТЫ
    connect(ui->scroll,SIGNAL(valueChanged(int)),this,SLOT(scrollList(int)));
    connect(ui->nextMap,SIGNAL(clicked()),this,SLOT(showNextMap())); //Нажатие кнопки "Следущая карта" -> "Показать следущую карту"
    connect(ui->prevMap,SIGNAL(clicked()),this,SLOT(showPreviousMap())); //Нажатие кнопки "Предыдущая карта" -> "Показать предыдущую карту"
    connect(ui->editMaps,SIGNAL(clicked()),this,SLOT(showChangings())); //Нажатие кнопки "Показать настройки" -> "Показать настройки"
    connect(ui->changeList,SIGNAL(clicked()),this,SLOT(changeListView())); //Поменять список по нажатию кнопки
    connect(&portQueryTimer,SIGNAL(timeout()),this,SLOT(readGSMPortData())); //CИГНАЛ ТАЙМЕРА GSM -> "Прочитать данные из буффера"
    connect(ui->addDevButton,SIGNAL(clicked()),this,SLOT(addNewStationForm())); //Нажатие кнопки "Создать НС" -> "СОздать НС"
    connect(&gsmTimer,SIGNAL(timeout()),this,SLOT(gsmTimerCycle()),Qt::DirectConnection); //Таймер для отправления смс
    connect(&timeTimer,SIGNAL(timeout()),this,SLOT(changeTimeInList())); //Меняем время в списке по времени
    connect(&tcpTimer,SIGNAL(timeout()),this,SLOT(tcpTimerCycle()));
    connect(closeWindowAction,SIGNAL(triggered()),this,SLOT(closeWindowSlot()));
    connect(closeProgramAction,SIGNAL(triggered()),this,SLOT(closeProgramSlot()));
    connect(showWindowAction,SIGNAL(triggered()),this,SLOT(showWindowSlot()));
    connect(reloadModemAction,SIGNAL(triggered()),this,SLOT(reloadModemSlot()));
    connect(ui->exitEditing,SIGNAL(clicked()),this,SLOT(showExitSettingsDialog()));
    connect(ui->addMap,SIGNAL(clicked()),this,SLOT(addMapPressed()));
    connect(ui->checkUpdate,SIGNAL(triggered()),downUpd,SLOT(checkUpdate()));
    connect(ui->backupAction,SIGNAL(triggered()),this,SLOT(showBackup()));
    connect(ui->changeMap,SIGNAL(clicked()),this,SLOT(editMapPressed()));
    connect(ui->deleteMap,SIGNAL(clicked()),this,SLOT(deleteMapPressed()));
    connect(ui->addDevice,SIGNAL(clicked()),this,SLOT(addDeviceToCurrentMapPressed()));
    connect(ui->deleteDevice,SIGNAL(clicked()),this,SLOT(removeDeviceFromCurrentMapPressed()));
    connect(ui->changePass,SIGNAL(triggered()),this,SLOT(changePassword()));
    connect(ui->fullScreen,SIGNAL(triggered(bool)),this,SLOT(fullScreenMode(bool)));
    connect(ui->about,SIGNAL(triggered()),this,SLOT(showAbout()));
    connect(ui->action_CSV,SIGNAL(triggered()),this,SLOT(exportToCsv()));
    connect(&sendingTimer,SIGNAL(timeout()),this,SLOT(sendingTimerChanged()));
    connect(ui->help,SIGNAL(triggered()),this,SLOT(openHelp()));
    connect(ui->crash,SIGNAL(clicked()),this,SLOT(crashMemory()));
    connect(ui->clearQueue,SIGNAL(clicked()),this,SLOT(clearGsmQueue()));
    //connect(&tcpPort,SIGNAL(status(QByteArray,QBitArray,QString)),this,SLOT(renderTcpResponse(QByteArray,QBitArray,QString)));
    connect(ui->changeSortType,SIGNAL(clicked(bool)),this,SLOT(changeSortType(bool)));
    connect(&mdbPort,SIGNAL(Data(quint16,QString)),this,SLOT(renderMdbResponse(quint16,QString)));
    connect(&mdbTimer,SIGNAL(timeout()),this,SLOT(mdbTimerCycle()));
    connect(&mdbPort,SIGNAL(noSignal(quint16,quint8)),this,SLOT(mdbNoSignal(quint16,quint8)));
    connect(&packsTimer,SIGNAL(timeout()),this,SLOT(writeConStat()));

    connect(this,&UI::AddDevice,&mdbPort,&mdb::AddDevice);
    connect(this,&UI::RemoveDevice,&mdbPort,&mdb::RemoveDevice);
    connect(this,&UI::mdbSetMessage,&mdbPort,&mdb::SetMessage);

    //connect(&tcpPort,SIGNAL(error(QString,int)),this,SLOT(parseTCPError(QString)));
    connect(&conTimer,SIGNAL(timeout()),this,SLOT(windowConTimer()));
    connect(downUpd,SIGNAL(message(QString,QString,int)),this,SLOT(messageFromDownloader(QString,QString,int)));
    connect(&mdbThread,&QThread::finished,&mdbPort,&QObject::deleteLater);

    mdbThread.start();

    //Запускаем таймеры
    gsmIterator = minIndex;
    gsmTimer.setInterval(5000);
    gsmTimer.start();

    timeTimer.setInterval(1000);
    timeTimer.start();

    tcpTimer.setInterval(5000);
    tcpTimer.start();

    packsTimer.setInterval(60000);
    packsTimer.start();

    conTimer.setInterval(1000);
    conTimer.start();

    mdbTimer.setInterval(5000);
    mdbTimer.start();
    sendingTimer.setInterval(750);
    sendingValue = false;
    sendingTimer.start();
    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint);

    if (fullScreen) {
        ui->fullScreen->setChecked(true);
        fullScreenMode(true);
    }



    loadDevices(); //Загружаем устройства

}

UI::~UI()
{
    mdbThread.quit();
    mdbThread.wait();
    tray->hide();
    delete ui;
    delete tray;
}

/***************************************************************
                ОПИСАНИЕ СЛОТОВ ИНТЕРФЕЙСА
 **************************************************************/

void UI::setFormType(quint16 index) {

}

//Показать предыдущую карту
void UI::showPreviousMap() {
    //Загружаем текущую карту
    _loadMap((currentMap - 1 + mapCount) % mapCount);
}

//Показать следущую карту
void UI::showNextMap() {
    //Загружаем текущую карту
    _loadMap((currentMap + 1 + mapCount) % mapCount);
}

//Наведение мыши на указатель
void UI::hovered(quint16 index) {
    //Вызываем функцию увеличения указателя
    _makePointerBigger(index);
}

//Убрать мышь с указателя
void UI::left(quint16 index) {
    //Вызываем фунцию уменьшения указателя
    _makePointerSmaller(index);
}

//Обработка двойного щелчка
void UI::dblckick(quint16 index) {
    //Если есть такой прибор
    if (deviceArray.contains(index)) {
        deviceForms[index]->warning = false;
        //Если включен режим редатирования
        if (enableChangings) {
            //Создаем форму
//            devEdit = new deviceEdit(this);
//            connect(devEdit,SIGNAL(accepted()),this,SLOT(acceptDeviceEdit()));
//            connect(devEdit,SIGNAL(rejected()),this,SLOT(rejectDeviceEdit()));
//            connect(devEdit,SIGNAL(deleteDevice()),this,SLOT(deleteDevice()));
//            connect(devEdit,SIGNAL(Error(QString)),this,SLOT(errorDeviceEdit(QString)));
//            //Заполняем имя
//            devEdit->setWindowTitle(deviceArray[index].name);
//            devEdit->name->setText(deviceArray[index].name);
//            //заполняем индекс
//             devEdit->index->setValue(index);
//            devEdit->type->setCurrentText(deviceArray[index].type);

//            devEdit->typeChanged();
//            //заполняем насосы
//            devEdit->pumps->setValue(deviceArray[index].pumps);
//            //если прибор gsm
//            if (deviceArray[index].type == "gsm") {
//                //выбираем gsm
//                devEdit->type->setCurrentText("gsm");
//                //Заполняем номер
//                devEdit->num1->setText(deviceArray[index].address.mid(1,3));
//                devEdit->num2->setText(deviceArray[index].address.mid(4));
//                //заполняем входы информатора
//                devEdit->alx->setText(deviceArray[index].alx);
//                devEdit->aly->setText(deviceArray[index].aly);
//                devEdit->alw->setText(deviceArray[index].alw);
//            } else if (deviceArray[index].type == "tcp") {
//                devEdit->type->setCurrentText("tcp");
//                devEdit->num->setText(deviceArray[index].address);
//            } else if (deviceArray[index].type == "modbus") {
//                devEdit->type->setCurrentText("modbus");
//                QRegExp reg("-([0-9]{1,3}})$");
//                if (deviceArray[index].address.contains(reg)) {
//                    devEdit->mdbPort->setCurrentText(deviceArray[index].address.remove(reg));
//                    devEdit->mdbAddr->setValue(reg.cap(1).toInt());
//                };
//            };
//            devEdit->type->setDisabled(true);
//            devEdit->mdbAddr->setDisabled(true);
//            devEdit->mdbPort->setDisabled(true);
//            devEdit->delButton->setVisible(true);
//            devEdit->index->setDisabled(true);
//            devEdit->num1->setDisabled(true);
//            devEdit->num2->setDisabled(true);
//            devEdit->num->setDisabled(true);
//            devEdit->setModal(true);
//            devEdit->show();
            foreach(deviceWindow * win,deviceWindows) win->close();
            dev = new deviceDialog(this);
            dev->index->setValue(index);
            dev->index->setDisabled(true);
            dev->name->setText(deviceArray[index].name);
            if (deviceArray[index].type == "gsm") dev->type->setCurrentIndex(0);
            else if (deviceArray[index].type == "modbus") dev->type->setCurrentIndex(1);
            else if (deviceArray[index].type == "tcp") dev->type->setCurrentIndex(2);
            dev->type->setDisabled(true);
            dev->flow->setValue(deviceArray[index].flow);
            dev->start->setCurrentIndex((deviceArray[index].start == "w") ? 1 : 0);
            dev->pumps->setValue(deviceArray[index].pumps);
            dev->typeChanged();
            if (deviceArray[index].view) dev->graph->setChecked(true);
            dev->digCheck(dev->dig->isChecked());
            if (deviceArray[index].type == "gsm") {
                dev->gsmNum->setText(QString(deviceArray[index].address).remove(0,1));
                dev->alx->setText(deviceArray[index].alx);
                dev->aly->setText(deviceArray[index].aly);
                dev->alw->setText(deviceArray[index].alw);
            } else if (deviceArray[index].type == "tcp") {
                dev->tcpNum->setText((deviceArray[index].tcpPort == 502 ) ? deviceArray[index].address : QString(deviceArray[index].address).append(":").append(QString::number(deviceArray[index].tcpPort)));
                dev->tcpAddr->setValue(deviceArray[index].mdbAddr);
            } else if (deviceArray[index].type == "modbus") {
                dev->mdbPort->setCurrentText(deviceArray[index].address);
                dev->mdbAddr->setValue(deviceArray[index].mdbAddr);
                dev->mdbPage->setValue(deviceArray[index].mdbPage);
            };
            if (deviceArray[index].view == 1) dev->graphPicPr();
            connect(dev,SIGNAL(deleted(quint16)),this,SLOT(deleteDevice(quint16)));
            connect(this,SIGNAL(showHelp(QString)),dev,SLOT(showHelp(QString)));
            if (dev->exec() == QDialog::Accepted) {
                if (deviceArray[index].type == "gsm") {
                    deviceArray[index].address = "7" + dev->gsmNum->text();
                    //deviceArray[index].mdbAddr = dev->mdbAddr->value();
                    //deviceArray[index].mdbPage = dev->mdbPage->value();
                    deviceArray[index].name = dev->name->text();
                    deviceArray[index].start = (dev->start->currentIndex()) ? "w" : "d";
                    deviceArray[index].view = dev->viewType();
                    deviceArray[index].pumps = dev->pumps->value();
                    deviceArray[index].alx = dev->alx->text();
                    deviceArray[index].aly = dev->aly->text();
                    deviceArray[index].alw = dev->alw->text();
                    deviceArray[index].flow = dev->flow->value();
                    deviceWindows[index]->flow = deviceArray[index].flow;
                    deviceWindows[index]->alxLab->setToolTip(deviceArray[index].alx);
                    deviceWindows[index]->alxLab_2->setToolTip(deviceArray[index].alx);
                    deviceWindows[index]->alyLab->setToolTip(deviceArray[index].aly);
                    deviceWindows[index]->alyLab_2->setToolTip(deviceArray[index].aly);
                    deviceWindows[index]->alwLab->setToolTip(deviceArray[index].alw);
                    deviceWindows[index]->alwLab_2->setToolTip(deviceArray[index].alw);
                    deviceForms[index]->name->setText(deviceArray[index].name);
                    //Удаляем форму, создаем новую
                    _setDeviceWindowStatus(index,deviceArray[index].stat);
                } else if (deviceArray[index].type == "tcp") {
                    //tcpPort.del(deviceArray[index].address);
                    if (tcpDev.contains(index)) {
                        delete tcpDev[index];
                        tcpDev.remove(index);
                    }
                    //deviceArray[index].address = dev->mdbPort->currentText();
                    //deviceArray[index].mdbAddr = dev->mdbAddr->value();
                    //deviceArray[index].mdbPage = dev->mdbPage->value();
                    //deviceArray[index].address = dev->tcpNum->text();
                    QRegExp portRegExp(":([0-9]{1,5})$");
                    if (dev->tcpNum->text().contains(portRegExp)) {
                        deviceArray[index].tcpPort = portRegExp.cap(1).toUInt();
                        qDebug() << "CUSTOM PORT: " << deviceArray[index].tcpPort;
                        deviceArray[index].address = dev->tcpNum->text().remove(portRegExp);
                    } else {
                        deviceArray[index].tcpPort = 502;
                        deviceArray[index].address = dev->tcpNum->text();
                    }
                    deviceArray[index].mdbAddr = dev->tcpAddr->text().toUInt();
                    deviceArray[index].mdbPage = 0;
                    deviceArray[index].name = dev->name->text();
                    deviceArray[index].flow = dev->flow->value();
                    deviceArray[index].start = (dev->start->currentIndex()) ? "w" : "d";
                    deviceArray[index].view = dev->viewType();
                    deviceArray[index].pumps = dev->pumps->value();
                    deviceForms[index]->name->setText(deviceArray[index].name);
                    deviceWindows[index]->flow = deviceArray[index].flow;
                    //Удаляем форму, создаем новую
                    _setDeviceWindowStatus(index,deviceArray[index].stat);
                    //mdbPort.addDevice(deviceArray[index].address,index,deviceArray[index].mdbAddr,deviceArray[index].mdbPage);
                    //tcpPort.add(deviceArray[index].address,deviceArray[index].mdbAddr);


                    addTcpDev(index,deviceArray[index].address,deviceArray[index].mdbAddr,deviceArray[index].tcpPort);


                } else if (deviceArray[index].type == "modbus") {
                    emit RemoveDevice(index);
                    deviceArray[index].address = dev->mdbPort->currentText();
                    deviceArray[index].mdbAddr = dev->mdbAddr->value();
                    deviceArray[index].mdbPage = 0;
                    deviceArray[index].start = (dev->start->currentIndex()) ? "w" : "d";
                    deviceArray[index].name = dev->name->text();
                    deviceArray[index].flow = dev->flow->value();
                    deviceArray[index].view = dev->viewType();
                    deviceArray[index].pumps = dev->pumps->value();
                    deviceForms[index]->name->setText(deviceArray[index].name);
                    deviceWindows[index]->flow = deviceArray[index].flow;
                    //Удаляем форму, создаем новую
                    _setDeviceWindowStatus(index,deviceArray[index].stat);
                    emit AddDevice(deviceArray[index].address,index,deviceArray[index].mdbAddr,static_cast<quint8>(deviceArray[index].mdbPage));

                };
            };
            delete dev;
            trayMessage("Редактирование прибора: ", "номер " + QString::number(index),6);
        } else {
        //Если не включен
            //Если прибор есть на одной из карт, включаем ее
           if(deviceArray[index].map > 0) _loadMap(deviceArray[index].map - 1);
           //Показываем его окно
           if (deviceWindows.contains(index)) {
               deviceWindows[index]->show();
               deviceWindows[index]->activateWindow();
               deviceWindows[index]->raise();
               if (deviceWindows[index]->history->isVisible()) showFullHistory(index,15);
               trayMessage("Окно прибора: ", "номер " + QString::number(index),6);
           };
        };
    };
}

//Обработка перемещения мыши
void UI::movePointer(quint16 index) {
    //Если такой уазатель есть в природе и перемещения разрешены
    if (deviceArray.contains(index) && enableChangings) {
        if (DEBUG_MODE) qDebug() << "ПЕРЕДВИЖЕНИЕ УКАЗАТЕЛЯ (СЛОТ): " << QCursor::pos() - pos();
        //if (DEBUG_MODE) qDebug() << QCursor::pos().x()*2 - pos().x()*2 - devicePointers[index]->x();
        //if (DEBUG_MODE) qDebug() << QCursor::pos().y()*2 - pos().y()*2 - devicePointers[index]->y();
        //Перемещаем указатель туда, где есть мышь
        if (DEBUG_MODE) qDebug() << QWidget::mapFromGlobal(ui->mapImage->cursor().pos()) << QWidget::mapFromParent(devicePointers[index]->cursor().pos());
        _setPointerCoords(index,
                          (QWidget::mapFromGlobal(ui->mapImage->cursor().pos())).x() - devicePointers[index]->width()/2,
                          (QWidget::mapFromGlobal(ui->mapImage->cursor().pos())).y() - devicePointers[index]->height()/2);
    }
}
void UI::rejectDeviceEdit() {
    //delete devEdit;
    //delete dev;
    dev->reject();
    if (DEBUG_MODE) qDebug() << "ЗАКРЫТИЕ ОКНА РЕДАКТИРОВАНИЯ: ОКНО ЗАКРЫТО";
}

//Сохранить изменения в таблицу и поменять данные
void UI::acceptDeviceEdit() {
    quint16 index = devEdit->index->value();
    if (deviceArray.contains(index)) {
        trayMessage("Изменение параметров НС: ", "Параметры изменены",6);
        //Если такое устройство есть
        //меняем данные в массиве
        deviceArray[index].name = devEdit->name->text();
        deviceArray[index].alw = devEdit->alw->text();
        deviceArray[index].aly = devEdit->aly->text();
        deviceArray[index].alx = devEdit->alx->text();
        deviceArray[index].pumps = devEdit->pumps->value();
        if (DEBUG_MODE) qDebug() << "СОХРАНЕНИЕ НАСТРОЕК НС: ИЗМЕНЕНИЯ ЗАПИСАНЫ!!!";
        //меняем данные в списке
        deviceForms[index]->name->setText(devEdit->name->text());
        //Удаляем форму, создаем новую
        _setDeviceWindowStatus(index,deviceArray[index].stat);
    } else {
        if (DEBUG_MODE) qDebug() << "СОХРАНЕНИЕ НАСТРОЕК НС: НЕТ УСТРОЙСТВА С ТАКИМ ИНДЕКСОМ";
    };
    delete devEdit;
}

void UI::deleteDevice(quint16 index) {
    delDevDial * DelDevDial = new delDevDial(this);
    if (DelDevDial->exec() == QDialog::Accepted)
    {
        if (deviceArray.contains(index)) {
            if (deviceArray[index].type == "modbus") emit RemoveDevice(index);
            else if (deviceArray[index].type == "tcp") {
                delete tcpDev[index];
                tcpDev.remove(index);
            }
            deviceArray.remove(index);
            delete deviceForms[index]; deviceForms.remove(index);
            delete deviceWindows[index]; deviceWindows.remove(index);
            delete devicePointers[index]; devicePointers.remove(index);
            delete deviceBuses[index]; deviceBuses.remove(index);
            minIndex = 3000;
            maxIndex = 0;
            foreach(quint16 i,deviceArray.keys()) {
                if (i < minIndex) minIndex = i;
                if (i > maxIndex) maxIndex = i;
            }
            _sortDeviceList();
            _sortBuseList();
            trayMessage("Удаление НС", "Удалено НС №" + QString::number(index),6);
        };
    };
    delete DelDevDial;
    rejectDeviceEdit();
}

void UI::deleteDevice() {
    quint16 index = devEdit->index->value();
    delDevDial * DelDevDial = new delDevDial(this);
    if (DelDevDial->exec() == QDialog::Accepted)
    {
        if (deviceArray.contains(index)) {
            deviceArray.remove(index);
            delete deviceForms[index]; deviceForms.remove(index);
            delete deviceWindows[index]; deviceWindows.remove(index);
            delete devicePointers[index]; devicePointers.remove(index);
            delete deviceBuses[index]; deviceBuses.remove(index);
            minIndex = 3000;
            maxIndex = 0;
            foreach(quint16 i,deviceArray.keys()) {
                if (i < minIndex) minIndex = i;
                if (i > maxIndex) maxIndex = i;
            }
            _sortDeviceList();
            _sortBuseList();
            trayMessage("Удаление НС", "Удалено НС №" + QString::number(index),6);
        };
    };
    delete DelDevDial;
    rejectDeviceEdit();
}

//Вывести сообщение об ошибке
void UI::errorDeviceEdit(QString err) {
    if (DEBUG_MODE) qDebug() << "НЕКОРРЕКТНЫЕ ВВОД ДАННЫХ: " << err;
    trayMessage("Ошибка!",err,10);
}

//Обработка смены индекса в окне редатирования НС
void UI::deviceEditIndexChanged(int index) {
    if (DEBUG_MODE) qDebug() << "СМЕНА ИНДЕКСА: БУДЕТ ОСУЩЕСТВЛЕНА ПРОВЕРКА!";
    //Если индекс уже есть среди элементов
    if (deviceArray.contains(index)) {
        //Ищем наибольший
        quint16 maxElem = 0;
        foreach(quint16 i, deviceArray.keys()) if (i >= maxElem) maxElem = i+1;
        //Ставим его в поле индекса в форме
        dev->index->setValue(maxElem);
        if (DEBUG_MODE) qDebug() << "СМЕНА ИНДЕКСА: ТАКОЙ ИНДЕКС УЖЕ ЕСТЬ В СИСТЕМЕ";
    };
}

//Форма добавления новой станции
void UI::addNewStationForm() {
//    devEdit = new deviceEdit(this);
//    connect(devEdit,SIGNAL(accepted()),this,SLOT(addNewStation()));
//    connect(devEdit,SIGNAL(rejected()),this,SLOT(rejectDeviceEdit()));
//    connect(devEdit,SIGNAL(Error(QString)),this,SLOT(errorDeviceEdit(QString)));
//    connect(devEdit->index,SIGNAL(valueChanged(int)),this,SLOT(deviceEditIndexChanged(quint16)));
//    //Заполняем имя
//    devEdit->setWindowTitle("Создать НС");
//    //заполняем индекс
//    quint16 maxElem = 0;
//    foreach(quint16 i, deviceArray.keys()) if (i >= maxElem) maxElem = i+1;
//    devEdit->index->setValue(maxElem);
//    devEdit->index->setDisabled(false);
//    devEdit->num1->setDisabled(false);
//    devEdit->num2->setDisabled(false);
//    devEdit->delButton->setVisible(false);
//    devEdit->gsmWidget->setVisible(true);
//    devEdit->tcpWidget->setVisible(false);
//    devEdit->mdbWidget->setVisible(false);
//    //заполняем насосы
//    devEdit->pumps->setValue(6);
//    //если прибор gsm
//    devEdit->setModal(true);
//    devEdit->show();

    dev = new deviceDialog(this);
    dev->del->setVisible(false);
    dev->index->setValue(maxIndex + 1);
    connect(dev->index,SIGNAL(valueChanged(int)),this,SLOT(deviceEditIndexChanged(int)));
    connect(this,SIGNAL(showHelp(QString)),dev,SLOT(showHelp(QString)));
    dev->showHelp("gsm");
    if (dev->exec() == QDialog::Accepted) {
        deviceStruct d;
        if (dev->type->currentIndex() == 0) d.type = "gsm";
        else if (dev->type->currentIndex() == 1) d.type = "modbus";
        else if (dev->type->currentIndex() == 2) d.type = "tcp";
        d.index = dev->index->value();
        d.name = dev->name->text();
        d.view = dev->viewType();
        d.flow = dev->flow->value();
        if (maxIndex < d.index) maxIndex = d.index;
        if (minIndex > d.index) minIndex = d.index;
        d.alw = dev->alw->text();
        d.aly = dev->aly->text();
        d.alx = dev->alx->text();
        d.map = 0;
        d.period = 1440;
        d.x = 0.5;
        d.y = 0.5;
        d.pumps = dev->pumps->value();
        if (d.type == "gsm") {
            d.address = "7" + dev->gsmNum->text();
        } else if (d.type == "tcp") {
            d.address = dev->tcpNum->text();
            d.mdbAddr = dev->tcpAddr->value();
            d.mdbPage = 0;
        } else if (d.type == "modbus") {
            d.address = dev->mdbPort->currentText();
            d.mdbAddr = dev->mdbAddr->value();
            d.mdbPage = 0;
        };
        bool exist = false;
        foreach(deviceStruct ds, deviceArray) {
            if (d.type == "gsm") {
                if (d.address == ds.address) exist = true;
            } else if (d.type == "tcp") {
                if (d.address == ds.address) exist = true;
            } else if (d.type == "modbus") {
                if (d.address == ds.address && d.mdbAddr == ds.mdbAddr && d.mdbPage == ds.mdbPage) exist = true;
            };
        }

        if (exist) {trayMessage("Невозможно добавить НС!","Прибор с таким адресом уже есть в системе!",14); return;};
        d.stat = StringToStat(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss") + QString(" *ALARM* *TIMEOUT* *END*"));
                deviceArray.insert(d.index,d);
                _addDevice(d.index);

                _sortDeviceList();
                _sortBuseList();
    };
    delete dev;
}

//Добавить новую станцию
void UI::addNewStation() {
    QString address(""),type("");
    if (dev->type->currentIndex() == 0) type = "gsm";
    else if (dev->type->currentIndex() == 1) type = "modbus";
    else if (dev->type->currentIndex() == 2) type = "tcp";


    if (type == "gsm") {
        address.append("7").append(devEdit->num1->text()).append(devEdit->num2->text());
    } else if (type == "tcp") {
        address.append(devEdit->num->text());
    } else if (type == "modbus") {
        address.append(devEdit->mdbPort->currentText()).append("-").append(QString::number(devEdit->mdbAddr->value()));
    };

    foreach(quint16 i,deviceArray.keys()) if (deviceArray[i].address == address) {
        trayMessage("Ошибка добавление НС","НС с таким адресом уже есть в системе. Если требуется добавить НС с другим адресом, измените этот параметр.",15);
        return;
    };

    quint16 index = devEdit->index->value();
    //Добавляем устройство
    deviceStruct device;
    //меняем данные в массиве
    device.address = address;
    device.name = devEdit->name->text();
    device.index = index;
    if (maxIndex < index) maxIndex = index;
    if (minIndex > index) minIndex = index;
    device.alw = devEdit->alw->text();
    device.aly = devEdit->aly->text();
    device.alx = devEdit->alx->text();
    device.pumps = devEdit->pumps->value();
    if (dev->type->currentIndex() == 0) device.type = "gsm";
    else if (dev->type->currentIndex() == 1) device.type = "modbus";
    else if (dev->type->currentIndex() == 2) device.type = "tcp";
    device.map = 0;
    device.period = 1440;
    device.x = 0.5;
    device.y = 0.5;

    device.stat = StringToStat(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss") + QString(" *ALARM* *TIMEOUT* *END*"));
    deviceArray.insert(index,device);
    _addDevice(index);

    _sortDeviceList();
    _sortBuseList();

    if (DEBUG_MODE) qDebug() << "СОЗДАНИЕ НС: НАСОСНАЯ СТАНЦИЯ СОЗДАНА";
    trayMessage("Создание НС: ", "Создана НС номер " + QString::number(index),6);
    delete devEdit;
}

//Поменять время в списке
void UI::changeTimeInList() {
    foreach(quint16 i,deviceForms.keys()) {
        _setDeviceFormData(i);
        if (deviceArray[i].type == "tcp" || deviceArray[i].type == "modbus") {
            bool work = false;
            for (int j=0;j<6;j++) if (deviceArray[i].stat.run[j]) work = true;
            if (work)deviceArray[i].working++;
        }
    }
}

//Смена списков
void UI::changeListView() {
    if (deviceList->isVisible() && !(deviceList2->isVisible())) {
        deviceList->setVisible(false);
        deviceList2->setVisible(true);
        ui->scroll->setEnabled(false);
        //ui->changeList->setText("2->1");
        ui->changeSortType->setDisabled(true);
    } else {
        deviceList->setVisible(true);
        scrollList(1);
        deviceList2->setVisible(false);
        //ui->changeList->setText("1->2");
        ui->scroll->setEnabled(true);
        ui->changeSortType->setDisabled(false);
    };
    trayMessage("Выбор списка отображения","",6);
}

void UI::turnHistory(quint16 index) {
    if (deviceWindows.contains(index) && deviceArray.contains(index)) {
        if (deviceWindows[index]->history->isVisible()) {
            deviceWindows[index]->resize(500,370);
            deviceWindows[index]->setFixedSize(500,370);
            deviceWindows[index]->history->setVisible(false);
            deviceWindows[index]->turnHistory->setEnabled(true);
            deviceWindows[index]->turnHistory2->setEnabled(false);
            deviceWindows[index]->onlyAlarm->setChecked(false);
            _setDeviceWindowStatus(index,deviceArray[index].stat);
        } else {
            deviceWindows[index]->resize(710,370);
            deviceWindows[index]->setFixedSize(710,370);
            deviceWindows[index]->history->setVisible(true);
            deviceWindows[index]->turnHistory2->setEnabled(true);
            deviceWindows[index]->turnHistory->setEnabled(false);
            showFullHistory(index,15);
        };
        //_setDeviceWindowStatus(index,deviceArray[index].stat);
    };
}

void UI::showHistory() {

}

void UI::hideHistory() {

}
//Показать поле истории
void UI::showFullHistory(quint16 index,qint16 count) {
    _loadFullHistory(index,count);
    trayMessage("Запрос истории: ", "История НС номер " + QString::number(index),6);
    if (DEBUG_MODE) qDebug() << "БУДЕТ ПОКАЗАНА ИСТОРИЯ ДЛЯ НС " << index;
}

//Выдать статистику
void UI::showHistoryStat(quint16 index) {
    if (deviceWindows.contains(index)) {
        status devSt = StringToStat(deviceWindows[index]->historyList->currentItem()->text());
        _setDeviceWindowStatus(index,devSt);
    }
}

void UI::closeEvent(QCloseEvent *ev) {
    writeConStat();
    if (!reallyClose) {
        ev->ignore();
        hide();
        closeWindowAction->setDisabled(true);
        showWindowAction->setDisabled(false);
    } else {
        ev->accept();
        exit(0);
    };
}

void UI::closeWindowSlot() {
    reallyClose = false;
    close();
}

void UI::closeProgramSlot() {
    reallyClose = true;
    close();
}

void UI::showWindowSlot() {
    closeWindowAction->setDisabled(false);
    showWindowAction->setDisabled(true);
    show();
}

void UI::reloadModemSlot() {
    tray->showMessage("GSM-модем","Модем перезагружается");
    portQueryTimer.stop();
    _portStart();
    portQueryTimer.start();
    reloadFlag = true;
}

void UI::trayMessage(QString header, QString text, int type) {
    if (type & 1) tray->showMessage(header,text);
    if (type & 2) systemLog(header,text);
    if (type & 4) actionLog(header,text);
    if (type & 8) QMessageBox::warning(0,header,text);
}

void UI::exitEditMode() {
    trayMessage("Выход из режима редатирования", "",6);
    quint16 i;
    //foreach(ModbusTCP * mdb, tcpPort.MDBTCP) mdb->disconnect();
    //tcpPort.MDBTCP.clear();
    while (tcpDev.count()) {
        delete tcpDev.last();
    };
    foreach(i,deviceForms.keys()) delete deviceForms[i]; deviceForms.clear();
    foreach(i,deviceBuses.keys()) delete deviceBuses[i]; deviceBuses.clear();
    foreach(i,devicePointers.keys()) delete devicePointers[i]; devicePointers.clear();
    foreach(i,deviceWindows.keys()) {deviceWindows[i]->close(); delete deviceWindows[i];}; deviceWindows.clear();
    deviceArray.clear();

    if (DEBUG_MODE) qDebug() << "ВЫХОД ИЗ РЕЖИМА РЕДАКТИРОВАНИЯ И ЗАГРУЗКА НОВЫХ НАСТРОЕК";
    enableSending = false;
    maxIndex = 0;
    minIndex = 63000;
    loadSettings(); //Загружаем настройки
    loadDevices(); //Загружаем устройства
    _setChangingsEnabled(false); //Закрываем настройки от редактирования
    _initPort(); //Инициализируем настройки прта для GSM
    _loadMap(0);
    _portStart(); //Это GSM
    enableChangings = false;
    editPanel->setVisible(false);

    if (DEBUG_MODE) qDebug() << "ВЫХОД ИЗ РЕЖИМА РЕДАКТИРОВАНИЯ: ДОБАВЛЕНО УСТРОЙСТВ: " << deviceArray.count();
    if (DEBUG_MODE) qDebug() << "ВЫХОД ИЗ РЕЖИМА РЕДАКТИРОВАНИЯ: ДОБАВЛЕНО ЭЛЕМЕНТОВ В СПИСОК: " << deviceForms.count();
    if (DEBUG_MODE) qDebug() << "ВЫХОД ИЗ РЕЖИМА РЕДАКТИРОВАНИЯ: ДОБАВЛЕНО ОКОН НС: " << deviceWindows.count();
    if (DEBUG_MODE) qDebug() << "ВЫХОД ИЗ РЕЖИМА РЕДАКТИРОВАНИЯ: ДОБАВЛЕНО УКАЗАТЕЛЕЙ: " << devicePointers.count();
    closeExitSettingsDialog();
}

void UI::closeExitSettingsDialog() {
    if (DEBUG_MODE) qDebug() << "ЗАКРЫТИЕ ДИАЛОГА СОХРАНЕНИЯ НАСТРОЕК";
    exitingDialog->disconnect();
    exitingDialog->close();
    delete exitingDialog;
}

void UI::saveNewSettings() {
    if (DEBUG_MODE) qDebug() << "СОХРАНЕНИЕ НАСТРОЕК";
    saveSettings();
    exitEditMode();
    writeConStat();
}

void UI::showExitSettingsDialog() {
    if (DEBUG_MODE) qDebug() << "СОЗДАНИЕ ДИАЛОГА СОХРАНЕНИЯ НАСТРОЕК";
    exitingDialog = new exitEditModeDialog(this);
    exitingDialog->setModal(true);
    exitingDialog->show();
    connect(exitingDialog,SIGNAL(ok()),this,SLOT(saveNewSettings()));
    connect(exitingDialog,SIGNAL(no()),this,SLOT(exitEditMode()));
    connect(exitingDialog,SIGNAL(cancel()),this,SLOT(closeExitSettingsDialog()));
}

void UI::showEditDialog() {
    EditDialog = new editDialog(this);
    EditDialog->setModal(true);
    connect(EditDialog,SIGNAL(no()),this,SLOT(closeEditDialog()));
}

void UI::closeEditDialog() {
    while(EditDialog->list->count() > 0) delete EditDialog->list->item(0);
    EditDialog->disconnect();
    EditDialog->close();
    delete EditDialog;
}

void UI::addMapPressed() {
    mapCount++;
    maps.append("default.jpg");
    _loadMap(mapCount-1);
    editMapPressed();
    trayMessage("Добавлена карта", "",6);
}

void UI::editMapPressed() {
    showEditDialog();
    QStringList lstFiles = QDir(QDir().currentPath() + "/maps/").entryList(QDir::Files);
    foreach(QString s, lstFiles) EditDialog->list->addItem(new QListWidgetItem(s));
    if (EditDialog->list->count() > 0) EditDialog->list->setCurrentRow(0);
    EditDialog->show();
    EditDialog->setWindowTitle("Выберите изображение");
    connect(EditDialog,SIGNAL(ok()),this,SLOT(editMapOk()));
}

void UI::editMapOk() {
    maps[currentMap] = EditDialog->list->currentItem()->text();
    closeEditDialog();
    _loadMap(currentMap);
    trayMessage("Изменение изображения карты: ", "Изменено изображение карты " + QString::number(currentMap+1),6);
}

void UI::deleteMapPressed() {
    if (mapCount > 1) {
        mapCount--;
        foreach(quint16 ind, devicePointers.keys()) if (deviceArray[ind].map == currentMap + 1) {deviceArray[ind].map = 0; deviceForms[ind]->map->setText("-");};
        maps.removeAt(currentMap);
        if (currentMap != 0) _loadMap(currentMap - 1); else _loadMap(0);
        trayMessage("Карта удалена","",6);
    } else {
        trayMessage("Ошибка","невозможно удалить единственную карту");
    }
}

void UI::addDeviceToCurrentMapPressed() {
    showEditDialog();
    foreach(quint16 i, deviceArray.keys()) {
        if (deviceArray[i].map != currentMap + 1) {
            EditDialog->list->addItem(new QListWidgetItem(deviceArray[i].name + " (" + QString::number(i) + ")"));
            EditDialog->list->item(EditDialog->list->count() - 1)->setData(0x0100,i);
        };
    };
    if (EditDialog->list->count() > 0) EditDialog->list->setCurrentRow(0);
    EditDialog->show();
    EditDialog->setWindowTitle("Перенести НС");
    connect(EditDialog,SIGNAL(ok()),this,SLOT(addDeviceToCurrentMapOk()));
}

void UI::addDeviceToCurrentMapOk() {
    trayMessage("Устройство перенесено на карту","",6);
    quint16 index = EditDialog->list->currentItem()->data(0x0100).toInt();
    if (deviceArray.contains(index)) {
        deviceArray[index].map = currentMap + 1;
        deviceForms[index]->map->setText("Карта " + QString::number(index));
    };
    _loadMap(currentMap);
    _sortBuseList();
    _sortDeviceList();
    closeEditDialog();
}

void UI::removeDeviceFromCurrentMapPressed() {
    showEditDialog();
    foreach(quint16 i, deviceArray.keys()) {
        if (deviceArray[i].map == currentMap + 1) {
            EditDialog->list->addItem(new QListWidgetItem(deviceArray[i].name + " (" + QString::number(i) + ")"));
            EditDialog->list->item(EditDialog->list->count() - 1)->setData(0x0100,i);
        };
    };
    if (EditDialog->list->count() > 0) EditDialog->list->setCurrentRow(0);
    EditDialog->show();
    EditDialog->setWindowTitle("Перенести НС");
    connect(EditDialog,SIGNAL(ok()),this,SLOT(removeDeviceFromCurrentMapOk()));
}

void UI::removeDeviceFromCurrentMapOk() {
    trayMessage("Устройство  удалено с карты","",6);
    quint16 index = EditDialog->list->currentItem()->data(0x0100).toInt();
    if (deviceArray.contains(index)) {
        deviceArray[index].map = 0;
        deviceForms[index]->map->setText("-");
    };
    _loadMap(currentMap);
    _sortBuseList();
    _sortDeviceList();
    closeEditDialog();
}

void UI::savePassword() {
    QString oldp = editPasswordWindow->old->text();
    QString newp1 = editPasswordWindow->new1->text();
    QString newp2 = editPasswordWindow->new2->text();
    if (oldp == passwordWindow->pass || oldp == QDate::currentDate().toString("yyyyMMdd")) {
        if (newp1 == newp2) {
            if (newp1.count() == 0) {
                trayMessage("Ошибка","Пароль не может быть пустым");
            } else {
                _savePass(newp1);
                //trayMessage("Сохранение пароля","Новый пароль успешно сохранен");
                closeEditPasswordWindow();
            };
        } else {
            trayMessage("Ошибка","Пароли не совпадают");
        };
    } else {
        trayMessage("Ошибка","Неправильный старый пароль");
    };
}

void UI::changePassword() {
    showEditPasswordWindow();
    editPasswordWindow->show();
}

//Открываем доступ к редактированию
void UI::showChangings() {
    showPasswordWindow();
    connect(passwordWindow,SIGNAL(ok()),this,SLOT(goToEditMode()));
    connect(passwordWindow,SIGNAL(no()),this,SLOT(closePasswordWindow()));
}


void UI::showPasswordWindow() {
    passwordWindow->show();
}

void UI::goToEditMode() {
    //Вызываем команду изменения настроек
    _setChangingsEnabled(true);
    closePasswordWindow();
    trayMessage("Включен режим редактирования","",6);
}

void UI::closePasswordWindow() {
    passwordWindow->hide();
    passwordWindow->disconnect();
}

void UI::showEditPasswordWindow() {
    editPasswordWindow = new editPassword(this);
    connect(editPasswordWindow,SIGNAL(no()),this,SLOT(closeEditPasswordWindow()));
    connect(editPasswordWindow,SIGNAL(ok()),this,SLOT(savePassword()));
}

void UI::closeEditPasswordWindow() {
    editPasswordWindow->reallyClose = true;
    editPasswordWindow->close();
    editPasswordWindow->disconnect();
    delete editPasswordWindow;
}

void UI::closeDelDial() {
    deleteDialog->close();
    deleteDialog->disconnect();
    delete deleteDialog;
}

void UI::deleteDeviceFromMap(quint16 index) {
    if (DEBUG_MODE) qDebug() << "УДАЛЕНИЕ НС С КАРТЫ: УДАЛЯЕТСЯ НС № " << index;
    if (deviceArray.contains(index)) {
        deviceArray[index].map = 0;
        deviceForms[index]->map->setText("-");
        _loadMap(currentMap);
    };
    closeDelDial();
}

void UI::deviceFormMovedToMap(quint16 index) {
    if (deviceArray.contains(index) && enableChangings) {
        deviceForms[index]->setParent(this);
        deviceForms[index]->show();
        deviceForms[index]->setCursor(QCursor(Qt::ClosedHandCursor));
        deviceForms[index]->move(QWidget::mapFromGlobal(cursor().pos()).x() - deviceForms[index]->width()/2,
                                 QWidget::mapFromGlobal(cursor().pos()).y() - deviceForms[index]->height()/2);
    };
}

void UI::deviceBuseMovedToMap(quint16 index) {
    if (deviceArray.contains(index) && enableChangings) {
        deviceBuses[index]->setParent(this);
        deviceBuses[index]->show();
        deviceBuses[index]->setCursor(QCursor(Qt::ClosedHandCursor));
        deviceBuses[index]->move(QWidget::mapFromGlobal(cursor().pos()).x() - deviceBuses[index]->width()/2,
                                 QWidget::mapFromGlobal(cursor().pos()).y() - deviceBuses[index]->height()/2);
    };
}

void UI::deviceFormReleasedToMap(quint16 index) {
    trayMessage("Устройство перенесено на карту","",6);
    if (deviceArray.contains(index)  && enableChangings) {
        if (deviceForms[index]->pos().x() > ui->mapImage->pos().x()
            && deviceForms[index]->pos().x() + deviceForms[index]->width() < ui->mapImage->pos().x() + ui->mapImage->width()
            && deviceForms[index]->pos().y() > ui->mapImage->pos().y()
            && deviceForms[index]->pos().y() + deviceForms[index]->height() < ui->mapImage->pos().y() + ui->mapImage->height()) {
            deviceArray[index].map = currentMap + 1;
            _setPointerCoords(index,
                              QWidget::mapFromGlobal(cursor().pos()).x() - deviceForms[index]->width()/2,
                              QWidget::mapFromGlobal(cursor().pos()).y() - deviceForms[index]->height()/2);
            deviceForms[index]->map->setText("Карта " + QString::number(currentMap + 1));
            _loadMap(currentMap);
        };
        deviceForms[index]->setParent(deviceList);
        deviceForms[index]->show();
        deviceForms[index]->setCursor(QCursor(Qt::PointingHandCursor));
        _sortDeviceList();
    }
}

void UI::deviceBuseReleasedToMap(quint16 index) {
    trayMessage("Устройство перенесено на карту","",6);
    if (deviceArray.contains(index)  && enableChangings) {
        if (deviceBuses[index]->pos().x() > ui->mapImage->pos().x()
            && deviceBuses[index]->pos().x() + deviceBuses[index]->width() < ui->mapImage->pos().x() + ui->mapImage->width()
            && deviceBuses[index]->pos().y() > ui->mapImage->pos().y()
            && deviceBuses[index]->pos().y() + deviceBuses[index]->height() < ui->mapImage->pos().y() + ui->mapImage->height()) {
            deviceArray[index].map = currentMap + 1;
            _setPointerCoords(index,
                              QWidget::mapFromGlobal(cursor().pos()).x() - deviceBuses[index]->width()/2,
                              QWidget::mapFromGlobal(cursor().pos()).y() - deviceBuses[index]->height()/2);
            _loadMap(currentMap);
        };
        deviceBuses[index]->setParent(deviceList2);
        deviceBuses[index]->show();
        deviceBuses[index]->setCursor(QCursor(Qt::PointingHandCursor));
        _sortDeviceList();
        _sortBuseList();
    }
}

void UI::fullScreenMode(bool on) {
    if (on) {
        showFullScreen();
        trayMessage("Полноэкранный режим ","Включен",6);
    } else {
        showNormal();
        resize(1015,700);
        trayMessage("Полноэкранный режим ","Выключен",6);
    };
    _saveFullScreen(on);
    ensurePolished();
    scrollList(0);
    resizeAll();
}

void UI::resizeAll() {
    currentMapImage->resize(size().width() - 215, size().height() - 100);
    currentMapImage->ensurePolished();
    _loadMap(currentMap);
    ui->gsmBox->move(3,size().height() - 95);
    ui->tcpBox->move(240,size().height() - 95);
    ui->modbusBox->move(325,size().height() - 95);
    ui->mapButtons->move(size().width() - 165,size().height() - 100);
    ui->dList->resize(200,size().height() - 100);
    _sortBuseList();
    _sortDeviceList();
    ui->mapNumLabel->move((size().width() - 200) / 2 + 110,size().height() - 90);
    if (DEBUG_MODE) qDebug() << "Положение названия карт: " << ui->mapNumLabel->pos().x();
    ui->changeList->move(20,ui->dList->size().height()-40);
    ui->changeSortType->move(130,ui->dList->size().height()-40);
    ui->scroll->resize(ui->scroll->width(),ui->dList->height());
    ui->editPanel->move(size().width() - 50, 0);
    foreach(quint16 i, deviceArray.keys()) _movePointerTo(i,deviceArray[i].x,deviceArray[i].y);
}

void UI::showAbout() {
    About = new about(this);
    About->show();
    About->setVersion(currentVersion);
    connect(About,SIGNAL(closed()),this,SLOT(closeAbout()));
}

void UI::closeAbout() {
    delete About;
}

void UI::sendingTimerChanged() {
    sendingValue = !sendingValue;
    foreach (deviceWindow * window, deviceWindows) {
        if (window->waiting) {
            window->statButton->setIcon((sendingValue) ? QIcon(":/images/refreshOr.png") : QIcon(":/images/refresh.png"));
        } else {
            window->statButton->setIcon(QIcon(":/images/refresh.png"));
        };
    }
    foreach( deviceform * form, deviceForms) {
        if (form->warning) {
            form->status->setVisible(sendingValue);
        } else {
            form->status->setVisible(true);
        };
    }
}

void UI::setAvailablePorts() {
    foreach(QSerialPortInfo info, QSerialPortInfo::availablePorts()) {
        portActions.append(new QAction(info.portName(),this));
        if (DEBUG_MODE) qDebug() << "СОЗДАНИЕ СПИСКА ПОРТОВ: СОЗДАЕТСЯ ПОРТ " << info.portName();
        portActions.last()->setCheckable(true);
        portActions.last()->setObjectName(info.portName());
        if (info.portName() == Port.portName()) portActions.last()->setChecked(true);
        connect(portActions.last(),SIGNAL(triggered(bool)),this,SLOT(chooseAvailablePort()));
    };
    ui->portMenu->addActions(portActions);
}

void UI::chooseAvailablePort() {
    if (DEBUG_MODE) qDebug() << "СМЕНА ПОРТА: ВЫБРАН ПОРТ " << sender()->objectName();
    foreach(QAction * action, portActions) {
        action->setChecked(action->objectName() == sender()->objectName());
    };
    portQueryTimer.stop();
    Port.close();
    Port.setPortName(sender()->objectName());
    if (Port.open(QIODevice::ReadWrite)) {
        trayMessage("Порт GSM-модема изменен","Подключено к " + sender()->objectName(),3);
        _setPortSettings("");
        portQueryTimer.start();
        _setDefPort(sender()->objectName());
    } else {
        trayMessage("Ошибка подключения","Порт" + sender()->objectName() + " недоступен. Попробуйте другой порт",3);
    };
}

void UI::openHelp() {
    QDesktopServices::openUrl(QUrl("file:///" + QDir().currentPath() + "/documentation/arm-sk-doc-1-2.pdf", QUrl::TolerantMode));
}

void UI::crashMemory() {
    try {
        deviceWindows[10000]->Show();
    } catch(int err) {
        QMessageBox::warning(this,"foooo","foofooo");
        exit(66);
    };
}

void UI::setTcpStat() {
    bool timeout = true;
    foreach(quint16 index,deviceArray.keys()) if (deviceArray[index].type == "tcp" & deviceArray[index].stat.stat != "timeout") timeout = false;
    if (timeout) {
        ui->ipCon->setPixmap(QPixmap(":/images/ipConError.png"));
        ui->ipLabel->setText("Нет\nсоединения");
    } else {
        ui->ipCon->setPixmap(QPixmap(":/images/ipCon.png"));
        ui->ipLabel->setText("Соединение\nустановлено");
    }
}

void UI::setMdbStat() {
    bool timeout = true;
    foreach(quint16 index,deviceArray.keys()) if (deviceArray[index].type == "modbus" & deviceArray[index].stat.stat != "timeout") timeout = false;
    if (timeout) {
        ui->mdbIcon->setPixmap(QPixmap(":/images/ipConError.png"));
        ui->mdbLabel->setText("Нет\nсоединения");
    } else {
        ui->mdbIcon->setPixmap(QPixmap(":/images/ipCon.png"));
        ui->mdbLabel->setText("Соединение\nустановлено");
    }
}

void UI::changeSortType(bool ch) {
    sortByStatus = ch;
    if (ch) {
        ui->changeSortType->setToolTip("Выключить сортировку по статусу");
    } else {
        ui->changeSortType->setToolTip("Включить сортировку по статусу");
    };
    _sortDeviceList();
}

void UI::renderMdbResponse(quint16 index, QString answer) {
    int i,j; bool correct = false;
    foreach (j,deviceArray.keys()) if (j == index && deviceArray[j].type == "modbus") { i = j; correct = true; break;};

    //если он есть среди НС
    if (correct) {
        //Отключаем мигание
        deviceArray[i].packsAll++;
    //Если он корректен
        status deviceStat = StringToStat(QString(answer.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
        if (deviceStat.correct) {
            deviceArray[i].tcpLastReport = QDateTime().currentDateTime();
            _stopWaiting(i);
            _setPointerStatus(i,deviceStat.stat);
            if (deviceWindows.contains(i))
                if (!deviceWindows[i]->history->isVisible()) _setDeviceWindowStatus(i,deviceStat);
            //if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС КОРРЕКТЕН";

            if (needToSave(i,deviceStat)) {
                deviceArray[i].outCurrentDate = QDateTime().currentDateTime();
                if (statArray.contains(deviceStat.stat)) trayMessage(deviceArray[i].name,statArray[deviceStat.stat]);
                //Проигрываем зву сообщения
                if (deviceStat.fullText.contains("*ALARM*")) {
                    //PlaySoundA(QString(MAIN_PATH + "sound/alarm.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                    QSound::play(alarmSound);
                } else {
                    //PlaySoundA(QString(MAIN_PATH + "sound/message.wav").toUtf8().data(), NULL, SND_FILENAME|SND_ASYNC);
                    QSound::play(messageSound);
                };
                _saveDeviceStat(i,QString(answer.toUtf8()).prepend(" ").prepend(QDateTime().currentDateTime().toString("yyyy/MM/dd hh:mm:ss")));
                if(deviceForms.contains(i)) deviceForms[i]->warning = (deviceStat.stat == "alarm");
            };
            deviceArray[i].stat = deviceStat;
            _setBuseStat(i);
            _setDeviceFormData(i);
            _sortDeviceList();
            _sortBuseList();
        } else {
            if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: СТАТУС НЕВЕРНЫЙ!!!";
        };
    //Если время
    } else {
        if (DEBUG_MODE) qDebug() << "ОБРАБОТКА ВХОДЯЩЕГО СООБЩЕНИЯ: НЕТ УСТРОЙСТВА С НОМЕРОМ " << index;
    };
}

void UI::mdbTimerCycle() {
//    foreach (quint16 index,deviceArray.keys()) {
//        if (deviceArray[index].type == "modbus") {
//            if (deviceArray[index].tcpLastReport.addSecs(mdbWaiting) < QDateTime::currentDateTime()) {
//                renderMdbResponse(index,"*ALARM* *TIMEOUT* *END*");
//            };
//        }
//    }

    setMdbStat();
}

void UI::mdbNoSignal(quint16 index, quint8 err) {
    if (err == 1) {
        deviceArray[index].packsNull++;
    } else if (err == 2) {
        deviceArray[index].packsBad++;
    };
    if (DEBUG_MODE) qDebug() << "Нет сигнала от" << index;
    renderMdbResponse(index,"*ALARM* *TIMEOUT* *END*");
}

void UI::showWindowInfo(quint16 index) {
    if (deviceArray.contains(index)) {
        quint64 badPacks = (deviceArray[index].type == "modbus") ? (deviceArray[index].packsBad + deviceArray[index].packsNull) : (deviceArray[index].packsBad);
        QString lev1;
        if (deviceArray[index].packsAll > 0) {
            lev1.setNum((1 - (double)(badPacks / deviceArray[index].packsAll)) * 100); lev1.append("%");
        } else {
            lev1.append("Нет данных");
        };
        if (deviceArray[index].type == "gsm") {
        }   else if (deviceArray[index].type == "tcp") {
        }   else if (deviceArray[index].type == "modbus") {
        };
        statInfo * info = new statInfo(deviceWindows[index]);
        info->lab->setText(lev1);
        if (info->exec() == QDialog::Rejected) {

        };
        delete info;
    }
}

void UI::parseTCPError(QString ip) {
    foreach(quint16 index,deviceArray.keys()) {
        if (deviceArray[index].type == "tcp" && deviceArray[index].address == ip) {deviceArray[index].packsAll++; deviceArray[index].packsBad++;};
    };
}

void UI::windowConTimer() {
    foreach(quint16 key,deviceArray.keys()) {
        if (deviceArray[key].type != "gsm" && deviceWindows[key]->conStat) {
            QString pixmap = (conCount) ? (":/images/winCon.png") : (":/images/winCon2.png");
            deviceWindows[key]->winCon->setPixmap(QPixmap(pixmap).scaled(70,30));
        };
    }
    conCount = !conCount;
}

void UI::deviceWindowMoved() {
    if (DEBUG_MODE) qDebug() << "ПЕРЕМЕЩЕНИЕ ОКНА: ОКНО ПЕРЕМЕЩАЕТСЯ";
}

void UI::showD(QString t) {
    //ui->testBrowser->append(t);
}

void UI::deviceWindowReleased() {
    if (DEBUG_MODE) qDebug() << "ПЕРЕМЕЩЕНИЕ ОКНА: КОНЕЦ ПЕРЕМЕЩЕНИЯ";
}

void UI::scrollList(int x) {
    double h = (double)x / 64;
    h = h * (deviceList->height() - ui->dList->height());
    //trayMessage(QString::number(h),QString::number(deviceList->height()) + QString::number(ui->dList->height()));
    if (deviceList->height() > ui->dList->height())
        deviceList->move(0,-h);
}

void UI::getFocus(QWidget *old, QWidget *now) {
    if (now && now->objectName().length()) emit showHelp(now->objectName());
}
void UI::addTcpDev(quint16 index, QString address, quint16 port, quint32 tcpPort) {
    tcpDev.insert(index,new TModbusTCPDevice(address,port,tcpPort,this));
    tcpDev[index]->setProperty("index",QVariant(index));
    qDebug() << "device added: " <<address;
    connect(tcpDev[index],SIGNAL(data(QByteArray)),this,SLOT(renderTcpNResponse(QByteArray)));
    connect(tcpDev[index],SIGNAL(error(QString)),this,SLOT(parseTCPError(QString)));
}

void UI::getDaily(quint16 id, QDate min, QDate max, QDate current) {
    if (deviceArray.contains(id) && min <= max) {
        qDebug() << "ЗАПРОС ИСТОРИИ ДЛЯ УСТРОЙСТВА N" << id << "c" << min << "по"  << max;
        _loadDailyStat(id,min,max,current);
    } else {
        qDebug() << "ЗАПРОС ИСТОРИИ: НЕТ УСТРОЙСТВА " << id;
        foreach(deviceStruct d,deviceArray) qDebug() << d.index;
    }
}
void UI::showConIcons() {
    ui->gsmBox->setVisible(gsmHere);
    ui->tcpBox->setVisible(tcpHere);
    ui->modbusBox->setVisible(mdbHere);
    qDebug() << "НАЛИЧИЕ СТАНЦИЙ" << gsmHere << tcpHere << mdbHere;
}

void UI::messageFromDownloader(QString h, QString t, int act) {
    trayMessage(h,t,act);
}

void UI::showChangesLog() {
    changesLog * log = new changesLog();
    log->append("Новая версия программы:" + currentVersion);
    QFile f(MAIN_PATH + "changes.log");
    if (f.exists() && f.open(QIODevice::ReadOnly)) {
        log->append(QString(f.readAll()));
        f.close();
    }
    if (log->exec() == QDialog::Rejected || log->exec() == QDialog::Accepted) {
    }
    delete log;
}

void UI::showBackup() {
    qDebug() << "BACKUP FUNCTION: started";
    QRegExp verNum("version\\s([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})");
    QRegExp updReg("updated\\sat\\s([0-9]{4}\/[0-9]{2}\/[0-9]{2}\\s[0-9]{2}:[0-9]{2}:[0-9]{2})");
    QRegExp delReg("deleted\\sat\\s([0-9]{4}\/[0-9]{2}\/[0-9]{2}\\s[0-9]{2}:[0-9]{2}:[0-9]{2})");
    QDir dir(MAIN_PATH + "_elder");
    qDebug() << "BACKUP FUNC: " << dir.path();
    qDebug() << "BACKUP FUNC: dir exists: " << dir.exists();
    if (!dir.exists()) return;
    backupWindow * win = new backupWindow(this);
    foreach(QString dirName, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        qDebug() << "BACKUP FUNC: dir " << dirName;
        QFile updF(dir.path() + "/" + dirName + "/" + "update.log");
        qDebug() << "BACKUP FUNC: update file:" << updF.fileName() << "exists:" << updF.exists();
        if (updF.exists() && updF.open(QIODevice::ReadOnly)) {
            QString t(updF.readAll());
            qDebug() << t.contains(verNum) << t.contains(updReg) << t.contains(delReg);
            if (t.contains(verNum) && t.contains(updReg) && t.contains(delReg)) {
                qDebug() << "BACKUP FUNC: item added with: " << verNum.cap(1) << updReg.cap(1) << delReg.cap(1);
                win->addItem(QString(verNum.cap(1) + " (" + updReg.cap(1) + " - " + delReg.cap(1) + ")"),verNum.cap(1));
            };
            updF.close();
        };

    }
    if (win->exec() == QDialog::Accepted) {
        qDebug() << "BACKUP FUNC: backup from " << win->value();
        backUp(win->value());
    }
    delete win;
}

bool UI::updateFileCorrect(QString fileName) {
    QFile f(fileName);
    if (!f.exists() || !f.open(QIODevice::ReadOnly)) return false;
    else {
        //i
    };
}

void UI::backUp(QString ver) {

    QProcess * proc = new QProcess(0);
    qDebug() << "backUP CALLED with " << ver;
    QStringList arguments;
    arguments << "backup" << ver << currentVersion << ver;
    proc->start(QDir().absolutePath() + "/proc.exe",arguments);
}

void UI::showBackupLog() {
    QMessageBox::information(this,"Восстановление версий","Версия " + currentVersion + "восстановлена.");
}
