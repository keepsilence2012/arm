#ifndef DOWNLOADWIDGET_H
#define DOWNLOADWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QString>
#include "askdialog.h"

namespace Ui {
class downloadWidget;
}

class downloadWidget : public QWidget
{
    Q_OBJECT

public:
    explicit downloadWidget(QWidget *parent = 0);
    ~downloadWidget();
    QString lateVer;
public slots:
    void setProgress(int per,int max);
    void okPressed();
    void showText(QString text);
    void reload();
    void blockRefresh(bool on);
    void setReady();
private:
    Ui::downloadWidget *ui;
signals:
    void newFileName(QString);
};

#endif // DOWNLOADWIDGET_H
