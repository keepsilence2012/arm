#include "deviceform.h"
#include "ui_deviceform.h"

deviceform::deviceform(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deviceForm)
{
    ui->setupUi(this);

    //Линковка указателей
    index = ui->index;
    date = ui->date;
    status = ui->status;
    name = ui->name;
    map = ui->map;
    pointer = ui->pointer;
    setCursor(QCursor(Qt::PointingHandCursor));

    //Сигнал тревоги п умолчанию выключен
    alarm = false;
    warning = false;
    moving = false;
}

void deviceform::enterEvent(QEvent *ev) {
    Q_UNUSED(ev);
    emit hovered(index->text().toUInt());
}

void deviceform::leaveEvent(QEvent *ev) {
    Q_UNUSED(ev);
    emit left(index->text().toUInt());
}

void deviceform::mouseDoubleClickEvent(QMouseEvent *ev) {
    Q_UNUSED(ev);
    emit dblclick(ui->index->text().toUInt());
}

deviceform::~deviceform()
{
    delete ui;
}

void deviceform::mouseMoveEvent(QMouseEvent *ev) {
    if (ev->buttons() & Qt::LeftButton) {
        emit moved(index->text().toUInt());
        moving = true;
    }
}

void deviceform::mouseReleaseEvent(QMouseEvent *ev) {
    setCursor(QCursor(Qt::PointingHandCursor));
    moving = false;
    emit released(index->text().toUInt());
}
