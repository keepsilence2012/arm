#include "devicewindow.h"
#include "ui_devicewindow.h"

deviceWindow::deviceWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deviceWindow)
{
    ui->setupUi(this);

    in.append(ui->in0);in.append(ui->in1);in.append(ui->in2);in.append(ui->in3);in.append(ui->in4);in.append(ui->in5);
    graphics = false;

    rdy.append(ui->rdy0);rdy.append(ui->rdy1);rdy.append(ui->rdy2);rdy.append(ui->rdy3);rdy.append(ui->rdy4);rdy.append(ui->rdy5);
    run.append(ui->run0);run.append(ui->run1);run.append(ui->run2);run.append(ui->run3);run.append(ui->run4);run.append(ui->run5);
    err.append(ui->err0);err.append(ui->err1);err.append(ui->err2);err.append(ui->err3);err.append(ui->err4);err.append(ui->err5);
    pwr.append(ui->pwr0);pwr.append(ui->pwr1);pwr.append(ui->pwr2);pwr.append(ui->pwr3);pwr.append(ui->pwr4);pwr.append(ui->pwr5);
    pumpSelector.append(ui->line_2);pumpSelector.append(ui->line_3);pumpSelector.append(ui->line_4);pumpSelector.append(ui->line_5);pumpSelector.append(ui->line_6);
    pumps = ui->pumps;
    for (int i=0;i<5;i++) pumpSelector[i]->setVisible(false);

    in_g.append(ui->in0_g);in_g.append(ui->in1_g);in_g.append(ui->in2_g);in_g.append(ui->in3_g);in_g.append(ui->in4_g);in_g.append(ui->in5_g);
    sbm_g = ui->sbm_g;
    ssm_g = ui->ssm_g;
    sbm = ui->sbm;
    ssm = ui->ssm;
    an = ui->an;
    an_g = ui->an_g;
    con = ui->con;
    winCon = ui->winCon;
    text = ui->status;
    alx = ui->alx;
    alx_2 = ui->alx_g;
    alxLab = ui->alxLab;
    alxLab_2 = ui->alxLab_2;
    aly = ui->aly;
    aly_2 = ui->aly_g;
    alyLab = ui->alyLab;
    alyLab_2 = ui->alyLab_2;
    alw = ui->alw;
    alw_2 = ui->alw_g;
    alwLab = ui->alwLab;
    alwLab_2 = ui->alwLab_2;
    onButton = ui->onSms;
    offButton = ui->offSms;
    statButton = ui->statusSms;
    resetButton = ui->resetSms;
    history = ui->history;
    data = ui->data;
    loading = ui->loading;
    loadingStat = ui->loadingStat_2;

    onlyAlarm = ui->onlyAlarm;
    minDate = ui->minDate;
    maxDate = ui->maxDate;
    historyList = ui->historyList;
    showHistory = ui->showHistory;
    minDate->setMinimumDate(QDate(2013,1,1));
    minDate->setDate(QDate(2013,1,1));
    maxDate->setMinimumDate(minDate->date());
    maxDate->setDate(QDate::currentDate());
    turnHistory = ui->turnHistory;
    turnHistory2 = ui->turnHistory2;
    turnHistory->setToolTip("История");
    history->setVisible(false);

    setFixedSize(500,370);
    resize(500,370);

    loading->setVisible(false);

    dailyWindow = new dailyWorking();
    dailyWindow->did = 0;

    connect(ui->onSms,SIGNAL(clicked()),this,SLOT(on()));
    connect(ui->offSms,SIGNAL(clicked()),this,SLOT(off()));
    connect(ui->statusSms,SIGNAL(clicked()),this,SLOT(stat()));
    connect(ui->resetSms,SIGNAL(clicked()),this,SLOT(reset()));
    connect(ui->turnHistory,SIGNAL(clicked()),this,SLOT(histPr()));
    connect(ui->turnHistory2,SIGNAL(clicked()),this,SLOT(histPr()));
    connect(ui->showHistory,SIGNAL(clicked()),this,SLOT(shH()));
    connect(minDate,SIGNAL(dateChanged(QDate)),this,SLOT(dCh()));
    connect(historyList,SIGNAL(itemChanged(QListWidgetItem*)),this,SLOT(shHS()));
    connect(historyList,SIGNAL(itemActivated(QListWidgetItem*)),this,SLOT(shHS()));
    connect(historyList,SIGNAL(itemSelectionChanged()),this,SLOT(shHS()));
    connect(ui->forWeek,SIGNAL(clicked()),this,SLOT(frWk()));
    connect(ui->forMonth,SIGNAL(clicked()),this,SLOT(frMnt()));
    connect(ui->exitButton,SIGNAL(clicked()),this,SLOT(close()));
    connect(ui->last15,SIGNAL(clicked()),this,SLOT(lastF()));
    connect(ui->info,SIGNAL(clicked()),this,SLOT(inPr()));
    connect(ui->daily,SIGNAL(clicked()),this,SLOT(showDailyWorking()));
    connect(dailyWindow,SIGNAL(dailyQuery(quint16,QDate,QDate,QDate)),this,SIGNAL(dailyQuery(quint16,QDate,QDate,QDate)));
    connect(dailyWindow,SIGNAL(saveQuery(quint16,QDate,QDate)),this,SIGNAL(saveQuery(quint16,QDate,QDate)));
    connect(dailyWindow,SIGNAL(monthQuery(quint16,int,int)),this,SIGNAL(monthQuery(quint16,int,int)));
    connect(dailyWindow,SIGNAL(m_saveQuery(quint16,int,int)),this,SIGNAL(m_saveQuery(quint16,int,int)));
    connect(dailyWindow,SIGNAL(yearQuery(quint16,int)),this,SIGNAL(yearQuery(quint16,int)));
    connect(dailyWindow,SIGNAL(y_saveQuery(quint16,int)),this,SIGNAL(y_saveQuery(quint16,int)));

    waiting = false;
    setWindowFlags((windowFlags() & ~Qt::WindowMaximizeButtonHint & ~Qt::WindowMinimizeButtonHint)| Qt::WindowStaysOnTopHint);
    ui->digBox->setVisible(false);
    ui->graphBox->setVisible(true);

}
deviceWindow::~deviceWindow()
{
    dailyWindow->close();
    delete dailyWindow;
    delete ui;
}

void deviceWindow::on() {
    if (!waiting) emit Action(index,"1");
}

void deviceWindow::off() {
    if (!waiting) emit Action(index,"0");
}

void deviceWindow::stat() {
    if (!waiting) emit Action(index,"5");
}

void deviceWindow::reset() {
    if (!waiting) emit Action(index,"9");
}

void deviceWindow::histPr() {
    emit History(index);
}

void deviceWindow::shH() {
    emit ShowHistory(index,-1);
}
void deviceWindow::shHS() {
    emit ShowHistoryStat(index);
}
void deviceWindow::dCh() {
    if (maxDate->date() < minDate->date()) maxDate->setDate(minDate->date());
    maxDate->setMinimumDate(minDate->date());
}
void deviceWindow::frWk() {
    minDate->setDate(QDate().currentDate().addDays(-7));
    maxDate->setDate(QDate().currentDate());
    emit ShowHistory(index,-1);
}

void deviceWindow::frMnt() {
    minDate->setDate(QDate().currentDate().addDays(-30));
    maxDate->setDate(QDate().currentDate());
    emit ShowHistory(index,-1);
}

void deviceWindow::SetWindowTitle(QString title) {
    setWindowTitle("(" + QString::number(index) + ") " + title);
}

void deviceWindow::lastF() {
    minDate->setDate(QDate(2013,1,1));
    maxDate->setDate(QDate::currentDate());
    onlyAlarm->setChecked(false);
    emit ShowHistory(index,15);
}

void deviceWindow::Show() {
    try {
        show();
    } catch (int err) {
        QMessageBox::critical(0,"БЕДАААААА","atqk");
        exit(88);
    };
}

void deviceWindow::setType(QString type) {
    if (type == "gsm") {
        ui->resetWidget->setVisible(true);
        ui->statusWidget->setVisible(true);
        ui->alWidget->setVisible(true);
        ui->alWidget_g->setVisible(true);
        ui->winCon->setVisible(false);
        ui->con->setVisible(false);
        ui->conLab->setVisible(false);
    } else if (type == "tcp") {
        ui->resetWidget->setVisible(false);
        ui->statusWidget->setVisible(false);
        ui->alWidget->setVisible(false);
        ui->alWidget_g->setVisible(false);
        ui->winCon->setVisible(true);
        ui->con->setVisible(true);
        ui->conLab->setVisible(true);
    } else if (type == "modbus") {
        ui->resetWidget->setVisible(false);
        ui->statusWidget->setVisible(false);
        ui->alWidget->setVisible(false);
        ui->alWidget_g->setVisible(false);
        ui->winCon->setVisible(true);
        ui->con->setVisible(true);
        ui->conLab->setVisible(true);
    };
}

void deviceWindow::inPr() {
    emit info(index);
}

void deviceWindow::setGraphics(bool on) {
    ui->digBox->setVisible(!on);
    ui->graphBox->setVisible(on);
}

void deviceWindow::moveEvent(QMoveEvent *ev) {
    emit moved(index);
}
void deviceWindow::showDailyWorking() {
    dailyWindow->did = index;
    dailyWindow->flow = flow;
    dailyWindow->show();
    dailyWindow->sendDailyQuery();
    dailyWindow->_setMonthQuery();
    dailyWindow->_setYearQuery();
}
