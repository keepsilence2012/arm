#include "askdialog.h"
#include "ui_askdialog.h"

askDialog::askDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::askDialog)
{
    ui->setupUi(this);
    setWindowTitle("Обновление АРМ SK-712");
    setModal(true);
}

askDialog::~askDialog()
{
    delete ui;
}

bool askDialog::saveSettings() {
    return true;
}


bool askDialog::saveStations() {
    return false;
}
