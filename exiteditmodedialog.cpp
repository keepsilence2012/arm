#include "exiteditmodedialog.h"
#include "ui_exiteditmodedialog.h"

exitEditModeDialog::exitEditModeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exitEditModeDialog)
{
    ui->setupUi(this);

    connect(ui->ok,SIGNAL(clicked()),this,SIGNAL(ok()));
    connect(ui->no,SIGNAL(clicked()),this,SIGNAL(no()));
    connect(ui->cancel,SIGNAL(clicked()),this,SIGNAL(cancel()));
    setWindowTitle("Выход из режима редактирования");
}

exitEditModeDialog::~exitEditModeDialog()
{
    delete ui;
}
