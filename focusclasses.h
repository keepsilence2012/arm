#ifndef FOCUSCLASSES_H
#define FOCUSCLASSES_H

#include "QWidget"
#include "QSpinBox"
#include "QTextEdit"
#include "QFocusEvent"

class devSpinBox : public QSpinBox {
    Q_OBJECT
public:
    devSpinBox(QWidget *parent = 0);
    void focusInEvent(QFocusEvent *event);
signals:
    void focused();
};


#endif // FOCUSCLASSES_H
