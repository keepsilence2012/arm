#include "tcp.h"

//TCPDevices
void TCPDevices::add(QString ip,quint8 addr)
{
    MDBTCP.append(new ModbusTCP(ip, addr, this));
    connect(MDBTCP.last(), SIGNAL(status_send(QByteArray,QBitArray,QString)), this, SLOT(got_status(QByteArray,QBitArray,QString)));
    connect(MDBTCP.last(), SIGNAL(error(QString,int)), this, SLOT(err(QString,int)));
}

void TCPDevices::del(QString ip) {
    int i= ip_match(ip);
    if ( i  != -1) {
        MDBTCP.at(i)->status_timer.stop();
        MDBTCP.at(i)->disconnect();
        delete MDBTCP.at(i);
    }
}

TCPDevices::TCPDevices(QObject *par) : QObject(par) {
    start = false;
}

int TCPDevices::ip_match(QString ip)
{
    for(int b=0;b<MDBTCP.count();b++)
        if (MDBTCP.at(b)->ip == ip)
            return b;
    emit error(ip, 3);
    return -1;
}

void TCPDevices::ch_switch(QString ip, int ch_num, int ch_set)
{
    int num=ip_match(ip);
    if (num>-1)
        MDBTCP.at(num)->ch_switch(ch_num, ch_set);
    else{

        if (DEBUG_MODE) qDebug() << "Error: No device with such ip:" << ip;

    }
}

void TCPDevices::makeChan(QString ip, int code) {
    int num=ip_match(ip);
    if (num>-1)
        MDBTCP.at(num)->makeChan(code);
    else{

        if (DEBUG_MODE) qDebug() << "Error: No device with such ip:" << ip;

    }
}

void TCPDevices::ch_off(QString ip)
{
    int num=ip_match(ip);
    if (num>-1)
        MDBTCP.at(num)->ch_off();
    else{
        if (DEBUG_MODE) qDebug() << "Error: No device with such ip:" << ip;
    }
}

void TCPDevices::set_par(QString ip, int par_num_h, int par_num_l, int par_set_h, int par_set_l)
{
    int num=ip_match(ip);
    if (num>-1)
        MDBTCP.at(num)->set_par(par_num_h, par_num_l, par_set_h, par_set_l);
    else{
        if (DEBUG_MODE) qDebug() << "Error: No device with such ip:" << ip;
    }
}

void TCPDevices::got_status(QByteArray Status, QBitArray Channels, QString ip)
{
    emit status(Status, Channels, ip);
}

void TCPDevices::err(QString ip, int code)
{
    emit error(ip, code);
}

//MODBUSTCP

ModbusTCP::ModbusTCP(QString ip_set, quint8 addr, QObject *parent) : QObject(parent)
{
    ip=ip_set;
    Channels.resize(6);
    Channels.fill(0);
    address = addr;
    status_timer.setInterval(Update_Interval);
    for (int b=0;b<MAXTHREADS;b++){
        WriteReq.append(new WriteThread(address));
        connect(WriteReq.at(b), SIGNAL(fail(int, int, int, int)), this, SLOT(WriteFail(int, int, int, int)));
        connect(WriteReq.at(b), SIGNAL(success(int, int, int, int)), this, SLOT(WriteSuccess(int, int, int, int)));
        connect(WriteReq.at(b), SIGNAL(error(QString,int)), this, SLOT(err(QString,int)));
    }
    cycle = new StatusThread(address);
    load();
    connect(&status_timer, SIGNAL(timeout()), cycle, SLOT(start()));
    connect(cycle, SIGNAL(sys_st(QByteArray)), this, SLOT(system_status(QByteArray)));
    connect(cycle, SIGNAL(error(QString,int)), this, SLOT(err(QString,int)));
    if (DEBUG_MODE) qDebug() << "КОЛИЧЕСТВО ТРЕДОВ: " << WriteReq.count();
}

void ModbusTCP::del() {
    delete this;
}

ModbusTCP::~ModbusTCP() {
    status_timer.stop();
    disconnect();
}

void ModbusTCP::load()
{
    ch_wait=0;
    port=ModbusTCP_PORT;
    Timeout=ModbusTCP_TIMEOUT;
    if (DEBUG_MODE) qDebug() << "Modbus TCP loaded for" << ip << ":" << port;
    cycle->load(ip, port, Timeout);
    status_timer.start();
}

void ModbusTCP::system_status(QByteArray stat)
{
    status=stat;
    emit status_send(status, Channels, ip);
    if (status.size()>10)
        ch_response(10);
}

void ModbusTCP::ch_switch(int ch_num, int ch_set)
{
    ch_wait=Response_Wait_Counter;
    int temp=0;
    if (ch_set) Channels.setBit(ch_num);
    else Channels.clearBit(ch_num);
    for (int b=0;b<Channels.size();++b)
        temp+=Channels.at(b)*qPow(2,b);
    set_par(0, 0, 0, temp);
}

void ModbusTCP::ch_off()
{
    ch_wait=Response_Wait_Counter;
    Channels.fill(0);
    set_par(0, 0, 0, 0);
}

void ModbusTCP::makeChan(int code) {
    set_par(0,0,0,code);
}

void ModbusTCP::set_par(int par_num_h, int par_num_l, int par_set_h, int par_set_l)
{
    int b=find_free_write_thread();
    if (b>-1){
        if (DEBUG_MODE) qDebug() << "Write at thread №" << b << "for ip:" << ip;
        WriteReq.at(b)->load(ip, port, Timeout, par_num_h, par_num_l, par_set_h, par_set_l);
    }
    else {
        if (DEBUG_MODE) qDebug() << "Error: no free write threads for ip:" << ip;
    }
}

void ModbusTCP::ch_response(int byte_num)
{
    if (ch_wait>0){
        ch_wait--;
        if (DEBUG_MODE) qDebug() << "Channels aren't updating:" << ch_wait << "for ip:" << ip;
    }
    else {
        for(int b=8-Channels.size(); b<8; ++b)
            Channels.setBit(7-b, status.at(byte_num)&(1<<(7-b)));
    }
}

void ModbusTCP::WriteFail(int par_num_h, int par_num_l, int par_set_h, int par_set_l)
{
    if (DEBUG_MODE) qDebug() << "Error when writing:" << par_num_h << par_num_l << par_set_h << par_set_l << "to ip" << ip;
}

void ModbusTCP::WriteSuccess(int par_num_h, int par_num_l, int par_set_h, int par_set_l)
{
    if (DEBUG_MODE) qDebug() << "Write: Success when writing:" << par_num_h << par_num_l << par_set_h << par_set_l << "to ip" << ip;
}

void ModbusTCP::err(QString ip, int code)
{
    emit error(ip, code);
}

int ModbusTCP::find_free_write_thread()
{
    for (int b=0;b<MAXTHREADS;b++)
        if (!(WriteReq.at(b)->buzy))
            return b;
    emit error(ip, 4);
    return -1;
}

//WRITETHREAD
WriteThread::WriteThread(quint8 addr, QObject * parent) : MyThread(parent) {
    buzy=false;
    address = addr;
}

void WriteThread::run() {
    Response=socket_req("Write:", Request, ip, port, Timeout);
    qDebug() << "write to " << ip << ": " << port;
    if (Response==Request)
        emit (success(Request.at(8),Request.at(9),Request.at(10),Request.at(11)));
    else
        emit (fail(Request.at(8),Request.at(9),Request.at(10),Request.at(11)));
    buzy=false;
}

void WriteThread::load(QString sip, int prt, int tmout, int par_num_h, int par_num_l, int par_set_h, int par_set_l) {
    buzy=true;
    ip=sip;
    port=prt;
    Timeout=tmout;
    Request.resize(12);
    Request[0]=0;
    Request[1]=0;
    Request[2]=0;
    Request[3]=0;
    Request[4]=0;
    Request[5]=6;
    Request[6]=address;
    Request[7]=6;
    Request[8]=par_num_h;
    Request[9]=par_num_l;
    Request[10]=par_set_h;
    Request[11]=par_set_l;

    qDebug() << "REQUEST ADDR" << Request[6];
    this->start();
}

//STATUS

StatusThread::StatusThread(quint8 addr, QObject * parent) : MyThread(parent) {
    address = addr;
}

void StatusThread::run() {
    QByteArray req;
    QByteArray resp;
    req.resize(12);
    req[0]=0;
    req[1]=0;
    req[2]=0;
    req[3]=0;
    req[4]=0;
    req[5]=6;
    req[6]=address;
    req[7]=4;
    req[8]=0;
    req[9]=0;
    req[10]=0;
    req[11]=124;

    resp = socket_req("Status: ", req, ip, port, Timeout);
    qDebug() << "ADDRESS:" << (quint8)req[6];
    emit sys_st(resp);
}

void StatusThread::load(QString sip, int prt, int tmout) {
    ip=sip;
    port=prt;
    Timeout=tmout;
}

//MYTHREAD

MyThread::MyThread(QObject *parent) : QThread(parent)
{
}

QByteArray MyThread::socket_req(QString debugstr, QByteArray req, QString rip, int prt, int tmout)
{
    QTcpSocket *Socket;
    QByteArray resp;

    Socket=new QTcpSocket(0);
    Socket->connectToHost(rip, prt);

    if(Socket->waitForConnected(tmout)) {
        if (DEBUG_MODE) qDebug() << debugstr << "Connected to" << rip << ":" << prt;
    }
    else {
        if (DEBUG_MODE) qDebug() << "Error: Connection timeout to" << rip << ":" << prt;
        emit error(rip, 1);
        Socket->close();
        delete Socket;
        return NULL;
    }
    Socket->write(req);
    if (DEBUG_MODE) qDebug() << debugstr << "Writed to" << rip << ":" << prt;
    if (Socket->waitForReadyRead(tmout)) {
        if (DEBUG_MODE) qDebug() << debugstr << "READ DATA STARTED... for " << rip << ":" << prt;
        resp = Socket->readAll();
        if (DEBUG_MODE) qDebug() << debugstr << "READ DATA FINISHED for " << rip << ":" << prt;
    }
    else {
        if (DEBUG_MODE) qDebug() << "Error: Answer timeout.";
        emit error(rip, 2);
        Socket->close();
        delete Socket;
        return NULL;
    }
    Socket->close();
    delete Socket;
    if (DEBUG_MODE) qDebug() << debugstr << "Connection closed to" << rip << ":" << prt;
    return resp;
}
