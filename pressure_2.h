#ifndef PRESSURE_2_H
#define PRESSURE_2_H

#include <QDialog>
#include <QSlider>
#include <QList>
#include <QLabel>
#include <QPushButton>
#include <QDebug>

namespace Ui {
class pressure_2;
}

class pressure_2 : public QDialog
{
    Q_OBJECT

public:
    explicit pressure_2(QWidget *parent = 0);
    ~pressure_2();
    QList<QSlider *> sliders;
    QList<QPushButton*> ups;
    QList<QPushButton*> downs;
    QList<QLabel *> presses;
public slots:
    void sliderMoved();
    void slideUp();
    void slideDown();
private:
    Ui::pressure_2 *ui;
};

#endif // PRESSURE_H
